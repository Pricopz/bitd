import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'msToSec'
})
export class MsToSecPipe implements PipeTransform {

  transform(ms: number): any {
    const sec: number = ms / 1000
    return sec.toFixed(1)
  }

}
