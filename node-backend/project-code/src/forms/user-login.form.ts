"use strict";

import * as Joi from 'joi';
import {FormHelperService} from "../services/form-helper.service";

export interface IUserLoginForm
{
    email?: string;
    password?: string;
    remember?: number;
}

export class UserLoginForm implements IUserLoginForm
{
    public email: string;
    public password: string;
    public remember: number;

    public static fromJson(json: IUserLoginForm): UserLoginForm
    {
        const _usr      = new UserLoginForm();
        _usr.email      = json.email ? json.email : null;
        _usr.password   = json.password ? json.password : null;
        _usr.remember   = json.remember ? json.remember : null;

        return _usr;
    }

    public static getJoiValidator(): any
    {
        return Joi.object().keys({
            'email': Joi.string().required().email(),
            'password': Joi.string().required(),
        });
    }

    public static getErrors(form: IUserLoginForm): any | null
    {
        const result = this.getJoiValidator().validate(form, {abortEarly: false, allowUnknown: true});
        if (result.error && result.error.details && result.error.details.length) {
            return FormHelperService.extractErrorsFromJoi(result.error.details);
        }
        return null;
    }
}
