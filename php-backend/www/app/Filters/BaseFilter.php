<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder as EBuilder;
use Illuminate\Database\Query\Builder as QBuilder;

/**
 * Interface IFilter
 * @package App\Filters
 */
abstract class BaseFilter
{
    /**
     * @param QBuilder $builder
     * @return QBuilder
     */
    public function getQueryBuilder(QBuilder $builder): QBuilder
    {
        return $builder;
    }

    /**
     * @param EBuilder $builder
     * @return EBuilder
     */
    public function getEloquentBuilder(EBuilder $builder): EBuilder
    {
        return $builder;
    }

    /**
     * @return array
     */
    abstract public function getRules(): array;

    /**
     * @param array|null $data
     * @return static
     */
//    public static function fromArray(array $data = null): self
//    {
//        $self = new static();
//        if ($data) {
//            foreach ($data as $property => $value) {
//                if (isset($data[$property])) {
//                    $self->{$property} = $value;
//                }
//            }
//        }
//        return $self;
//    }

    /**
     * @param array|null $data
     * @return static
     */
    public static function fromArray(array $data = null): self
    {
        $self = new static();
        if ($data !== null) {
            foreach ($data as $property => $value) {
                if (property_exists($self, $property) && isset($data[$property])) {
                    $type = self::getPropertyType($self, $property);
                    $self->{$property} = self::getCastedValue($type, $value);
                }
            }
        }
        return $self;
    }

    /**
     * @param $class
     * @param $property
     * @return string|null
     */
    private static function getPropertyType($class, $property): ?string
    {
        try {
            $rp = new \ReflectionProperty($class, $property);
            if ($type = $rp->getType()) {
                return $type->getName();
            }
            return null;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param $type
     * @param $value
     * @return bool|float|int|string
     */
    private static function getCastedValue($type, $value)
    {
        switch ($type) {
            case 'string':
                return (string)$value;
            case 'int':
                return (int)$value;
            case 'float':
                return (float)$value;
            case 'double':
                return (double)$value;
            case 'bool':
                return (bool)$value;
            default:
                return $value;
        }
    }

    /**
     * @return array
     */
    public function getValues(): array
    {
        return get_object_vars($this);
    }
}
