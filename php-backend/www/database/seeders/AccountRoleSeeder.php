<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AccountRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('account_roles')->insert([
            ['title' => 'administrator', 'readonly' => 1],
        ]);
    }
}
