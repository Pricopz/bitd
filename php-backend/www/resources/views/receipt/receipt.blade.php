<!DOCTYPE html>
<html lang="ro">
<head>
    <title>CHITANȚA</title>
    <style type='text/css'>
        * {
            margin: 0;
            padding: 0;
        }
        body {
            width: 100%;
            margin: 0;
            padding: 0;
        }
        p {
            margin:0;
            padding:0;
        }
        .page-one {
            margin: 0;
            width: 100%;
            height: 100%;
        }
        table tr {
            padding: 0;
            margin: 0;
        }
        table tr td {
            padding: 0;
            margin: 0;
        }
        /* Receipt */
        .receipt-body {
            width: 100%;
            height: 50%;
            border-top: 1pt solid #555;
        }
        .receipt-organization-table tr td.organization-info {
            width: 90%;
            font-size: 11pt;
        }
        .receipt-organization-table tr td.organization-logo {
            text-align: right;
            vertical-align: middle;
        }
        div.receipt-info {
            margin: 6mm 28mm 10mm;
            padding: 1mm;
            border: 1pt solid #555;
        }
        div.receipt-info table tr td {
            font-size: 12pt;
        }
        div.receipt-payer-info {
            height: 50mm;
        }
        div.receipt-cashier-info {
            width: 100%;
            text-align: right;
            padding: 5mm 0;
        }
    </style>
</head>
<body>
    <div class="page page-one">
        @for ($i = 0; $i < 2; $i++)
            <div class="receipt-body">
                <div class="receipt-organization">
                    <table class="receipt-organization-table">
                        <tr>
                            <td class="organization-info">
                                <p>
                                    {{ $receipt['organization']['name'] }}
                                </p>
                                <p>
                                    <strong>CIF: </strong>{{ $receipt['organization']['cif'] }}
                                </p>
                                <p>
                                    <strong>Sediul: </strong> ( {{ $receipt['organization']['county'] }} )
                                </p>
                                <p>
                                    <strong>Telefon: </strong> {{ $receipt['organization']['phones'] }}
                                </p>
                                <p>
                                    <strong>Email: </strong> {{ $receipt['organization']['emails'] }}
                                </p>
                                <p>
                                    <strong>Site: </strong> {{ $receipt['organization']['website_url'] }}
                                </p>
                            </td>
                            <td class="organization-logo">
{{--                                @if ($receipt['organization']['avatar'])--}}
{{--                                    <img src="data:image/jpg;base64, {!! $receipt['organization']['avatar'] !!}" width="90pt">--}}
{{--                                @endif--}}
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="receipt-info">
                    <table style="width: 100%;">
                        <tr>
                            <td colspan="3" style="text-align: center">
                                <strong>CHITANȚA</strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 33.33%; text-align: right">
                                <strong>Seria:</strong>  {{ $receipt['receipt']['series_prefix'] }}
                            </td>
                            <td style="width: 33.33%; text-align: center">
                                <strong>Nr.:</strong> {{ $receipt['receipt']['series_number'] }}
                            </td>
                            <td style="width: 33.33%; text-align: left">
                                <strong>Data:</strong> {{ $receipt['receipt']['date'] }}
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="receipt-payer-info">
                    <p>
                        <strong>Am primit de la:</strong> {{ $receipt['payer']['first_name'] }} {{ $receipt['payer']['last_name'] }}
                    </p>
                    <p>
                        <strong>Adresa:</strong> {{ $receipt['payer']['street'] }} {{ $receipt['payer']['city'] }} {{ $receipt['payer']['county'] }}
                    </p>
                    <p>
                        <strong>CNP:</strong> {{ $receipt['payer']['cnp'] }}
                    </p>
                    <p>
                        <strong>Telefon:</strong> {{ $receipt['payer']['phone'] }}
                    </p>
                    <p>
                        <strong>Email:</strong> {{ $receipt['payer']['email'] }}
                    </p>
                    <p>
                        <strong>Suma de:</strong> {{ $receipt['receipt']['amount'] }} lei, <strong>adică</strong>: {{ $receipt['receipt']['amount_string'] }}
                    </p>
                    <p>
                        <strong>Reprezentând:</strong> {{ implode(', ', array_map(static function($arr) { return $arr['title'] . ' (' . $arr['value'] . ')'; }, $receipt['receipt']['items'])) }}
                    </p>
                </div>
                <div class="receipt-cashier-info">
                    <p>
                        <strong>Casier:</strong> {{ $receipt['added_by']['first_name'] }} {{ $receipt['added_by']['last_name'] }}
                    </p>
                </div>
            </div>
        @endfor
    </div>
</body>
</html>
