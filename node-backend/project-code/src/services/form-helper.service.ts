'use strict';

export class FormHelperService
{
    public static REGEX_PASSWORD = new RegExp('^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$');
    public static REGEX_PHONE = new RegExp('^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\\s\\./0-9]*$');

    /**
     * Extract K-V Error From JOI
     * @param errorDetails
     */
    public static extractErrorsFromJoi(errorDetails: any[]): any | null
    {
        let errors = {};
        for (let i = 0; i < errorDetails.length; i++) {
            if (errorDetails[i].path && errorDetails[i].message) {
                const _key      = errorDetails[i].path[0];
                const _value    = errorDetails[i].message;
                errors[_key]    = _value.toString().replace(/["']/g, "");
            }
        }
        return JSON.stringify(errors) !== '{}' ? errors : null;
    }

    public static getErrorMessages(): any
    {
        return {
            "string.base": `field.string`,
            "pattern.base": `field.pattern`,
            "number.base": `field.number`,
            "date.base": `field.date`,
            "valid.base": `field.invalid`,
            "string.empty": `field.not.empty`,
            "string.email": `field.email`,
            "any.required": `field.required`,
            "any.date": `field.invalid.date`,
            "any.valid": `field.invalid.range`,
            "any.only": `field.invalid.match`,
            "string.pattern.base": `field.invalid.pattern`,
        };
    }
}
