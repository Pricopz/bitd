<!DOCTYPE html>
<html lang="ro">
<head>
    <title>Factură Fiscală</title>
    <style type='text/css'>
        * {
            margin: 0;
            padding: 0;
        }
        body {
            width: 100%;
            margin: 0;
            padding: 0;
        }
        p {
            margin:0;
            padding:0;
        }
        .page-one {
            margin: 0;
            width: 100%;
            height: 100%;
        }
        table tr {
            padding: 0;
            margin: 0;
        }
        table tr td {
            padding: 0;
            margin: 0;
        }
        /* Receipt */
        .invoice-body {
            width: 100%;
            height: 100%;
        }
        .invoice-organization-table tr td.organization-info {
            vertical-align: top;
            width: 105mm;
        }
        .invoice-organization-table tr td.organization-buyer {
            vertical-align: top;
            width: 90mm;
            padding-left: 15mm;
        }
        div.invoice-info {
            margin: 15mm 28mm 5mm;
        }
        table.invoice-series-info tr td {
            font-size: 9pt;
            padding: 1pt 2pt 0;
        }
        table.invoice-table-body {
            width: 100%;
            border-collapse: collapse;
        }
        table.invoice-table-body thead tr th {
            background: #DDDDDD;
        }
        table.invoice-table-body thead tr th,
        table.invoice-table-body tbody tr td {
            margin: 0;
            padding: 1mm;
            font-size: 9pt;
            border: 1pt solid gray;
            font-weight: normal;
        }
    </style>
</head>
<body>
    <div class="page page-one">
        <div class="invoice-body">
            <div class="invoice-organization">
                <table class="invoice-organization-table">
                    <tbody>
                        <tr>
                            <td class="organization-info">
                                <p>
                                    <strong>Furnizor: </strong>{{ $invoice['organization']['name'] }}
                                </p>
                                <p>
                                    <strong>CIF: </strong>{{ $invoice['organization']['cif'] }}
                                </p>
                                <p>
                                    <strong>Sediul: </strong>( {{ $invoice['organization']['county'] }} )
                                </p>
                                <p>
                                    <strong>Telefon: </strong>{{ $invoice['organization']['phones'] }}
                                </p>
                                <p>
                                    <strong>Email: </strong>{{ $invoice['organization']['emails'] }}
                                </p>
                            </td>
                            <td class="organization-buyer">
                                <p>
                                    <strong>Cumpărător:</strong> {{ $invoice['payer']['first_name'] }} {{ $invoice['payer']['last_name'] }}
                                </p>
                                <p>
                                    <strong>Adresa:</strong> {{ $invoice['payer']['street'] }}, {{ $invoice['payer']['city'] }}, {{ $invoice['payer']['county'] }}
                                </p>
                                <p>
                                    <strong>CNP:</strong> {{ $invoice['payer']['cnp'] }}
                                </p>
                                <p>
                                    <strong>Telefon:</strong> {{ $invoice['payer']['phone'] }}
                                </p>
                                <p>
                                    <strong>Email:</strong> {{ $invoice['payer']['email'] }}
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="invoice-info">
                <table style="width: 100%;">
                    <tbody>
                        <tr>
                            <td style="text-align: center; font-size: 14pt">
                                <strong>FACTURĂ FISCALĂ</strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <table class="invoice-series-info">
                                    <tbody>
                                    <tr>
                                        <td style="text-align: left">SERIA:</td>
                                        <td style="text-align: right">{{ $invoice['receipt']['series_prefix'] }}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">NR. FACTURII:</td>
                                        <td style="text-align: right">{{ $invoice['receipt']['series_number'] }}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">DATA (zi-luna-an): </td>
                                        <td style="text-align: right">{{ $invoice['receipt']['date'] }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="invoice-body">
                <table class="invoice-table-body">
                    <thead>
                        <tr>
                            <th>Nr.<br>Crt.</th>
                            <th>Denumirea produselor sau a serviciilor</th>
                            <th>U.N.</th>
                            <th>Cantitate</th>
                            <th>Cotă<br>TVA</th>
                            <th>Valoare<br>(fară TVA)</th>
                            <th>Valoare TVA</th>
                            <th>Valoare<br>(cu TVA)</th>
                        </tr>
                    </thead>
                    @if (count($invoice['receipt']['items']) > 0)
                        <tbody>
                            @php
                                $totalWithoutVat = 0;
                                $totalVatValue = 0;
                                $totalValue = 0;
                            @endphp
                            @foreach ($invoice['receipt']['items'] as $key => $item)
                                {{ $totalWithoutVat += (float)$item['value_without_vat'] }}
                                {{ $totalVatValue += (float)$item['vat_value'] }}
                                {{ $totalValue += (float)$item['value'] }}
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $item['title'] }}</td>
                                    <td>buc</td>
                                    <td style="text-align: center">1</td>
                                    <td style="text-align: center">
                                        {{ $item['vat'] }}
                                    </td>
                                    <td style="text-align: center">
                                        {{ number_format($item['value_without_vat'], 2, '.', '') }}
                                    </td>
                                    <td style="text-align: center">
                                        {{ number_format($item['vat_value'], 2, '.', '') }}
                                    </td>
                                    <td style="text-align: center">
                                        {{ number_format($item['value'], 2, '.', '') }}
                                    </td>
                                </tr>
                            @endforeach
                                <tr>
                                    <td colspan="5" style="text-align: right">Total</td>
                                    <td style="text-align: center">
                                        {{ number_format($totalWithoutVat, 2, '.', '') }}
                                    </td>
                                    <td style="text-align: center">
                                        {{ number_format($totalVatValue, 2, '.', '') }}
                                    </td>
                                    <td style="text-align: center">
                                        {{ number_format($totalValue, 2, '.', '') }}
                                    </td>
                                </tr>
                        </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</body>
</html>
