'use strict';

import * as express from "express";
import UserController from "../controllers/user.controller";

export const userRouter: express.Router = express.Router();
const controller: UserController        = new UserController();

userRouter.get('/', controller.hello.bind(controller));
userRouter.post('/', controller.create.bind(controller));
userRouter.post('/login', controller.login.bind(controller));
