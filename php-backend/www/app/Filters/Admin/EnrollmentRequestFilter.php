<?php

namespace App\Filters\Admin;

use App\Filters\BaseFilter;
use App\Models\Organization\Organization;
use App\Models\Organization\OrganizationEmployee;
use Illuminate\Database\Eloquent\Builder as EBuilder;

/**
 * Class EnrollmentRequestFilter
 * @package App\Filters\Admin
 */
class EnrollmentRequestFilter extends BaseFilter
{
    /**
     * @var string
     */
    public string $key = '';

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            'key' => [
                'string',
                'min:2',
            ],
        ];
    }

    /**
     * @param EBuilder $builder
     * @return EBuilder
     */
    public function getEloquentBuilder(EBuilder $builder): EBuilder
    {
        $t1   = OrganizationEmployee::getTableName();
        $t2   = Organization::getTableName();
        $self = $this;
        if ($this->key) {
            $builder->where(static function ($query) use ($t1, $t2, $self) {
                $query->where("{$t1}.first_name", 'like', "%{$self->key}%")
                    ->orWhere("{$t1}.last_name", 'like', "%{$self->key}%")
                    ->orWhere("{$t1}.phone", 'like', "%{$self->key}%")
                    ->orWhere("${t2}.name", 'like', "%{$self->key}%");
            });
        }
        return $builder;
    }
    /**
     * @return array
     */
    public function getErrorMessage() : array
    {
        return [
            'key.string'                        => trans('admin/validation.field.string'),
            'key.min'                           => trans('admin/validation.field.min'),
        ];
    }
}
