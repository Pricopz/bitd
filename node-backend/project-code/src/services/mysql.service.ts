'use strict';

import {Sequelize} from "sequelize";
import UserModelCto from "../models/user.model";
import {TableEnum} from "../enums/table.enum";

type config = {host: any, user: any, password: any, database: any};

export default class MySqlService
{
    private static instance: MySqlService;
    private sequelize: any;
    private config: config;

    // Model creators for every table
    private userModelCto: UserModelCto;

    public constructor()
    {
        this.initModels();
    }

    private initModels(): void
    {
        this.userModelCto = new UserModelCto();
    }

    public static getInstance(): MySqlService
    {
        if (!MySqlService.instance) {
            MySqlService.instance = new MySqlService();
        }
        return MySqlService.instance;
    }

    public isEmptyConnection(): boolean
    {
        return (!this.sequelize || (this.sequelize && !this.sequelize.options && !this.sequelize.options.pool));
    }

    public async initDatabase(config: config)
    {
        this.config = config;
        if (this.isEmptyConnection()) {
            try {
                // init connection & authenticate
                this.sequelize = await this.initSequelizeHelper();
                await this.sequelize.authenticate();
            } catch (e) {
                this.sequelize = null;
                throw e;
            }
        }
        return 'Connection started and table models initialized.';
    }

    public async getSequelize()
    {
        if (this.isEmptyConnection()) {
            try {
                this.sequelize = await this.initSequelizeHelper();
                await this.sequelize.authenticate();
            } catch (e) {
                this.sequelize = null;
                throw e;
            }
        }
        return this.sequelize;
    }

    public async availableConnection()
    {
        await this.getSequelize();
        if (this.isEmptyConnection()) {
            throw new Error('Empty database connection.');
        }
        return true;
    }

    public async getModelCto(table: TableEnum)
    {
        const seq = await this.getSequelize();
        switch (table) {
            case TableEnum.USERS:
                return this.userModelCto.getModelCto(seq);
            default:
                return null;
        }
    }

    public async closeConnection()
    {
        if (this.sequelize) {
            await this.sequelize.close();
            this.sequelize = null;
        }
    }

    private initSequelizeHelper(): any
    {
        return new Sequelize(this.config.database, this.config.user, this.config.password, {
            host: this.config.host,
            dialect: 'mysql',
            pool: {
                max: 5,
                min: 0,
                acquire: 30000,
                idle: 10000
            }
        });
    }
}
