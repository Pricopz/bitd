<?php

namespace App\Filters\Organization;

use App\Enum\FeeTaxTypeEnum;
use App\Filters\BaseFilter;
use App\Models\Organization\OrganizationMoneyTrack;
use App\Models\Organization\OrganizationMoneyTrackItem;
use Illuminate\Database\Query\Builder as QBuilder;
use Illuminate\Validation\Rule;

/**
 * Class ReceiptFilter
 * @package App\Filters\Organization
 */
class ReceiptFilter extends BaseFilter
{
    /**
     * @var string|null
     */
    public ?string $startDate = null;
    /**
     * @var string|null
     */
    public ?string $endDate = null;
    /**
     * @var mixed|null
     */
    public $payment_type = null;
    /**
     * @var int|null
     */
    public ?int $offlinePayment = null;
    /**
     * @var int|null
     */
    public ?int $onlinePayment = null;

    /**
     * @param QBuilder $builder
     * @return QBuilder
     */
    public function getQueryBuilder(QBuilder $builder): QBuilder
    {
        $t0 = OrganizationMoneyTrackItem::getTableName();
        $t1 = OrganizationMoneyTrack::getTableName();
        if ($this->startDate) {
            $builder->whereDate("{$t1}.created_at", ">=", $this->startDate);
        }
        if ($this->endDate) {
            $builder->whereDate("{$t1}.created_at", "<=", $this->endDate);
        }
        if ($this->offlinePayment && !$this->onlinePayment) {
            $builder->whereNotNull(["{$t1}.cash_register_id"]);
        }
        if ($this->onlinePayment && !$this->offlinePayment) {
            $builder->whereNull(["{$t1}.cash_register_id"]);
        }
        if ($this->payment_type && $this->payment_type['type'] && $this->payment_type['id']) {
            if ($this->payment_type['type'] === FeeTaxTypeEnum::FEE) {
                $builder->where("{$t0}.fee_id", "=", $this->payment_type['id']);
            } elseif ($this->payment_type['type'] === FeeTaxTypeEnum::ADDITIONAL_FEE) {
                $builder->where("{$t0}.additional_fee_id", "=", $this->payment_type['id']);
            } elseif ($this->payment_type['type'] === FeeTaxTypeEnum::TAX) {
                $builder->where("{$t0}.tax_id", "=", $this->payment_type['id']);
            }
        }
        return $builder;
    }

    public function getRules(): array
    {
        return [
            'startDate' => [
                'nullable',
                'date',
                'date_format:Y-m-d',
            ],
            'endDate' => [
                'nullable',
                'date',
                'date_format:Y-m-d',
//                'after:startDate',
            ],
            'offlinePayment' => [
                'nullable',
                Rule::in([0, 1])
            ],
            'onlinePayment' => [
                'nullable',
                Rule::in([0, 1])
            ],
            'payment_type' => [
                'nullable',
            ],
            'payment_type.type' => [
                'nullable',
                Rule::in(FeeTaxTypeEnum::asList())
            ],
            'payment_type.id' => [
                'required_if:payment_type.type,FEE,ADDITIONAL_FEE,TAX',
                'integer',
            ],
        ];
    }

    public function getErrorMessage(): array
    {
        return [
            'startDate.date'                => trans('organization/validation.field.date'),
            'startDate.date_format'         => trans('organization/validation.field.invalid'),
            'endDate.date'                  => trans('organization/validation.field.date'),
            'endDate.date_format'           => trans('organization/validation.field.invalid'),
            'offlinePayment.in'             => trans('organization/validation.field.invalid'),
            'onlinePayment.in'              => trans('organization/validation.field.invalid'),
            'payment_type.type.in'          => trans('organization/validation.field.invalid'),
            'payment_type.id.required_if'   => trans('organization/validation.field.required'),
            'payment_type.id.integer'       => trans('organization/validation.field.invalid'),
        ];
    }
}
