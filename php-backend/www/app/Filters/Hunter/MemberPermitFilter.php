<?php

namespace App\Filters\Hunter;

use App\Filters\BaseFilter;
use App\Models\Hunter\Hunter;
use App\Models\Organization\Organization;
use Illuminate\Database\Eloquent\Builder as EBuilder;

/**
 * Class MemberPermitFilter
 * @package App\Filters\Hunter
 */
class MemberPermitFilter extends BaseFilter
{
    /**
     * @var Hunter
     */
    private Hunter $hunter;
    /**
     * @var string
     */
    public ?string $organization_id = null;

    /**
     * @param Hunter $hunter
     */
    public function setHunter(Hunter $hunter): void
    {
        $this->hunter = $hunter;
    }

    /**
     * @param EBuilder $builder
     * @return EBuilder
     */
    public function getEloquentBuilder(EBuilder $builder): EBuilder
    {
        if ($this->hunter) {
            $builder->where('hunter_id', '=', $this->hunter->id);
        }
        if ($this->organization_id) {
            $t2 = Organization::getTableName();
            $builder->where($t2 . '.public_id', '=', $this->organization_id);
        }
        return $builder;
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            'organization_id' => [
                'nullable',
                'exists:organizations,public_id'
            ]
        ];
    }

    /**
     * @return array
     */
    public function getErrorMessage(): array
    {
        return [
            'organization_id.exists'    => trans('hunter/validation.field.invalid'),
        ];
    }
}
