import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { ICalendarLevel } from './calendar-level'
import { MonthsLabelEnum } from './months-label.enum'
import { ICalendarSelection } from './ICalendarSelection'
import { HelperService } from '../../services/helper.service'

@Component({
  selector: 'app-calendar-filter',
  templateUrl: './calendar-filter.component.html',
  styleUrls: ['./calendar-filter.component.scss'],
})
export class CalendarFilterComponent implements OnInit {
  // OnChanges

  @Input() selection: ICalendarSelection
  @Input() useDefault: boolean = false
  @Input() loading: boolean = false
  @Input() months: ICalendarLevel[] = []
  @Input() days: ICalendarLevel[]
  @Input() hours: ICalendarLevel[]
  @Output() changeCalendarEvent: EventEmitter<ICalendarSelection> = new EventEmitter<
    ICalendarSelection
  >()

  private _year: number
  private _month: number
  private _day: number
  private _hour: number
  private _noOfDays: number = 31

  public get noOfDays(): number {
    return this.days ? this.days.length : this._noOfDays
  }

  public get year(): number {
    return this._year
  }

  public get month(): number {
    return this._month
  }

  public get day(): number {
    return this._day
  }

  public get hour(): number {
    return this._hour
  }

  constructor() {}

  public onSelectMonth(value: ICalendarLevel): void {
    if (this.loading) {
      return
    }
    this._year = value.level.year
    this._month = value.level.month
    this._day = undefined
    this._hour = undefined
    if (this.useDefault) {
      this.days = this.getDefaultDaysLevel(this._year, this._month)
    }
    this.changeCalendarEvent.emit({ year: this._year, month: this._month })
    return
  }

  public onSelectDay(value: ICalendarLevel): void {
    if (this.loading) {
      return
    }
    this._day = value.level.day
    this._hour = undefined
    if (this.useDefault) {
      this.hours = this.getDefaultHoursLevel()
    }
    this.changeCalendarEvent.emit({ year: this._year, month: this._month, day: this._day })
    return
  }

  public onSelectHour(value: ICalendarLevel): void {
    if (this.loading) {
      return
    }
    this._hour = value.level.hour
    this.changeCalendarEvent.emit({
      year: this._year,
      month: this._month,
      day: this._day,
      hour: this._hour,
    })
    return
  }

  private getDefaultMonthsLevel(): ICalendarLevel[] {
    const monthsLevel: ICalendarLevel[] = []
    const year = new Date().getFullYear()
    const month = new Date().getMonth() + 1
    for (let i = 0; i < 12; i++) {
      let _year = year
      let _month = month - i
      if (_month <= 0) {
        _month = 12 + _month
        _year--
      }
      monthsLevel.push({
        label: this.getMonthLabel(_month),
        level: { year: _year, month: _month },
        info: `${_year}`,
      })
    }
    return monthsLevel
  }

  private getDefaultDaysLevel(year: number, month: number): ICalendarLevel[] {
    this._noOfDays = new Date(year, month, 0).getDate()
    const daysLevel: ICalendarLevel[] = []
    for (let i = 1; i <= this._noOfDays; i++) {
      daysLevel.push({ label: `${i}`, level: { year: this._year, month: this._month, day: i } })
    }
    return daysLevel
  }

  private getDefaultHoursLevel(): ICalendarLevel[] {
    const hoursLevel: ICalendarLevel[] = []
    for (let i = 0; i <= 23; i++) {
      hoursLevel.push({
        label: i < 10 ? `0${i}:00` : `${i}:00`,
        level: { year: this._year, month: this._month, day: this._day, hour: i },
      })
    }
    return hoursLevel
  }

  private getMonthLabel(m: number): string {
    const mm = `m${m}`
    if (mm in MonthsLabelEnum) {
      return MonthsLabelEnum[mm]
    }
    return ''
  }

  private parseSelection(): void {
    this._year = this.selection && this.selection.year ? this.selection.year : undefined
    this._month = this.selection && this.selection.month ? this.selection.month : undefined
    this._day = this.selection && this.selection.day ? this.selection.day : undefined
    this._hour = this.selection && this.selection.hour ? this.selection.hour : undefined
  }

  public isActiveMonth(data: ICalendarLevel): boolean {
    return (
      this.selection &&
      this.selection.year === data.level.year &&
      this.selection.month === data.level.month
    )
  }

  public isActiveDay(data: ICalendarLevel): boolean {
    return this.selection && this.selection.day === data.level.day
  }

  public isActiveHour(data: ICalendarLevel): boolean {
    return this.selection && this.selection.hour === data.level.hour
  }

  public getColorStatus(data: ICalendarLevel): string {
    if (!HelperService.isNullOrUndefined(data.status)) {
      return HelperService.getColorClassForBullet(data.status)
    }
    return ''
  }

  ngOnInit(): void {
    if (this.useDefault) {
      this.months = this.getDefaultMonthsLevel()
    }
    this.parseSelection()
  }

  // ngOnChanges(changes: SimpleChanges): void {
  //   // if ('months' in changes) {
  //   //   this.monthsLevel = changes['months'].currentValue
  //   // }
  //   // if ('days' in changes) {
  //   //   this.daysLevel = changes['days'].currentValue
  //   //   this._noOfDays = this.daysLevel ? this.daysLevel.length : this._noOfDays
  //   // }
  //   // if ('hours' in changes) {
  //   //   this.hoursLevel = changes['hours'].currentValue
  //   // }
  //   // if ('selection' in changes) {
  //   //   this.selection = changes['selection'].currentValue
  //   //   this.parseSelection()
  //   // }
  // }
}
