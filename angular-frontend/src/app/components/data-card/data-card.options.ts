export interface IDataCardOptions {
  closeOnOutsideClick?: boolean
  appendOnElement?: {class?: string, id?: string}
  ignoreOutsiderElements?: string[]
}

export class DataCardOptions {
  public closeOnOutsideClick: boolean = true
  public appendOnElement: {class?: string, id?: string}
  public ignoreOutsiderElements: string[]

  public constructor(model?: IDataCardOptions) {
    if (model) {
      Object.assign(this, model)
    }
  }
}
