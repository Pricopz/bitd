import { ValidatorFn, FormControl } from '@angular/forms'

export function notPattern(pattern: string): ValidatorFn {
  return (control: FormControl): { [key: string]: boolean } | null => {
    if (control.value && control.value.match(pattern)) {
      return {invalidStringForPattern: true}
    }
    return null
  }
}
