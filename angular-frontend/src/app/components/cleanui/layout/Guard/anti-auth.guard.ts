import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { BasicAuthService } from '../../../../services/basic-auth.service'

@Injectable({
  providedIn: 'root',
})
export class AntiAuthGuard {
  constructor(public authService: BasicAuthService, public router: Router) {}

  public async resolve() {
    const isAuthenticated = await this.authService.isAuthenticated().toPromise()
    if (isAuthenticated) {
      await this.router.navigate(['/posts'])
    }
  }
}
