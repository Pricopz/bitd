'use strict';

export class UserDto
{
    public id?: string;
    public name?: string;
    public email?: string;

    public static fromJson(json: any): UserDto
    {
        const _usr  = new UserDto();
        _usr.id     = json.id ? json.id : null;
        _usr.name   = json.name ? json.name : null;
        _usr.email  = json.email ? json.email : null;

        return _usr;
    }

    public static toString(json: any): string
    {
        const _usr  = new UserDto();
        _usr.id     = json.id ? json.id : null;
        _usr.name   = json.name ? json.name : null;
        _usr.email  = json.email ? json.email : null;

        return JSON.stringify(_usr);
    }
}
