<?php

return [
    'subject.account.activation'                => 'Activare cont',
    'subject.account.reset.password'            => 'Resetare parolă',
    'subject.admin.notification'                => 'Înregistrare cont asociații',
    'subject.enroll.to.organization.approval'   => 'Cerere de înrolare aprobată',
    'subject.enroll.to.organization.rejection'  => 'Cerere de înrolare respinsă',
    'subject.employee.activate.account'         => 'Activare cont angajat',
    'subject.organization.receipt.report'       => 'Raport încasari',
];
