import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { of } from 'rxjs'
import { CookieService } from 'ngx-cookie-service'
import { environment } from '../../environments/environment'
import { ILoginResponseModel } from '../models/login-response.model'
import { IUserModel } from '../models/user.model'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { HttpResponseHandlerService } from './http-response-handler.service'

@Injectable({
  providedIn: 'root',
})
export class BasicAuthService {
  private apiURL: string = environment.apiUrl + '/user'
  private cookieName: string = '__bit_token'
  private token: string = null
  private user: IUserModel = null

  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
    public router: Router,
    private handlerResponseService: HttpResponseHandlerService,
    private modalService: NgbModal,
  ) {}

  public async create(name: string, email: string, password: string, confirmPassword: string) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
    const data = {
      name: name,
      email: email,
      password: password,
      confirmPassword: confirmPassword,
    }
    try {
      await this.http
        .post<ILoginResponseModel>(this.apiURL, data, { headers: headers })
        .toPromise()
      this.handlerResponseService.notifySuccess(
        'Success',
        'You have successfully created account!',
      )
      await this.router.navigate(['auth/login'])
    } catch (e) {
      this.handlerResponseService.notifyError(e)
    }
  }

  private async _LoginHttpCall(data: any, headers: HttpHeaders): Promise<ILoginResponseModel> {
    return await this.http
      .post<ILoginResponseModel>(this.apiURL + '/login', data, { headers: headers })
      .toPromise()
  }

  public async login(email: string, password: string, remember?: any) {
    const data = {
      email: email,
      password: password,
      remember: remember ? 1 : 0,
    }
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
    try {
      const result = await this._LoginHttpCall(data, headers)
      await this.saveData(result.token, result.user, result.expires_in)
      this.handlerResponseService.notifySuccess(
        'Success',
        'You have successfully logged in!',
      )
      await this.router.navigate(['posts'])
    } catch (e) {
      this.handlerResponseService.notifyError(e)
    }
  }

  private async saveData(token: string, user: IUserModel, expires_in: number) {
    this.token = token
    this.user = user
    this.cookieService.set(
      this.cookieName,
      JSON.stringify({ t: token, u: user}),
      this.getExpirationDate(expires_in),
    )
  }

  private getExpirationDate(expires_in: number): Date {
    const date: Date = new Date()
    date.setSeconds(date.getSeconds() + expires_in)
    return date
  }

  private async clearData() {
    this.token = null
    this.user = null
    this.cookieService.delete(this.cookieName)
  }

  public isAuthenticated() {
    return of(this.getToken() !== null)
  }

  public getToken(): string {
    if (this.token === null) {
      let jsonData = null
      if ((jsonData = this.cookieService.get(this.cookieName))) {
        this.extractDataFromCookie(jsonData)
      }
    }
    return this.token
  }

  public getUser(): IUserModel {
    if (this.user === null) {
      let jsonData = null
      if ((jsonData = this.cookieService.get(this.cookieName))) {
        this.extractDataFromCookie(jsonData)
      }
    }
    return this.user
  }

  public getUserRole(): string | null {
    return null
  }

  public getUserName(): string {
    this.getUser()
    if (this.user) {
      return this.user.name
    }
    return ''
  }

  private extractDataFromCookie(jsonData: string): void {
    const data = JSON.parse(jsonData)
    if (data) {
      this.token = data.t
      this.user = data.u
    }
  }

  public async logout() {
    this.modalService.dismissAll()
    await this.clearData()
    await this.router.navigate(['posts'])
  }
}
