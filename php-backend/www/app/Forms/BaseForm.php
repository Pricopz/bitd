<?php

namespace App\Forms;

/**
 * Class BaseForm
 * @package App\Forms
 */
abstract class BaseForm
{
    /**
     * @param array $data
     * @return self
     */
    public static function fromArray(array $data): self
    {
        $self = new static();
        foreach ($data as $property => $value) {
            if (property_exists($self, $property) && isset($data[$property])) {
                $type = self::getPropertyType($self, $property);
                $self->{$property} = self::getCastedValue($type, $value);
            }
        }
        return $self;
    }

    /**
     * @param $class
     * @param $property
     * @return string|null
     */
    private static function getPropertyType($class, $property): ?string
    {
        try {
            $rp = new \ReflectionProperty($class, $property);
            if ($type = $rp->getType()) {
                return $type->getName();
            }
            return null;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param $type
     * @param $value
     * @return bool|float|int|string
     */
    private static function getCastedValue($type, $value)
    {
        switch ($type) {
            case 'string':
                return (string)$value;
            case 'int':
                return (int)$value;
            case 'float':
                return (float)$value;
            case 'double':
                return (double)$value;
            case 'bool':
                return (bool)$value;
            default:
                return $value;
        }
    }

    /**
     * @return array
     */
    public function getValues(): array
    {
        return get_object_vars($this);
    }
}
