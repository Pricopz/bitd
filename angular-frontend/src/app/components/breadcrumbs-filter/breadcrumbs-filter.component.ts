import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'

@Component({
  selector: 'app-breadcrumbs-filter',
  templateUrl: './breadcrumbs-filter.component.html',
  styleUrls: ['./breadcrumbs-filter.component.scss'],
})
export class BreadcrumbsFilterComponent implements OnInit {
  @Input() sid: number | null
  @Input() filter: any
  @Input() filterMap: any
  @Input() ignore: string[] = []
  @Output() filterChangeEvent: EventEmitter<any> = new EventEmitter<any>()
  @Output() filterRemoveEvent: EventEmitter<any> = new EventEmitter<any>()

  constructor() {}

  public clearAllFilters(): void {
    this.filterRemoveEvent.emit()
  }

  public clearFilterKey(key: string): void {
    this.filter[key] = null
    this.filterChangeEvent.emit(this.filter)
  }

  public displayFilter(item: any): boolean {
    if (this.ignore.indexOf(item.key) !== -1) {
      return false
    }
    if (item.value && (item.value !== undefined || item.value !== null || item.value !== '')) {
      return true
    }
    return false
  }

  public keyToLabel(key: string): string {
    return this.filterMap && this.filterMap[key] ? this.filterMap[key] : key
  }

  public hasFilters(): boolean {
    return this.filter && Object.keys(this.filter).length > 0
  }

  ngOnInit(): void {}
}
