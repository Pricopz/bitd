<?php

namespace App\Exceptions;

/**
 * Class EntityDontMeetCriteriaException
 * @package App\Exceptions
 */
class EntityDontMeetCriteriaException extends \Exception
{

}
