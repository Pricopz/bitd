<?php

namespace App\Filters\Organization;

use App\Enum\PaymentPaperTypeEnum;
use App\Filters\BaseFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Rule;

/**
 * Class OrganizationCashDeskTransactionFilter
 * @package App\Filters\Organization
 */
class OrganizationCashDeskTransactionFilter extends BaseFilter
{
    /**
     * @var string|null
     */
    public ?string $type = null;
    /**
     * @var string|null
     */
    public ?string $start_date = null;
    /**
     * @var string|null
     */
    public ?string $end_date = null;

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function getEloquentBuilder(Builder $builder): Builder
    {
        if ($this->type) {
            $builder->where('type', '=', $this->type);
        }
        if ($this->start_date) {
            $builder->where('created_at', '>=', $this->start_date);
        }
        if ($this->end_date) {
            $builder->where('created_at', '<=', $this->end_date);
        }
        return $builder;
    }

    /**
     * @return array[]
     */
    public function getRules(): array
    {
        return [
            'type' => [
                'nullable',
                Rule::in(PaymentPaperTypeEnum::asList())
            ],
            'start_date' => [
                'nullable',
                'date',
                'date_format:Y-m-d',
                'before_or_equal:end_date',
            ],
            'end_date' => [
                'nullable',
                'date',
                'date_format:Y-m-d',
                'before_or_equal:' . date('Y-m-d'),
            ],
        ];
    }

    public function getErrorMessage(): array
    {
        return [
            'type.in'                   => trans('admin/validation.field.invalid'),
            'start_date.date'           => trans('admin/validation.field.date'),
            'start_date.date_format'    => trans('admin/validation.field.date_format'),
            'start_date.before_or_equal'=> trans('admin/validation.field.date.invalid.value'),
            'end_date.date'             => trans('admin/validation.field.date'),
            'end_date.date_format'      => trans('admin/validation.field.date_format'),
            'end_date.before_or_equal'  => trans('admin/validation.field.date.invalid.value'),
        ];
    }
}
