'use strict';

import * as express from "express";
import {userRouter} from "./user.router";

export interface IRouteMap
{
    path: string;
    route: express.Router;
}

export class RouterManager
{
    private _user_routes: IRouteMap[] = [
        {path: '/api/v1/user', route: userRouter},
    ];

    public setRoutes(app: express.Application): void
    {
        this._user_routes.forEach((map: IRouteMap) => {
            app.use(map.path, map.route);
        });
    }
}
