"use strict";

import { DataTypes } from "sequelize";
import AbstractModelCreator from "./abstract.model.creator";
import { TableEnum } from "../enums/table.enum";

export default class UserModelCto extends AbstractModelCreator
{
    public initModel = sequelize => {
        this.modelCtor = sequelize.define('User',
        {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            name: DataTypes.STRING(128),
            email: {
                type: DataTypes.STRING(128),
                allowNull: false
            },
            password: {
                type: DataTypes.STRING(100),
                allowNull: true
            },
        },
        {
            sequelize,
            tableName: TableEnum.USERS,
            timestamps: false
        });
    };

    public addAssociations = async () => {}
}
