import { IUserModel } from './user.model'

export interface ILoginResponseModel {
  token?: string
  expires_in?: number
  user?: IUserModel
}

export class LoginResponseModel implements ILoginResponseModel {
  public token
  public expires_in
  public user

  constructor(model?: ILoginResponseModel) {
    if (model) {
      Object.assign(this, model)
    }
  }
}
