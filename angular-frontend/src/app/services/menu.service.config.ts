export const getMenuData: any[] = [
  {
    title: 'Posts',
    key: 'posts',
    icon: 'fa fa-pencil',
    url: '/user/posts',
  },
]
