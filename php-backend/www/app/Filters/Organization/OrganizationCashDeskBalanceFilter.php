<?php

namespace App\Filters\Organization;

use App\Filters\BaseFilter;

/**
 * Class OrganizationCashDeskBalanceFilter
 * @package App\Filters\Organization
 */
class OrganizationCashDeskBalanceFilter extends BaseFilter
{
    /**
     * @var string|null
     */
    public ?string $day = null;

    /**
     * @return array[]
     */
    public function getRules(): array
    {
        return [
            'day' => [
                'nullable',
                'date',
                'date_format:Y-m-d',
                'before_or_equal:' . date('Y-m-d')
            ],
        ];
    }

    public function getErrorMessage(): array
    {
        return [
            'day.date'              => trans('admin/validation.field.date'),
            'day.date_format'       => trans('admin/validation.field.date_format'),
            'day.before_or_equal'   => trans('admin/validation.field.date.invalid.value'),
        ];
    }
}
