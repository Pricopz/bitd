import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'nullAsDash',
})
export class NullAsDashPipe implements PipeTransform {
  transform(value: unknown, ...args: unknown[]): unknown {
    if (!value || value.toString().length === 0) {
      return '-'
    }
    return value
  }
}
