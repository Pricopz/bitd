import { Action } from '@ngrx/store'
import { IStoredOptionListModel } from '../../models/stored-option-list.model'

export const SAVE_OPTIONS   = '[OptionList] Save Options'
export const CLEAR_OPTIONS  = '[OptionList] Clear Options'

export class SaveOptionsAction implements Action {
  readonly type = SAVE_OPTIONS
  constructor(public payload: IStoredOptionListModel) {}
}

export class ClearOptionsAction implements Action {
  readonly type = CLEAR_OPTIONS
  // constructor(public payload: object) {}
}

export type Actions = SaveOptionsAction | ClearOptionsAction
