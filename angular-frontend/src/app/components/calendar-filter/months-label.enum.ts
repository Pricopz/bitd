export enum MonthsLabelEnum {
  'm1' = 'January',
  'm2' = 'February',
  'm3' = 'March',
  'm4' = 'April',
  'm5' = 'May',
  'm6' = 'June',
  'm7' = 'July',
  'm8' = 'August',
  'm9' = 'September',
  'm10' = 'October',
  'm11' = 'November',
  'm12' = 'December',
}
// January - 31 days
// February - 28 days in a common year and 29 days in leap years
// March - 31 days
// April - 30 days
// May - 31 days
// June - 30 days
// July - 31 days
// August - 31 days
// September - 30 days
// October - 31 days
// November - 30 days
// December - 31 days
