<?php

namespace App\Console\Commands;

use App\Models\Organization\FeeChangesTracking;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Organization\Fee;
use Illuminate\Support\Carbon;

class NextYearFeeValue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nextYearFeeValue:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the fee value with next year fee value';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        $fees = Fee::query()
            ->whereRaw('(next_year_value is not null and next_year_value > 0)')
            ->orWhereRaw('(next_year_vat is not null and next_year_vat > 0)')
            ->orWhereRaw('(next_year_sub_fee_percent is not null and next_year_sub_fee_percent > 0)')
            ->get()
            ->all();

        DB::beginTransaction();
        try {
            $tracking_fees = [];
            foreach ($fees as $fee) {
                /**
                 * @var Fee $fee
                 */
                if ($fee->next_year_sub_fee_percent) {
                    $fee->sub_fee_percent           = $fee->next_year_sub_fee_percent;
                    $fee->next_year_sub_fee_percent = null;
                }
                if ($fee->next_year_value) {
                    $fee->value           = $fee->next_year_value;
                    $fee->next_year_value = null;
                }
                if ($fee->next_year_vat) {
                    $fee->vat           = $fee->next_year_vat;
                    $fee->next_year_vat = null;
                }
                $fee->save();

                // Prepare object for tracking
                $tracking_fees[] = $this->updateFeeForTracking($fee);
            }

            DB::table(FeeChangesTracking::getTableName())->insert($tracking_fees);

            DB::commit();
            $this->info('Next year fee value has been successfully updated.');

        } catch (\Exception $e) {
            DB::rollBack();
            echo 'Error: ' . $e->getMessage();
        }
        return;
    }

    /**
     * @param Fee $fee
     * @return array
     */
    private function updateFeeForTracking(Fee $fee): array
    {
        $feeTracking                = new FeeChangesTracking($fee->toArray());
        $feeTracking->fee_id        = $fee->id;
        $feeTracking->changed_at    = Carbon::now();

        return $feeTracking->toArray();
    }
}
