import { Component, OnInit } from '@angular/core'
import {ActivatedRoute} from '@angular/router'
import {HelperService} from '../../../../services/helper.service'
import {HttpErrorResponse} from '@angular/common/http'
import {PostsService} from "../../../../services/posts.service";

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.scss']
})
export class PostDetailsComponent implements OnInit {

  public postId: number
  public post: any

  constructor(private service: PostsService,
              private activatedRoute: ActivatedRoute) { }

  public getPost(): void {
    this.service.getPost(this.postId).subscribe(
      (resource: any) => {
        this.post = resource
      }, (error: HttpErrorResponse) => {
        console.log(error)
      }
    )
  }

  ngOnInit(): void {
    const params = this.activatedRoute.snapshot.params
    if (!HelperService.isNullOrUndefined(params.id)) {
      this.postId = parseInt(params.id, 0)
      this.getPost()
    }
  }
}
