import { ValidatorFn, FormGroup } from '@angular/forms'

export interface ICompareNumbers {
  first: string,
  second: string,
  comparator: string
  type: 'INT' | 'FLOAT'
}

export function numberComparator(compare: ICompareNumbers): ValidatorFn {
  return (formGroup: FormGroup): {[key: string]: boolean} | null => {
    const firstNo   = formGroup.get(compare.first).value
    const secondNo  = formGroup.get(compare.second).value

    if (firstNo && secondNo) {
      let n1, n2: number
      if (compare.type === 'INT') {
        n1  = parseInt(firstNo as string, 0)
        n2  = parseInt(secondNo as string, 0)
      } else {
        n1  = parseFloat(firstNo as string)
        n2  = parseFloat(secondNo as string)
      }

      switch (true) {
        case compare.comparator === '<':
          if (n1 < n2) {
            return {invalidRange: true}
          }
          break
        case compare.comparator === '<=':
          if (n1 <= n2) {
            return {invalidRange: true}
          }
          break
        case compare.comparator === '>':
          if (n1 > n2) {
            return {invalidRange: true}
          }
          break
        case compare.comparator === '>=':
          if (n1 >= n2) {
            return {invalidRange: true}
          }
          break
        case compare.comparator === '!==':
          if (n1 !== n2) {
            return {invalidRange: true}
          }
          break
        case compare.comparator === '==':
        case compare.comparator === '===':
          if (n1 === n2) {
            return {invalidRange: true}
          }
          break
      }
    }
    return null
  }
}
