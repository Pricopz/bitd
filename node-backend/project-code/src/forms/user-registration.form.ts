"use strict";

import * as Joi from 'joi';
import {FormHelperService} from "../services/form-helper.service";

export interface IUserRegistrationForm
{
    name: string;
    email: string;
    password: string;
    confirmPassword: string;
}

export class UserRegistrationForm implements IUserRegistrationForm
{
    public name: string;
    public email: string;
    public password: string;
    public confirmPassword: string;

    constructor(model?: IUserRegistrationForm)
    {
        if (model) {
            Object.assign(this, model)
        }
    }

    public static fromJson(json: IUserRegistrationForm): UserRegistrationForm
    {
        const _usr          = new UserRegistrationForm();
        _usr.name           = json.name ? json.name : null;
        _usr.email          = json.email ? json.email : null;
        _usr.password       = json.password ? json.password : null;
        _usr.confirmPassword= json.confirmPassword ? json.confirmPassword : null;

        return _usr;
    }

    public static getJoiValidator(): any
    {
        return Joi.object().keys({
            'name': Joi.string().required().messages(FormHelperService.getErrorMessages()),
            'email': Joi.string().email().required().messages(FormHelperService.getErrorMessages()),
            'password': Joi.string().required().pattern(FormHelperService.REGEX_PASSWORD).messages(FormHelperService.getErrorMessages()),
            'confirmPassword': Joi.string().required().valid(Joi.ref('password')).messages(FormHelperService.getErrorMessages()),
        });
    }

    public static getErrors(form: UserRegistrationForm): any | null
    {
        const result = this.getJoiValidator().validate(form, {abortEarly: false});
        if (result.error && result.error.details && result.error.details.length) {
            return FormHelperService.extractErrorsFromJoi(result.error.details);
        }
        return null;
    }
}
