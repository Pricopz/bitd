<?php

return [
    'subject.account.activation'                => 'Activare cont',
    'subject.account.reset.password'            => 'Resetare parolă',
    'subject.enroll.to.organization'            => 'Cerere nouă de înrolare în asociație',
];
