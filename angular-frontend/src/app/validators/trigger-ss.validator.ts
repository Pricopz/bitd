import { ValidatorFn, FormGroup } from '@angular/forms'

export function ss_triggers(ss1: string, ss2: string, values: string[]): ValidatorFn {
  return (formGroup: FormGroup): {[key: string]: boolean} | null => {
    const ss1Trig   = formGroup.get(ss1).value.toString().toUpperCase()
    const ss2Trig   = formGroup.get(ss2).value.toString().toUpperCase()

    if (ss1Trig && ss2Trig) {
      if (ss1Trig === ss2Trig && values.indexOf(ss1Trig) !== -1) {
        return {invalidSSTriggerValues: true}
      }
    }
    return null
  }
}
