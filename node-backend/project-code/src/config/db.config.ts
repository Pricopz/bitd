'use strict';

import * as dotenv from "dotenv";

export const getDBConfig = (host : string) => {
    dotenv.config();
    return {
        host: host,
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE_MAIN,
        waitForConnections: process.env.DB_WAIT_FOR_CONNECTIONS,
        connectionLimit: process.env.DB_CONNECTION_LIMIT,
        queueLimit: process.env.DB_QUEUE_LIMIT
        // options: {enableArithAbort: true}
    };
};
