import { ICalendarSelection } from './ICalendarSelection'

export interface ICalendarLevel {
  label?: string,
  level?: ICalendarSelection,
  info?: string,
  status?: any
}
