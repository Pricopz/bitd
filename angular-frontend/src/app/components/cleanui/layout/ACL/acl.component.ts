import { Component, Input, OnInit } from '@angular/core'
import { Observable } from 'rxjs'

@Component({
  selector: 'cui-acl',
  template: `
    <ng-content *ngIf="authorized"></ng-content>
  `,
})
export class ACLComponent implements OnInit {
  @Input() roles: any[] = []
  authorized: Boolean = false
  role: string = ''

  constructor() {}

  ngOnInit() {
    console.log('xcadasd')
    const user = JSON.parse(localStorage.getItem('user'))
    this.authorized = this.roles.includes(user.role)
  }
}
