import { Pipe, PipeTransform } from '@angular/core'
import { HelperService } from '../services/helper.service'

@Pipe({
  name: 'niceDate',
})
export class NiceDatePipe implements PipeTransform {
  transform(date: string, separator: string = '-', onlyDate: boolean = false): string {
    if (HelperService.isNullOrUndefined(date)) {
      return '-'
    }
    const elemFullDate = date
      .replace('T', ' ')
      .replace('Z', '')
      .split(' ')
    const elemDate = elemFullDate[0].split('-')
    if (elemFullDate.length < 2) {
      return `${elemDate[2]}-${elemDate[1]}-${elemDate[0]}`
    }
    if (onlyDate) {
      return `${elemDate[2]}${separator}${elemDate[1]}${separator}${elemDate[0]}`
    }
    return `${elemDate[2]}${separator}${elemDate[1]}${separator}${elemDate[0]} ${elemFullDate[1]}`
  }
}
