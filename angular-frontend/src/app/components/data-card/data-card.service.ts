import {
  Injectable,
  ComponentFactoryResolver,
  ApplicationRef,
  Injector,
  EmbeddedViewRef,
  ComponentRef,
  Type
} from '@angular/core'

import { DataCardModule } from './data-card.module'
import { DataCardComponent } from './data-card.component'
import { DataCardConfig } from './data-card-config'
import { DataCardInjector } from './data-card-injector'
import { DataCardRef } from './data-card-ref'
import { DataCardOptions } from './data-card.options'

@Injectable({
  providedIn: DataCardModule
})
export class DataCardService {

  public dataCardComponentRef: ComponentRef<DataCardComponent>

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private appRef: ApplicationRef,
              private injector: Injector) { }

  public open(componentType: Type<any>, config: DataCardConfig, options: DataCardOptions = null) {
    const dataCardRef = this.appendDataCardComponentToBody(config, options)
    this.dataCardComponentRef.instance.childComponentType = componentType
    return dataCardRef
  }

  private appendDataCardComponentToBody(config: DataCardConfig, options: DataCardOptions = null) {
    this.removeDataCardComponentFromBody()

    const map = new WeakMap()
    map.set(DataCardConfig, config)

    // add the DialogRef to dependency injection
    const dataCardRef = new DataCardRef()
    map.set(DataCardRef, dataCardRef)

    // we want to know when somebody called the close method
    const sub = dataCardRef.afterClosed.subscribe(() => {
      // close the dialog
      this.removeDataCardComponentFromBody()
      sub.unsubscribe()
    })

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(DataCardComponent)
    const componentRef = componentFactory.create(new DataCardInjector(this.injector, map))
    componentRef.instance.clickOnOpen = true // Needed for outside click
    componentRef.instance.ignoreOutsiderElements = options && options.ignoreOutsiderElements ? options.ignoreOutsiderElements : []
    componentRef.instance.closeOnOutsideClick = options && options.closeOnOutsideClick ? options.closeOnOutsideClick : false
    this.appRef.attachView(componentRef.hostView)

    const domElem             = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement
    this.appendChild(domElem, options)

    this.dataCardComponentRef = componentRef

    this.dataCardComponentRef.instance.onClose.subscribe(() => {
      this.removeDataCardComponentFromBody()
    })

    // return the dialogRef
    return dataCardRef
  }

  private appendChild(domElem: HTMLElement, options: DataCardOptions = null): void {
    if (options && options.appendOnElement) {
      if (options.appendOnElement.id) {
        document.getElementById(options.appendOnElement.id).appendChild(domElem)
        return
      } else if (options.appendOnElement.class) {
        document.getElementsByClassName(options.appendOnElement.class)[0].appendChild(domElem)
        return
      }
    }
    document.body.appendChild(domElem)
    return
  }

  private removeDataCardComponentFromBody() {
    if (!this.dataCardComponentRef) { return }
    this.appRef.detachView(this.dataCardComponentRef.hostView)
    this.dataCardComponentRef.destroy()
  }

  public clearDataCard(): void {
    this.removeDataCardComponentFromBody()
  }
}
