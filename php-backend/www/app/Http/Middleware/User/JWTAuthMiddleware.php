<?php

namespace App\Http\Middleware\User;

use App\Models\Post\User;
use Closure;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\SignatureInvalidException;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Log;
use UnexpectedValueException;

/**
 * Class JWTAuthMiddleware
 * @package App\Http\Middleware\Admin
 */
class JWTAuthMiddleware
{
    /**
     * @param $request
     * @param Closure $next
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function handle($request, Closure $next)
    {
        if (!($token = $request->header('Authorization'))) {
            return response()->json(['error' => trans('validation.auth.token.invalid')], 401);
        }
        $token = str_replace('Bearer ', '', $token);
        try {
            $credentials= JWT::decode($token, env('JWT_SECRET'), ['HS256']);
            $id         = $credentials->uuid;
            $user       = User::query()->where('id', '=', $id)->first();
            # Check User
            if ($user === null) {
                return response()->json(['error' => trans('validation.auth.token.invalid')], 401);
            }

            # Add user data to request
            $request->merge(['user.user' => $user]);

        } catch (ExpiredException $e) {
            return response()->json(['error' => trans('validation.auth.token.invalid')], 401);
        } catch (UnexpectedValueException | SignatureInvalidException $e) {
            return response()->json(['error' => trans('validation.auth.token.invalid')], 401);
        } catch (\Exception $e) {
            # Log to DB to check the problem
            Log::error($e->getMessage());
            return response()->json(['error' => 'Server error.'], 500);
        }
        return $next($request);
    }
}
