<?php

namespace App\Filters\Organization;

use App\Filters\BaseFilter;
use Illuminate\Validation\Rule;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use App\Enum\PersonTypeEnum;

/**
 * Class CashRegisterMemberFilter
 * @package App\Filters\Admin
 */
class CashRegisterMemberFilter extends BaseFilter
{
    /**
     * @var ?string
     */
    public ?string $type = null;
    /**
     * @var ?string
     */
    public ?string $key = null;

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function getQueryBuilder(Builder $builder): Builder
    {
        if (isset($this->key)) {
            $elements = explode(' ', $this->key);
            $builder->where(static function ($query) use ($elements) {
                foreach ($elements as $element) {
                    $query->orWhere(DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'LIKE', "%{$element}%");
                }
            });
        }
        return $builder;
    }

    public function apply(Builder $builder)
    {
        return $this->getEloquentBuilder($builder);
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            'type' => [
                'required',
                'string',
                Rule::in(PersonTypeEnum::asList())
            ],
            'key' => [
                'required',
                'string',
                'min:2'
            ]
        ];
    }

    public function getErrorMessage(): array
    {
        return [
            'type.required'     => trans('organization/validation.field.required'),
            'type.string'       => trans('organization/validation.field.string'),
            'type.in'           => trans('organization/validation.field.invalid'),
            'key.required'      => trans('organization/validation.field.required'),
            'key.string'        => trans('organization/validation.field.string'),
            'key.min'           => trans('organization/validation.field.min'),
        ];
    }
}
