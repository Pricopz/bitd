<?php

namespace App\Filters\Organization;

use App\Filters\BaseFilter;

/**
 * Class MemberPayerFilter
 * @package App\Filters\Organization
 */
class MemberPayerFilter extends BaseFilter
{
    /**
     * @var int|null
     */
    public ?int $year = null;

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            'year' => [
                'nullable',
                'integer',
                'min:' . (date("Y") - 5),
                'max:' . (date("Y") + 5),
            ]
        ];
    }

    /**
     * @return array
     */
    public function getErrorMessage(): array
    {
        return [
            'year.integer'  => trans('organization/validation.field.invalid'),
            'year.min'      => trans('organization/validation.field.min.numeric'),
            'year.max'      => trans('organization/validation.field.max.numeric'),
        ];
    }
}
