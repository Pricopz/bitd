<?php

namespace App\Filters\Organization;

use App\Enum\FeeTypeEnum;
use App\Filters\BaseFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Rule;

/**
 * Class FeeFilter
 * @package App\Filters\Organization
 */
class FeeFilter extends BaseFilter
{
    /**
     * @var string|null
     */
    public ?string $type = null;
    /**
     * @var string|null
     */
    public ?string $member_type = null;
    /**
     * @var int|null
     */
    public ?int $season = null;
    /**
     * @var int|null
     */
    public ?int $is_active = null;
    /**
     * @var int|null
     */
    public ?int $available_online = null;

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            'type' => [
                'nullable',
                'string',
                Rule::in(FeeTypeEnum::asList())
            ],
            'member_type' => [
                'nullable',
                'string',
                Rule::in(['HUNTER', 'FISHERMAN', 'ALL'])
            ],
            'season' => [
                'nullable',
                'date_format:Y',
            ],
            'is_active' => [
                'nullable',
                'integer',
                Rule::in([0, 1])
            ],
            'available_online' => [
                'nullable',
                'integer',
                Rule::in([0, 1])
            ],
        ];
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function getEloquentBuilder(Builder $builder): Builder
    {
        if ($this->type) {
            $builder->where('type', '=', $this->type);
        }
        if ($this->member_type) {
            if ($this->member_type === 'HUNTER') {
                $builder->where('for_hunter', '=', 1);
                $builder->where('for_fisherman', '=', 0);
            } elseif ($this->member_type === 'FISHERMAN') {
                $builder->where('for_hunter', '=', 0);
                $builder->where('for_fisherman', '=', 1);
            }
        }
        if ($this->is_active !== null) {
            $builder->where('is_active', '=', $this->is_active);
        }
        if ($this->available_online !== null) {
            $builder->where('available_online', '=', $this->available_online);
        }
        return $builder;
    }

    /**
     * @return array
     */
    public function getErrorMessage(): array
    {
        return [
            'type.string'               => trans('organization/validation.field.string'),
            'type.in'                   => trans('organization/validation.field.invalid'),
            'member_type.string'        => trans('organization/validation.field.string'),
            'member_type.in'            => trans('organization/validation.field.invalid'),
            'is_active.integer'         => trans('organization/validation.field.number'),
            'is_active.in'              => trans('organization/validation.field.invalid'),
            'available_online.integer'  => trans('organization/validation.field.number'),
            'available_online.in'       => trans('organization/validation.field.invalid'),
        ];
    }
}
