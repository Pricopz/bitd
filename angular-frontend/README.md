### Clean UI Pro Angular Admin Template Preview ###
https://angular.cleanuitemplate.com

### Documentation ###
Please read documentation here https://docs.cleanuitemplate.com

### Quick Start ###
* Install latest node.js: https://nodejs.org​
* Install latest yarn package manager: https://yarnpkg.com/​
* Install angular cli: `npm install -g @angular/cli`
* Install node modules by running terminal command `yarn install`
* Run the app `yarn start` or `ng serve --open`
* Or build production app `yarn build` or `ng build`

### Support ###
Use GitHub Issues for tracking bugs and creating new feature requests or write to [support@sellpixels.com](mailto:support@sellpixels.com).

SPAS-FrontEnd
[![Build Status](https://dev.azure.com/zeljkokatic0981/SPAS/_apis/build/status/SPAS-FrontEnd?branchName=master)](https://dev.azure.com/zeljkokatic0981/SPAS/_build/latest?definitionId=13&branchName=master)