import { Pipe, PipeTransform } from '@angular/core'
import { HelperService } from '../services/helper.service'

@Pipe({
  name: 'formatEventId',
})
export class FormatEventIdPipe implements PipeTransform {
  transform(value: any): string {
    if (HelperService.isNullOrUndefined(value)) {
      return ''
    }
    const newValue = value.toString()
    if (newValue.length > 4) {
      return `${newValue.slice(0, 2)}-${newValue.slice(2, newValue.length)}`
    }
    return newValue
  }
}
