<?php

namespace App\Console;

use App\Console\Commands\Organization\GenerateMemberANPAPermit;
use App\Console\Commands\Organization\InsertMemberFishingFeeToPay;
use App\Console\Commands\Organization\InsertMemberHuntingFeeToPay;
use App\Console\Commands\Organization\ReallocateFeeForMemberWhenPrivilegeChanged;
use App\Console\Commands\Organization\RemoveMemberOnlineWhoDidNotPayed;
use App\Console\Commands\Organization\ResetMemberPrivilegeStatus;
use App\Console\Commands\Organization\SuppressFishingFee;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel
 * @package App\Console
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\NextYearFeeValue::class,
        InsertMemberHuntingFeeToPay::class,
        InsertMemberFishingFeeToPay::class,
        ResetMemberPrivilegeStatus::class,
        SuppressFishingFee::class,
        ReallocateFeeForMemberWhenPrivilegeChanged::class,
        GenerateMemberANPAPermit::class,
        RemoveMemberOnlineWhoDidNotPayed::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('nextYearFeeValue:update')
                 ->yearlyOn(5, 8, '16:22')
                 ->timezone('Europe/Bucharest');
    }
}
