import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { DataCardComponent } from './data-card.component'
import { InsertionDirective } from './insertion.directive'


@NgModule({
  declarations: [DataCardComponent, InsertionDirective],
  imports: [
    CommonModule
  ],
  entryComponents: [DataCardComponent]
})
export class DataCardModule { }
