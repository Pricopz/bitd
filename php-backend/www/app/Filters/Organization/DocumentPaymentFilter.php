<?php

namespace App\Filters\Organization;

use App\Enum\PaymentPaperTypeEnum;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Rule;

/**
 * Class DocumentPaymentFilter
 * @package App\Filters\Organization
 */
class DocumentPaymentFilter extends OrganizationCashDeskTransactionFilter
{
    /**
     * @param Builder $builder
     * @return Builder
     */
    public function getEloquentBuilder(Builder $builder): Builder
    {
        if ($this->type) {
            $builder->where('type', '=', $this->type);
        } else {
            $builder->whereIn('type', [PaymentPaperTypeEnum::RECEIPT, PaymentPaperTypeEnum::INVOICE, PaymentPaperTypeEnum::PROOF_OF_PAYMENT]);
        }
        if ($this->start_date) {
            $builder->where('created_at', '>=', $this->start_date);
        }
        if ($this->end_date) {
            $builder->where('created_at', '<=', $this->end_date);
        }
        return $builder;
    }

    /**
     * @return array[]
     */
    public function getRules(): array
    {
        return [
            'type' => [
                'nullable',
                Rule::in([PaymentPaperTypeEnum::RECEIPT, PaymentPaperTypeEnum::INVOICE, PaymentPaperTypeEnum::PROOF_OF_PAYMENT])
            ],
            'start_date' => [
                'nullable',
                'date',
                'date_format:Y-m-d',
                'before_or_equal:end_date',
            ],
            'end_date' => [
                'nullable',
                'date',
                'date_format:Y-m-d',
                'before_or_equal:' . date('Y-m-d'),
            ],
        ];
    }
}
