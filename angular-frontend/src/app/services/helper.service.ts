import { Injectable } from '@angular/core'
import { formatDate } from '@angular/common'

@Injectable({
  providedIn: 'root',
})
export class HelperService {
  public static dateToString(
    date: Date = null,
    format: string = 'yyyy-MM-dd',
    locale: string = 'en',
  ): string {
    if (date) {
      return formatDate(date, format, locale)
    }
    return ''
  }

  public static isNullOrUndefined(value: any): boolean {
    return value === null || value === undefined
  }

  public static deepCopy(object: any) {
    return JSON.parse(JSON.stringify(object))
  }

  public static inArrayByCriteria(items: any[], by: string, value: any): boolean {
    if (items.length) {
      for (const i in items) {
        if (items[i] && items[i][by] && items[i][by] === value) {
          return true
        }
      }
    }
    return false
  }

  public static inArray(items: any[], value: any, insensitive: boolean = false): boolean {
    if (items.length) {
      if (insensitive) {
        for (const i in items) {
          if (
            !HelperService.isNullOrUndefined(items[i]) &&
            items[i].toLowerCase() === value.toLowerCase()
          ) {
            return true
          }
        }
      } else {
        for (const i in items) {
          if (!HelperService.isNullOrUndefined(items[i]) && items[i] === value) {
            return true
          }
        }
      }
    }
    return false
  }

  public static arrayIntersect(arr1: any[], arr2: any[], by: string): any[] | null {
    if (arr1.length && arr2.length) {
      const intersect = []
      for (const i in arr1) {
        if (arr1[i] && HelperService.inArrayByCriteria(arr2, by, arr1[i])) {
          intersect.push(arr1[i])
        }
      }
      return intersect.length > 0 ? intersect : null
    }
    return null
  }

  public static complexObjectToQuery(obj: any, prefix: string = null): string {
    if (obj && Object.keys(obj).length) {
      const fields = []
      for (const i in obj) {
        if (obj.hasOwnProperty(i)) {
          if (typeof obj[i] === 'object') {
            const query = HelperService.complexObjectToQuery(
              obj[i],
              prefix ? prefix + '[' + i + ']' : i,
            )
            if (query !== '') {
              fields.push(query)
            }
          } else {
            if (obj[i] !== null) {
              fields.push(
                (prefix ? prefix + '[' + i + ']' : i) +
                  '=' +
                  (typeof obj[i] === 'undefined' ? '' : encodeURIComponent(obj[i])),
              )
            }
          }
        }
      }
      if (fields.length > 0) {
        return fields.join('&')
      }
    }
    return ''
  }

  public static isEmptyObjectByValues(object: any): boolean {
    if (object && Object.keys(object).length) {
      for (const i in object) {
        if (object.hasOwnProperty(i)) {
          if (object[i] !== undefined && object[i] !== null) {
            return false
          }
        }
      }
    }
    return true
  }

  public static findObjectByAttributeValue(
    objects: any[],
    attribute: string,
    value: string | number | null,
  ): any {
    for (let i = 0; i < objects.length; i++) {
      const object = objects[i]
      if (attribute in object && object[attribute] === value) {
        return object
      }
    }
    return null
  }

  public static copyAndRemoveItem(items: any[], item: any): any[] {
    const copied = this.deepCopy(items)
    const index = copied.indexOf(item)
    if (index > -1) {
      copied.splice(index, 1)
    }
    return copied
  }

  public static getColorClassForBullet(value: any): string {
    const newVal = parseFloat(value)
    if (newVal >= 0 && newVal < 3) {
      return 'circle-red'
    } else if (newVal >= 3 && newVal < 6) {
      return 'circle-orange'
    } else if (newVal >= 6 && newVal < 9) {
      return 'circle-yellow'
    } else if (newVal >= 9 && newVal < 11) {
      return 'circle-green'
    } else if (newVal >= 11) {
      return 'circle-gray'
    }
    return ''
  }

  public static getColorBulletHTML(value: any): string {
    if (!HelperService.isNullOrUndefined(value)) {
      return '<span class="circle ' + this.getColorClassForBullet(value) + '">' + value + '</span>'
    }
    return '-'
  }

  public static getObjectWithoutNullValues(object: any): any {
    const cleanObject = {}
    if (object && Object.keys(object).length) {
      for (const i in object) {
        if (object.hasOwnProperty(i)) {
          if (object[i] !== undefined && object[i] !== null) {
            cleanObject[i] = object[i]
          }
        }
      }
    }
    return cleanObject
  }

  public static getDeviationBulletHTML(value: any): string {
    if (!HelperService.isNullOrUndefined(value)) {
      const fill = parseInt(value, 0) === 2 ? 'X' : '.'
      return (
        '<span class="circle small-circle ' +
        this.getColorClassForDeviation(value) +
        '">' +
        fill +
        '</span>'
      )
    }
    return '-'
  }

  public static getColorClassForDeviation(value: any): string {
    const newVal = parseInt(value, 0)
    if (newVal === 1) {
      return 'circle-red-outline'
    } else if (newVal === 2) {
      return 'circle-red'
    } else if (newVal === 3) {
      return 'circle-green circle-green-text'
    } else if (newVal === 4) {
      return 'circle-yellow circle-yellow-text'
    }
    return ''
  }

  public static escapeRegExp(strToEscape) {
    // Escape special characters for use in a regular expression
    return strToEscape.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&')
  }

  public static trim(origString, charToTrim) {
    charToTrim = this.escapeRegExp(charToTrim)
    const regEx = new RegExp('^[' + charToTrim + ']+|[' + charToTrim + ']+$', 'g')
    return origString.replace(regEx, '')
  }

  public static msToMinSecMilliFormat(ms: number): string {
    const min = Math.floor(ms / (60 * 1000))
    const sec = Math.floor((ms - min * (60 * 1000)) / 1000)
    const milli = ms - (min * (60 * 1000) + sec * 1000)
    return `${min < 10 ? '0' + min : min}:${sec < 10 ? '0' + sec : sec},${milli}`
  }

  public static getFileExtension(file: File): string {
    return `.${file.name.split('.')[1].toLowerCase()}`
  }

  public static copyMessage(val: string) {
    const selBox = document.createElement('textarea')
    selBox.style.position = 'fixed'
    selBox.style.left = '0'
    selBox.style.top = '0'
    selBox.style.opacity = '0'
    selBox.value = val
    document.body.appendChild(selBox)
    selBox.focus()
    selBox.select()
    document.execCommand('copy')
    document.body.removeChild(selBox)
  }

  public static getLastTestValidationIco(status: string): string | null {
    let icoName = ''
    switch (true) {
      case status === 'VALID':
        icoName = 'valid'
        break
      case status === 'OVERDUE':
        icoName = 'overdue'
        break
      case status === 'FAILED':
        icoName = 'failed'
        break
      case status === 'TEST REQUIRED':
        icoName = 'test_required'
        break
      case status === 'TEST REQ.':
        icoName = 'test_req'
        break
      default:
        return null
    }
    return `assets/images/icons/${icoName}.PNG`
  }

  public static getBodyTableHeight(heightToReduce: number, minHeight: number = 560): number {
    const pageHeight = window.innerHeight
    const tableHeight = pageHeight - heightToReduce
    return tableHeight < minHeight ? minHeight : tableHeight
  }
}
