<?php
    use SimpleSoftwareIO\QrCode\Facades\QrCode;
    use Carbon\Carbon;
?>
<!DOCTYPE html>
<html lang="ro">
    <head>
        <title>Permis ANPA</title>
        <style type='text/css'>
            * {
                margin: 0;
                padding: 0;
            }
            body {
                width: 100%;
                margin: 0;
                padding: 0;
            }
            p {
                margin:0;
                padding:0;
            }
            .page-one {
                margin: 0;
                padding: 10mm;
                width: 100%;
                height: 100%;
            }
            .permit-bg {
                width: 100%;
                height: 100%;
                background-image: url("{{ $path }}/permisAsociatie.png");
                background-repeat: no-repeat;
                background-position-x: 0;
                background-position-y: 0;
                background-size: 100%;
            }
            .permit-info {
                width: 50%;
                padding: 25mm 0 0 3mm;
            }
            .permit-series {
                text-align: center;
                padding: 0 0 2mm;
                font-weight: bold;
            }
            .page-two {
                margin: 0;
                height: 100%;
                width: 100%;
                background-image: url("{{ $path }}/fisa.jpg");
                background-repeat: no-repeat;
                background-position-x: -10mm;
                background-position-y: -10mm;
                background-size: 100%;
            }
            table.permit-info-table tr {
                padding: 0;
                margin: 0;
            }
            table.permit-info-table tr td {
                padding: 0;
                margin: 0;
            }
        </style>
    </head>
    <body>
        <div class="page page-one">
            <div class="permit-bg">
                <div class="permit-info">
                    <div class="permit-series">
                        Seria {{ $permit->number }} din {!! Carbon::parse($permit->date_issue)->format('d.m.Y') !!}
                    </div>
                    <div class="permit-data">
                        <table>
                            <tr>
                                <td>
                                    <img src="data:image/svg;base64, {!! base64_encode(QrCode::format('svg')->size(90)->errorCorrection('H')->style('square')->generate($qrCodeText)) !!}">
                                </td>
                                <td style="vertical-align: bottom">
                                    <table class="permit-info-table">
                                        <tr>
                                            <td>Nume:</td>
                                            <td><strong>{{ $member->last_name }}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Prenume:</td>
                                            <td><strong>{{ $member->first_name }}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>CNP:</td>
                                            <td><strong>{{ $member->cnp }}</strong></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Asociatie:</strong></td>
                                            <td><strong>{{ $organizationName }}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Valabilitate:</td>
                                            <td>{!! Carbon::parse($permit->date_issue)->format('d.m.Y') !!} - {!! Carbon::parse($permit->date_expire)->format('d.m.Y') !!}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="page page-two">
            &nbsp;
        </div>
    </body>
</html>
