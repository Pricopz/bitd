import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { select, Store } from '@ngrx/store'
import * as Reducers from 'src/app/store/reducers'
import { BasicAuthService } from '../../../services/basic-auth.service'

@Component({
  selector: 'cui-system-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public form: FormGroup
  public logo: String


  public constructor(
    private fb: FormBuilder,
    public authService: BasicAuthService,
    private store: Store<any>,
  ) {
    this.form = fb.group({
      email: [null, [Validators.required, Validators.minLength(4), Validators.email]],
      password: [null, [Validators.required]],
      remember: [1],
    })
    this.store.pipe(select(Reducers.getSettings)).subscribe(state => {
      this.logo = state.logo
    })
  }

  public get email() {
    return this.form.controls.email
  }

  public get password() {
    return this.form.controls.password
  }

  public get remember() {
    return this.form.controls.remember
  }

  public async onSubmitForm() {
    this.email.markAsDirty()
    this.email.updateValueAndValidity()
    this.password.markAsDirty()
    this.password.updateValueAndValidity()
    this.remember.markAsDirty()
    this.remember.updateValueAndValidity()
    if (this.form.invalid) {
      return
    }
    await this.authService.login(this.email.value, this.password.value, this.remember.value)
  }

  public async ngOnInit() {}
}
