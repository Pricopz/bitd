<?php

namespace App\Enum;

/**
 * Class AccidentInsuranceStatusEnum
 * @package App\Enum
 */
abstract class AccidentInsuranceStatusEnum extends AbstractEnum
{
    public const ACTIVE                  = 'ACTIVE';
    public const CANCELED_LOST           = 'CANCELED_LOST';
    public const CANCELED_ANOTHER_REASON = 'CANCELED_ANOTHER_REASON';
}
