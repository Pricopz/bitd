<?php

namespace App\Exceptions;

/**
 * Class EntitySavingException
 * @package App\Exceptions
 */
class EntitySavingException extends \Exception
{

}
