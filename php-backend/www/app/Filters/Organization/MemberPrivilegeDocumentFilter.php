<?php

namespace App\Filters\Organization;

use App\Enum\MemberPrivilegeTypeEnum;
use App\Filters\BaseFilter;
use Illuminate\Validation\Rule;

/**
 * Class MemberPrivilegeDocumentFilter
 * @package App\Filters\Organization
 */
class MemberPrivilegeDocumentFilter extends BaseFilter
{
    /**
     * @var string
     */
    public string $type = '';

    /**
     * @return array[]
     */
    public function getRules(): array
    {
        return [
            'type' => [
                'required',
                Rule::in(MemberPrivilegeTypeEnum::asList())
            ],
        ];
    }

    /**
     * @return array
     */
    public function getErrorMessage(): array
    {
        return [
            'type.required'     => trans('admin/validation.field.required'),
            'type.in'           => trans('admin/validation.field.in'),
        ];
    }
}
