<?php

namespace App\Filters\Organization;

use App\Enum\FishingPermitsStatusEnum;
use App\Enum\HuntingPermitsStatusEnum;
use App\Enum\MemberStatusEnum;
use App\Filters\BaseFilter;
use App\Models\FishingPermit;
use App\Models\HuntingPermit;
use App\Models\Organization\Member;
use App\Models\Organization\MemberFeePaymentTracking;
use App\Models\Organization\MemberFishingPermitsStatusTracking;
use App\Models\Organization\MemberHuntingPermitsStatusTracking;
use App\Models\Organization\MemberPrivilegeTracking;
use App\Models\Organization\MemberStatusTracking;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

/**
 * Class MemberFilter
 * @package App\Filters\Organization
 */
class MemberFilter extends BaseFilter
{
    /**
     * @var string|null
     */
     public ?string $key = null;
    /**
     * @var array
     */
    public array $types = []; # h, f, hf
    /**
     * @var string|null
     */
    public ?string $dob_start = null;
    /**
     * @var string|null
     */
    public ?string $dob_end = null;
    /**
     * @var ?int
     */
    public ?int $county_id = null;
    /**
     * @var ?int
     */
    public ?int $city_id = null;
    /**
     * @var int|null
     */
    public ?int $group_id = null;
    /**
     * @var int|null
     */
    public ?int $subgroup_id = null;
    /**
     * @var array|null
     */
    public ?array $g1 = null;
    /**
     * @var array|null
     */
    public ?array $g2 = null;
    /**
     * @var array|null
     */
    public ?array $g3 = null;
    /**
     * @var array|null
     */
    public ?array $g4 = null;
    /**
     * @var array|null
     */
    public ?array $g5 = null;
    /**
     * @var array|null
     */
    public ?array $g6 = null;

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function getQueryBuilder(Builder $builder): Builder
    {
        $t1         = Member::getTableName();
        $t2         = MemberPrivilegeTracking::getTableName();
        $t3         = MemberStatusTracking::getTableName();
        $t4         = MemberFeePaymentTracking::getTableName();
        $t5         = MemberHuntingPermitsStatusTracking::getTableName();
        $t6         = MemberFishingPermitsStatusTracking::getTableName();
        $self       = $this;
        if ($this->key) {
            $builder->where(static function ($query) use ($self) {
                $query->where('first_name', 'like', "%{$self->key}%")
                    ->orWhere('last_name', 'like', "%{$self->key}%")
                    ->orWhere('phone', 'like', "%{$self->key}%")
                    ->orWhere('email', 'like', "%{$self->key}%");
            });
        }
        if (count($this->types) > 0) {
            $builder->where(static function ($query) use ($t1, $self) {
                foreach ($self->types as $type) {
                    if ($type === 'h') {
                        $query->orWhere("{$t1}.is_hunter", '=', 1);
                    }
                    if ($type === 'f') {
                        $query->orWhere("{$t1}.is_fisherman", '=', 1);
                    }
                    if ($type === 'hf') {
                        $query->orWhere(static function ($query) use ($t1) {
                            $query->where("{$t1}.is_hunter", '=', 1);
                            $query->where("{$t1}.is_fisherman", '=', 1);
                        });
                    }
                }
            });
        }
        if ($this->dob_start) {
            $builder->where("{$t1}.date_of_birth", '>=', $this->dob_start);
        }
        if ($this->dob_end) {
            $builder->where("{$t1}.date_of_birth", '<=', $this->dob_end);
        }
        if ($this->county_id) {
            $builder->where("{$t1}.county_id", '=', $this->county_id);
        }
        if ($this->city_id) {
            $builder->where("{$t1}.city_id", '=', $this->city_id);
        }
        if ($this->group_id) {
            $builder->where("{$t1}.group_id", '=', $this->group_id);
        }
        if ($this->subgroup_id) {
            $builder->where("{$t1}.subgroup_id", '=', $this->subgroup_id);
        }
        # Groups
        if ($this->g1) { # Group Member with discounts -> Should be OK

            if (isset($this->g1['type'], $this->g1['year'])) {
                # Add Join Condition
                $builder->leftJoin($t2, "{$t2}.member_id", '=', "{$t1}.id");
                # Add Where Condition
                if (in_array($this->g1['type'], ['r', 's', 'a'])) {
                    $column = $this->getGroup1ColumnsMapping()[strtolower($this->g1['type'])];
                    $builder->where("{$t2}.{$column}", '=', 1);
                    $builder->where(DB::raw("YEAR({$t2}.start_season_date)"), '=', $this->g1['year']);
                } else {
                    $builder->where(static function ($query) use ($t2) {
                        $query->where("{$t2}.is_privileged", '=', 1);
                        $query->orWhere("{$t2}.is_employee", '=', 1);
                    });
                    $builder->where(DB::raw("YEAR({$t2}.start_season_date)"), '=', $this->g1['year']);
                }
            }

        } elseif ($this->g2) { # Group Member State -> Should be OK Now... ish..

            if (isset($this->g2['type'], $this->g2['date'])) {
                # - sunt 3 cazuri
                # -> 1) Cand se uita intre dati
                # -> 2) Cand e o singura inregistrare, prima
                # -> 3) Cand se uita la ultima inregistrare
                # Add Join Condition
                $builder->leftJoin($t3, "{$t3}.member_id", '=', "{$t1}.id");
                # Add Where Condition
                $builder->where(static function ($query) use ($t3, $self) {
                    $query->where(static function ($query) use ($t3, $self) {
                        $query->where("{$t3}.is_single_row", '=', 1);
                        $query->where("{$t3}.current_status", '=', $self->getGroup2And3StatusMapping()[$self->g2['type']] ?? null);
                        $query->where("{$t3}.current_status_changed_date", '<=', $self->g2['date']);
                    });
                    $query->orWhere(static function ($query) use ($t3, $self) {
                        $query->where("{$t3}.prev_status", '=', $self->getGroup2And3StatusMapping()[$self->g2['type']] ?? null);
                        $query->where("{$t3}.prev_status_changed_date", '<=', $self->g2['date']);
                        $query->where("{$t3}.current_status_changed_date", '>', $self->g2['date']);
                    });
                    $query->orWhere(static function ($query) use ($t3, $self) {
                        $query->where("{$t3}.is_last_row", '=', 1);
                        $query->where("{$t3}.current_status", '=', $self->getGroup2And3StatusMapping()[$self->g2['type']] ?? null);
                        $query->where("{$t3}.current_status_changed_date", '<=', $self->g2['date']);
                    });
                });
            }

        } elseif ($this->g3) { # Group Member State Changed -> Should be OK

            if (isset($this->g3['type'], $this->g3['start_date'], $this->g3['end_date'])) {
                # Add Join Condition
                $builder->leftJoin($t3, "{$t3}.member_id", '=', "{$t1}.id");
                # Add Where Condition
                $builder->where("{$t3}.prev_status", '=', $this->getGroup2And3StatusMapping()[$this->g3['type']] ?? null);
                $builder->where("{$t3}.current_status_changed_date", '>=', $this->g3['start_date']);
                $builder->where("{$t3}.current_status_changed_date", '<=', $this->g3['end_date']);
            }

        } elseif ($this->g4) { # Group Hunting Permit Status -> Should be OK Now... ish..

            if (isset($this->g4['type'], $this->g4['date'])) {
                # Add Join Condition
                $builder->leftJoin($t5, "{$t5}.member_id", '=', "{$t1}.id");
                # Add Where Condition
                $builder->where(static function ($query) use ($t5, $self) {
                    $query->where(static function ($query) use ($t5, $self) {
                        $query->where("{$t5}.is_single_row", '=', 1);
                        $query->where("{$t5}.current_status", '=', $self->getGroup4StatusMapping()[$self->g4['type']] ?? null);
                        $query->where("{$t5}.current_status_changed_date", '<=', $self->g4['date']);
                    });
                    $query->orWhere(static function ($query) use ($t5, $self) {
                        $query->where("{$t5}.prev_status", '=', $self->getGroup4StatusMapping()[$self->g4['type']] ?? null);
                        $query->where("{$t5}.prev_status_changed_date", '<=', $self->g4['date']);
                        $query->where("{$t5}.current_status_changed_date", '>', $self->g4['date']);
                    });
                    $query->orWhere(static function ($query) use ($t5, $self) {
                        $query->where("{$t5}.is_last_row", '=', 1);
                        $query->where("{$t5}.current_status", '=', $self->getGroup4StatusMapping()[$self->g4['type']] ?? null);
                        $query->where("{$t5}.current_status_changed_date", '<=', $self->g4['date']);
                    });
                });
            }

        } elseif ($this->g5) { # Group Fishing Permit Status -> Should be OK Now... ish..

            if (isset($this->g5['type'], $this->g5['date'])) {
                # Add Join Condition
                $builder->leftJoin($t6, "{$t6}.member_id", '=', "{$t1}.id");
                # Add Where Condition
                $builder->where(static function ($query) use ($t6, $self) {
                    $query->where(static function ($query) use ($t6, $self) {
                        $query->where("{$t6}.is_single_row", '=', 1);
                        $query->where("{$t6}.current_status", '=', $self->getGroup5StatusMapping()[$self->g5['type']] ?? null);
                        $query->where("{$t6}.current_status_changed_date", '<=', $self->g5['date']);
                    });
                    $query->orWhere(static function ($query) use ($t6, $self) {
                        $query->where("{$t6}.prev_status", '=', $self->getGroup5StatusMapping()[$self->g5['type']] ?? null);
                        $query->where("{$t6}.prev_status_changed_date", '<=', $self->g5['date']);
                        $query->where("{$t6}.current_status_changed_date", '>', $self->g5['date']);
                    });
                    $query->orWhere(static function ($query) use ($t6, $self) {
                        $query->where("{$t6}.is_last_row", '=', 1);
                        $query->where("{$t6}.current_status", '=', $self->getGroup5StatusMapping()[$self->g5['type']] ?? null);
                        $query->where("{$t6}.current_status_changed_date", '<=', $self->g5['date']);
                    });
                });
            }

        } elseif ($this->g6) { # Group Payment Status -> Should be OK

            if (isset($this->g6['type'])) {
                if ($havingRaw = $this->getGroup6HavingCondition()) {
                    if (isset($this->g6['year'])) {
                        $builder->where('year', '=', $this->g6['year']);
                    } elseif (isset($this->g6['date'])) {
                        $builder->where("{$t4}.pay_date", '<=', $self->g6['date']);
                    }
                    $builder->havingRaw($havingRaw);
                }
            }

        }
        return $builder;
    }

    /**
     * @return array
     */
    public function getGroup1ColumnsMapping(): array
    {
        return [
            'r' => 'is_retired',
            's' => 'is_student',
            'a' => 'is_aged',
        ];
    }

    /**
     * @return array
     */
    public function getGroup2And3StatusMapping(): array
    {
        return [
            'a' => MemberStatusEnum::ACTIVE,
            'i' => MemberStatusEnum::INACTIVE,
            's' => MemberStatusEnum::SUSPENDED,
            'e' => MemberStatusEnum::ENROLLED
        ];
    }

    /**
     * @return array
     */
    public function getGroup4StatusMapping(): array
    {
        return [
            'r' => HuntingPermitsStatusEnum::REQUESTED,
            'a' => HuntingPermitsStatusEnum::ACTIVE,
        ];
    }

    /**
     * @return array
     */
    public function getGroup5StatusMapping(): array
    {
        return [
            'u' => FishingPermitsStatusEnum::UNREQUESTED,
            'r' => FishingPermitsStatusEnum::REQUESTED,
            'a' => FishingPermitsStatusEnum::ACTIVE,
        ];
    }

    /**
     * @return ?string
     */
    public function getGroup6HavingCondition(): ?string
    {
        $map = [
            'i' => 'total_paid_amount = 0',
            'p' => '(total_paid_amount > 0) and (total_paid_amount < fees_amount)',
            'ip' => '(total_paid_amount = 0 or (total_paid_amount > 0) and (total_paid_amount < fees_amount))',
        ];
        return $map[$this->g6['type']] ?? null;
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            'key' => [
                'nullable',
            ],
            'types' => [
                'array',
                Rule::in(['h', 'f', 'hf']),
            ],
            'dob_start' => [
                'nullable',
                'date',
                'date_format:Y-m-d',
            ],
            'dob_end' => [
                'nullable',
                'date',
                'date_format:Y-m-d',
            ],
            # Group 1 - Member Privileged
            'g1' => [
                'nullable',
                'array'
            ],
            'g1.type' => [
                Rule::in(['r', 's', 'a', 'o']),
            ],
            'g1.year' => [
                'date_format:Y',
            ],
            # Group 2 - Member Status
            'g2' => [
                'nullable',
                'array'
            ],
            'g2.type' => [
                Rule::in(['a', 'i', 's', 'e']),
            ],
            'g2.date' => [
                'date_format:Y-m-d',
            ],
            # Group 3 - Member Changed Status
            'g3' => [
                'nullable',
                'array'
            ],
            'g3.type' => [
                Rule::in(['a', 'i', 's', 'e']),
            ],
            'g3.start_date' => [
                'date_format:Y-m-d',
            ],
            'g3.end_date' => [
                'date_format:Y-m-d',
            ],
            # Group 4 - Hunting Permit Status
            'g4' => [
                'nullable',
                'array'
            ],
            'g4.type' => [
                Rule::in(['r', 'a']),
            ],
            'g4.date' => [
                'date_format:Y-m-d',
            ],
            # Group 5 - Fishing Permit Status
            'g5' => [
                'nullable',
                'array'
            ],
            'g5.type' => [
                Rule::in(['u', 'r', 'a']),
            ],
            'g5.date' => [
                'date_format:Y-m-d',
            ],
            # Group 6 - Payment Fee Payment Status
            'g6' => [
                'nullable',
                'array'
            ],
            'g6.type' => [
                Rule::in(['i', 'p', 'ip']),
            ],
            'g6.year' => [
                'date_format:Y',
            ],
            'g6.date' => [
                'date_format:Y-m-d',
            ],
        ];
    }


    public function getErrorMessage(): array
    {
        return [
            'dob_start.date'                => trans('organization/validation.field.date'),
            'dob_start.date_format'         => trans('organization/validation.field.date.invalid'),
            'dob_end.date'                  => trans('organization/validation.field.date'),
            'dob_end.date_format'           => trans('organization/validation.field.date.invalid'),

            'g1.type.required'              => trans('organization/validation.field.required'),
            'g1.type.in'                    => trans('organization/validation.field.in'),
            'g1.year.required'              => trans('organization/validation.field.required'),
            'g1.year.date_format'           => trans('organization/validation.field.date_format'),

            'g2.type.required'              => trans('organization/validation.field.required'),
            'g2.type.in'                    => trans('organization/validation.field.in'),
            'g1.date.required'              => trans('organization/validation.field.required'),
            'g1.date.date_format'           => trans('organization/validation.field.date_format'),

            'g3.type.required'              => trans('organization/validation.field.required'),
            'g3.type.in'                    => trans('organization/validation.field.in'),
            'g3.start_date.required'        => trans('organization/validation.field.required'),
            'g3.start_date.date_format'     => trans('organization/validation.field.date_format'),
            'g3.end_date.required'          => trans('organization/validation.field.required'),
            'g3.end_date.date_format'       => trans('organization/validation.field.date_format'),

            'g4.type.required'              => trans('organization/validation.field.required'),
            'g4.type.in'                    => trans('organization/validation.field.in'),
            'g4.date.required'              => trans('organization/validation.field.required'),
            'g4.date.date_format'           => trans('organization/validation.field.date_format'),

            'g5.type.required'              => trans('organization/validation.field.required'),
            'g5.type.in'                    => trans('organization/validation.field.in'),
            'g5.date.required'              => trans('organization/validation.field.required'),
            'g5.date.date_format'           => trans('organization/validation.field.date_format'),

            'g6.type.required'              => trans('organization/validation.field.required'),
            'g6.type.in'                    => trans('organization/validation.field.in'),
            'g6.date.required'              => trans('organization/validation.field.required'),
            'g6.date.date_format'           => trans('organization/validation.field.date_format'),
            'g6.year.date_format'           => trans('organization/validation.field.date_format'),
        ];
    }
}
