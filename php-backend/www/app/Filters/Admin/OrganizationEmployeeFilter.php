<?php

namespace App\Filters\Admin;

use App\Filters\BaseFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Rule;

/**
 * Class OrganizationEmployeeFilter
 * @package App\Filters\Admin
 */
class OrganizationEmployeeFilter extends BaseFilter
{
    /**
     * @var ?int[]
     */
    public ?array $organization_ids = null;

    /**
     * @param $builder
     * @return mixed
     */
    public function getEloquentBuilder(Builder $builder): Builder
    {   
        $self = $this;
        if(isset($this->organization_ids) && (count($this->organization_ids) > 0)) {
            $builder->join('organization_to_employee', function ($join) {
                $join->on('organization_employees.id', '=', 'organization_to_employee.employee_id')
                     ->whereIn('organization_to_employee.organization_id', $this->organization_ids);
            })->groupBy('organization_employees.public_id');
        } else {
            $builder->leftJoin('organization_to_employee', function ($join) {
                $join->on('organization_employees.id', '=', 'organization_to_employee.employee_id');
            })->groupBy('organization_employees.public_id');
        }
        return $builder;
    }

    public function apply(Builder $builder)
    {
        $self = $this;
        $builder = $this->getEloquentBuilder($builder);
        return $builder;
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            'organization_ids' => [
                'array'
            ],
            'organization_ids.*' => [
                'integer',
                'exists:organizations,id'
            ]
        ];
    }

    public function getErrorMessage() : array
    {
        return [
            'organization_ids.*.integer'    => trans('admin/validation.field.integer'),
            'organization_ids.*.exists'     => trans('admin/validation.field.exists'),
        ];
    }
}
