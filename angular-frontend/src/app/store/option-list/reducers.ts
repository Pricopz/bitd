import * as actions from './actions'

export const initialState: object = {
  options: null
}

export function reducer(state = initialState, action: actions.Actions): object {
  switch (action.type) {
    case actions.SAVE_OPTIONS:
      return {
        options: action.payload
      }
    case actions.CLEAR_OPTIONS:
      return {
        options: null
      }
    default:
      return state
  }
}

export const getSettings = (state: any) => state
