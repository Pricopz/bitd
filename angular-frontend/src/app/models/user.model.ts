export interface IUserModel {
  id?: number
  name?: string
  email?: string
}

export class UserModel implements IUserModel {
  public id: number
  public name: string
  public email: string

  constructor(model?: IUserModel) {
    if (model) {
      Object.assign(this, model)
    }
  }

  public static fromJson(json: IUserModel): UserModel {
    const _usr = new UserModel()
    _usr.id = json.id ? json.id : null
    _usr.name = json.name ? json.name : null
    _usr.email = json.email ? json.email : null
    return _usr
  }
}
