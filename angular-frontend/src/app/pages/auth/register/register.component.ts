import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { BasicAuthService } from "../../../services/basic-auth.service";
import {matchPassword} from "../../../validators/match-password.validator";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public form: FormGroup
  public name: FormControl
  public email: FormControl
  public password: FormControl
  public confirmPassword: FormControl

  constructor(private authService: BasicAuthService) { }

  public initControls(): void {
    this.name = new FormControl(null, [Validators.required])
    this.email = new FormControl(null, [Validators.required, Validators.email])
    this.password = new FormControl(null, [Validators.required])
    this.confirmPassword = new FormControl(null, [Validators.required])
  }

  public initForm(): void {
    this.form = new FormGroup({
      name: this.name,
      email: this.email,
      password: this.password,
      confirmPassword: this.confirmPassword,
    },
      {
        validators: [
          matchPassword('password', 'confirmPassword')
        ]
      })
  }

  public async onSubmitForm() {
    this.name.markAsDirty()
    this.name.updateValueAndValidity()
    this.email.markAsDirty()
    this.email.updateValueAndValidity()
    this.password.markAsDirty()
    this.password.updateValueAndValidity()
    this.confirmPassword.markAsDirty()
    this.confirmPassword.updateValueAndValidity()
    if (this.form.invalid) {
      return
    }
    await this.authService.create(this.name.value, this.email.value, this.password.value, this.confirmPassword.value)
  }

  ngOnInit(): void {
    this.initControls()
    this.initForm()
  }
}
