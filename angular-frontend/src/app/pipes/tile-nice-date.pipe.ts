import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'tileNiceDate',
})
export class TileNiceDatePipe implements PipeTransform {
  transform(date: string): string {
    if (date.indexOf('-') === -1) {
      return date
    }
    const elemDate = date.split('-')
    const year = elemDate[0]
    const month = elemDate[1]
    const day = elemDate[2]
    return `${day}.${month}-${year.slice(2, 4)}`
  }
}
