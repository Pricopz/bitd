Clone this package into your machine    
`$ git clone  https://Pricopz@bitbucket.org/Pricopz/bitd.git`


Run below command to create the services    
`$ docker-compose up -d`

You can see running container using     
`$ docker ps`


## node-backend:  ##

you can access nodejs at http://localhost:8081/api/v1


## php-backend: ##

you can access nodejs at http://localhost/public/api/v1



## angular-fronted:  ##

run commands:   
`cd angular-frontend`   
`npm install`   
`ng serve`

you can access nodejs at http://localhost:4200
