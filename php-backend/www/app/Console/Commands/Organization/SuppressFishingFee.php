<?php

namespace App\Console\Commands\Organization;

use App\Models\Organization\MemberFeePaymentTracking;
use App\Models\Organization\Organization;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

/**
 * Class SuppressFishingFee
 * @package App\Console\Commands\Organization
 */
class SuppressFishingFee extends Command
{
    /**
     * @var string
     */
    protected $signature = 'member:suppress-fishing-fee';

    /**
     * @var string \
     */
    protected $description = 'Cancel Due Fishing Fee for the members that own to the organizations that have suppress_due_fishing_fee flag with 1';


    public function handle(): void
    {
        $lastYear   = Carbon::now()->year - 1;
        $ids        = $this->getOrganizationsWithSuppressFishingFeeFlag();

        foreach ($this->getItemsToBeSuppressed($ids, $lastYear) as $item) {
            $suppressedAmount = (float)($item['total_fee_amount'] - $item['total_paid_amount']);
            try {
                $payment = new MemberFeePaymentTracking([
                    'member_id'            => $item['member_id'],
                    'organization_id'      => $item['organization_id'],
                    'fee_id'               => $item['fee_id'],
                    'is_hunter'            => 0,
                    'is_fisherman'         => $item['is_fisherman'],
                    'pay_date'             => Carbon::now()->toDate(),
                    'fee_amount'           => $item['fee_amount'],
                    'paid_amount'          => $suppressedAmount,
                    'is_payment'           => 1,
                    'is_fee_due_suppressed'=> 1,
                    'year'                 => $lastYear,
                ]);
                $payment->save();

                echo $suppressedAmount . ' suppressed for Member ID ' . $item['member_id'] . PHP_EOL;
            } catch (\Exception $e) {
                echo 'Fail to suppress for Member ID ' . $item['member_id'] . PHP_EOL;
                echo 'Error: ' . $e->getMessage() . PHP_EOL;
            }
        }
    }

    /**
     * @param array $ids
     * @param int $lastYear
     * @return array
     */
    private function getItemsToBeSuppressed(array $ids, int $lastYear): array
    {
        return MemberFeePaymentTracking::query()
            ->select(
                DB::raw(
                    'member_id, 
                    organization_id, 
                    fee_id,
                    fee_amount,
                    is_fisherman,
                    sum(case when is_payment = 1 and is_fisherman = 1 then paid_amount else 0 end) as total_paid_amount,
                    sum(case when is_payment != 1 and is_fisherman = 1 then fee_amount else 0 end) as total_fee_amount',
                )
            )
            ->where('is_fisherman', '=', 1)
            ->where('year', '=', $lastYear)
            ->whereIn('organization_id', $ids)
            ->groupBy(['member_id', 'organization_id', 'fee_id', 'fee_amount', 'is_fisherman'])
            ->havingRaw('total_fee_amount > total_paid_amount')
            ->get()
            ->toArray();
    }

    private function getOrganizationsWithSuppressFishingFeeFlag(): array
    {
        return Organization::query()
            ->where('suppress_due_fishing_fee', '=', 1)
            ->get()
            ->pluck('id')
            ->toArray();
    }
}
