<?php

namespace App\Console\Commands\Organization;

use App\Models\Organization\Member;
use App\Models\Organization\MemberFeePaymentTracking;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

/**
 * Class RemoveMemberOnlineWhoDidNotPayed
 * @package App\Console\Commands\Organization
 */
class RemoveMemberOnlineWhoDidNotPayed extends Command
{
    /**
     * @var string
     */
    protected $signature = 'member:remove-online-who-did-not-payed';

    /**
     * @var string \
     */
    protected $description = 'Remove online members who did not payed in 7 days';


    public function handle(): void
    {
        if ($members = $this->getMembers()) {
            /** @var Member $member */
            foreach ($members as $member) {
                try {
                    $member->delete();
                    $this->info("Member with ID # {$member->id} was deleted.");
                } catch (\Exception $e) {
                    $this->error(self::class . ' ' . $e->getMessage());
                }
            }
        }
    }

    public function getMembers(): array
    {
        $date       = Carbon::now()->subDays(7);
        $t1         = Member::getTableName();
        $t2         = MemberFeePaymentTracking::getTableName();
        $builder    = Member::query()
            ->select(
                DB::raw("{$t1}.id,
                    {$t1}.public_id,
                    sum(case when {$t2}.is_payment = 1 and {$t2}.is_fisherman = 1 then paid_amount else 0 end) as fisherman_paid_amount",
                )
            )
            ->leftJoin($t2, "{$t2}.member_id", '=', "{$t1}.id")
            ->where("{$t1}.created_online", '=', 1)
            ->whereDate("{$t1}.created_at", '=', $date)
            ->groupBy([
                "{$t1}.id",
                "{$t1}.public_id"
            ])
            ->havingRaw('fisherman_paid_amount = 0');

        return $builder->get()->all();
    }
}
