<?php

namespace App\Filters\Organization;

use App\Filters\BaseFilter;
use App\Models\HuntingBackground;
use App\Models\Organization\Member;
use App\Models\Organization\MemberFeePaymentTracking;
use App\Models\Organization\MemberPrivilegeTracking;
use App\Models\Organization\MemberStatusTracking;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Rule;

/**
 * Class HunterFilter
 * @package App\Filters\Admin
 */
class HunterGroupFilter extends BaseFilter
{
    /**
     * @var int
     */
    public $hunting_background_id = null;
    /**
     * @var ?bool
     */
    public $split_in_subgroups = null;
    /**
     * @var ?int
     */
    public $min_members = null;
    /**
     * @var ?int
     */
    public $max_members = null;
    /**
     * @var ?int
     */
    public $county_id = null;

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function getEloquentBuilder(Builder $builder): Builder
    {
        $self = $this;
        $builder->leftJoin('members', function ($join) {
            $join->on('hunters_groups.id', '=', 'members.group_id');
        })->groupBy('hunters_groups.id');

        $builder->leftJoin('hunting_background', function ($join) {
            $join->on('hunting_background.id', '=', 'hunters_groups.hunting_background_id');
        });

        // $builder->join('hunters_subgroups', function ($join) {
        //     $join->on('hunters_subgroups.hunters_group_id', '=', 'hunters_groups.id');
        // })->groupBy('hunters_groups.id');

        if (isset($this->county_id)) {
            $builder->where('hunting_background.county_id', $this->county_id);
        }

        if (isset($this->split_in_subgroups)) {
            $builder->where('split_in_subgroups', $this->split_in_subgroups);
        }

        if(isset($this->hunting_background_id)) {
            $builder->where('hunting_background_id', $this->hunting_background_id);
        }

        if(isset($this->min_members) || isset($this->max_members)) {

            if(isset($this->min_members) && isset($this->max_members)) {
                $builder->having('members_no', '<=', $this->max_members)
                        ->having('members_no', '>=', $this->min_members);
            }else if(isset($this->min_members)) {
                $builder->having('members_no', '>=', $this->min_members);
            }else if(isset($this->max_members)) {
                $builder->having('members_no', '<=', $this->max_members);
            }
        }

        return $builder;
    }

    public function apply(Builder $builder)
    {
        $builder = $this->getEloquentBuilder($builder);
        return $builder;
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            'hunting_background_id' => [
                'nullable',
                'integer',
                'exists:hunting_background,id'
            ],
            'split_in_subgroups' => [
                'nullable',
                'boolean'
            ],
            'min_members' => [
                'nullable',
                'integer',
                'min:0'
            ],
            'max_members' => [
                'nullable',
                'integer',
                'gte:min_members'
            ],
            'county_id' => [
                'nullable',
                'exists:counties,id'
            ]
        ];
    }

    public function getErrorMessage() : array
    {
        return [
            'hunting_background_id.integer'  => trans('admin/validation.field.integer'),
            'hunting_background_id.exists'   => trans('admin/validation.field.exists'),
            'split_in_subgroups.boolean'     => trans('admin/validation.field.boolean'),
            'min_members.integer'            => trans('admin/validation.field.integer'),
            'min_members.min'                => trans('admin/validation.field.min.numeric'),
            'max_members.integer'            => trans('admin/validation.field.integer'),
            'max_members.gte'                => trans('admin/validation.field.gte'),
            'county_id.exists'               => trans('admin/validation.field.exists'),
        ];
    }
}
