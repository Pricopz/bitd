import { Component, Input, OnInit } from '@angular/core'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'

@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.scss'],
})
export class ConfirmationModalComponent implements OnInit {
  @Input() title: string
  @Input() message: string
  @Input() okButton: string
  @Input() cancelButton: string

  constructor(private activeModal: NgbActiveModal) {}

  public ok(): void {
    this.activeModal.close({
      message: 'Ok',
    })
  }

  public cancel(): void {
    this.activeModal.dismiss({
      message: 'Cancel',
    })
  }

  ngOnInit(): void {
    if (!this.title) {
      this.title = 'Confirm action'
    }
    if (!this.message) {
      this.message = 'Are you sure you want to do this action?'
    }
    if (!this.okButton) {
      this.okButton = 'Yes'
    }
    if (!this.cancelButton) {
      this.cancelButton = 'No'
    }
  }
}
