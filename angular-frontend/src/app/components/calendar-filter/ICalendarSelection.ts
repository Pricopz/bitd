export interface ICalendarSelection {
  year: number,
  month: number,
  day?: number,
  hour?: number
}
