<?php

namespace App\Filters\Organization;

use App\Filters\BaseFilter;
use App\Models\Organization\Organization;
use App\Rules\Organization\CheckCNP;
use App\Rules\Organization\CheckExistenceOfEmployeeToOrganizationByCNP;

/**
 * Class OrganizationEmployeeFilter
 * @package App\Filters\Organization
 */
class OrganizationEmployeeFilter extends BaseFilter
{
    /**
     * @var Organization
     */
    private Organization $organization;

    /**
     * @param Organization $organization
     */
    public function setOrganization(Organization $organization): void
    {
        $this->organization = $organization;
    }
    /**
     * @var string|null
     */
    public ?string $cnp = null;

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            'cnp' => [
                'required',
                'size:13',
                new CheckCNP(),
                new CheckExistenceOfEmployeeToOrganizationByCNP($this->organization),
            ],
        ];
    }

    /**
     * @return array
     */
    public function getErrorMessage(): array
    {
        return [
            'cnp.required'  => trans('organization/validation.field.required'),
            'cnp.size'      => trans('organization/validation.field.size'),
        ];
    }
}
