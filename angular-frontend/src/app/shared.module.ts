import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
// acl
// import { ACLComponent } from 'src/app/components/cleanui/layout/ACL/acl.component'

// antd components module
import { AntdModule } from 'src/app/antd.module'
import { BreadcrumbsFilterComponent } from './components/breadcrumbs-filter/breadcrumbs-filter.component'
import { NgbdSortableHeader } from './directives/sortable-header.directive'
import { MsToSecPipe } from './pipes/ms-to-sec.pipe'
import { KeyValueFilterPipe } from './pipes/key-value-filter.pipe'
import { PaginationSizeComponent } from './components/pagination-size/pagination-size.component'
import { SelectionFilterPipe } from './pipes/selection-filter.pipe'
import { LabelMapPipe } from './pipes/label-map.pipe'
import { CalendarFilterComponent } from './components/calendar-filter/calendar-filter.component'
import { NiceDatePipe } from './pipes/nice-date.pipe'
import { FormatEventIdPipe } from './pipes/format-event-id.pipe'
import { TileNiceDatePipe } from './pipes/tile-nice-date.pipe'
import { DisableControlDirective } from './directives/disable-control.directive'
import { NullAsDashPipe } from './pipes/null-as-dash.pipe'
import { ConfirmationModalComponent } from './pages/user/common/confirmation-modal/confirmation-modal.component'
import { HighchartsChartModule } from 'highcharts-angular'

const MODULES = [
  CommonModule,
  RouterModule,
  AntdModule,
  TranslateModule,
  NgbModule,
  FormsModule,
  HighchartsChartModule,
]

@NgModule({
  imports: [...MODULES, ReactiveFormsModule],
  declarations: [
    // ACLComponent,
    BreadcrumbsFilterComponent,
    NgbdSortableHeader,
    DisableControlDirective,
    MsToSecPipe,
    KeyValueFilterPipe,
    SelectionFilterPipe,
    LabelMapPipe,
    PaginationSizeComponent,
    CalendarFilterComponent,
    NiceDatePipe,
    FormatEventIdPipe,
    TileNiceDatePipe,
    NullAsDashPipe,
    ConfirmationModalComponent,
  ],
  exports: [
    ...MODULES,
    BreadcrumbsFilterComponent,
    NgbdSortableHeader,
    DisableControlDirective,
    MsToSecPipe,
    KeyValueFilterPipe,
    SelectionFilterPipe,
    LabelMapPipe,
    PaginationSizeComponent,
    CalendarFilterComponent,
    NiceDatePipe,
    FormatEventIdPipe,
    TileNiceDatePipe,
    NullAsDashPipe,
  ],
  entryComponents: [
    ConfirmationModalComponent,
  ],
})
export class SharedModule {}
