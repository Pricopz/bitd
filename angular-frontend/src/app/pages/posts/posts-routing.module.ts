import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { LayoutsModule } from 'src/app/layouts/layouts.module'
import { InitComponent } from "./init/init.component";

const routes: Routes = [
  {
    path: '',
    component: InitComponent,
    data: { title: 'Init' },
  },
]

@NgModule({
  imports: [LayoutsModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostsRoutingModule {}
