import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { LogoutComponent } from "./logout/logout.component";

const routes: Routes = [
  {
    path: 'posts',
    data: { title: 'Posts' },
    loadChildren: () => import('src/app/pages/user/post/post.module').then(m => m.PostModule),
  },
  {
    path: 'logout',
    data: { title: 'Logout' },
    component: LogoutComponent
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule {}
