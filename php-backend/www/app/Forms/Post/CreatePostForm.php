<?php

namespace App\Forms\Post;

use App\Forms\BaseForm;
use App\Models\Post\Post;
use Illuminate\Validation\Rule;

/**
 * Class CreatePostForm
 * @package App\Forms\Admin
 */
class CreatePostForm extends BaseForm
{
    /**
     * @var string
     */
    public string $title = '';
    /**
     * @var string
     */
    public string $description = '';

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            'title' => [
                'required',
                'string',
                'min:1',
                'max:128',
                Rule::unique(Post::getTableName(), 'title')
            ],
            'description' => [
                'required',
                'string',
                'min:1',
                'max:255',
            ]
        ];
    }

    /**
     * @return array
     */
    public function getErrorMessage(): array
    {
        return [
            'title.required'                    => trans('validation.field.required'),
            'title.string'                      => trans('validation.field.string'),
            'title.min'                         => trans('validation.field.min'),
            'title.max'                         => trans('validation.field.max'),
            'title.unique'                      => trans('validation.field.unique'),
            'description.required'              => trans('validation.field.required'),
            'description.string'                => trans('validation.field.string'),
            'description.min'                   => trans('validation.field.min'),
            'description.max'                   => trans('validation.field.max'),
        ];
    }
}
