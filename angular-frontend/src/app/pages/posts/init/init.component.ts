import { Component, OnInit } from '@angular/core';
import {PostsService} from "../../../services/posts.service";
import {HttpErrorResponse} from "@angular/common/http";
import {HttpResponseHandlerService} from "../../../services/http-response-handler.service";

@Component({
  selector: 'app-init',
  templateUrl: './init.component.html',
  styleUrls: ['./init.component.scss']
})
export class InitComponent implements OnInit {

  private _posts: any[] = []
  private _isLoading: boolean = false

  public get posts(): any[] {
    return this._posts
  }

  public get isLoading(): boolean {
    return this._isLoading
  }

  constructor(private service: PostsService,
              private httpHandlerResponseHeader: HttpResponseHandlerService) { }

  private getPosts(): void {
    this._isLoading = true
    this.service.getPosts().subscribe(
      (resource: any) => {
        this._posts = resource
        this._isLoading = false
      }, (error: HttpErrorResponse) => {
        this._isLoading = false
        this.httpHandlerResponseHeader.notifyError(error)
      }
    )
  }

  ngOnInit(): void {
    this.getPosts()
  }
}
