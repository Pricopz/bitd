<?php

namespace App\Filters\Organization;

use App\Filters\BaseFilter;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class HunterEnrollToOrganizationFilter
 * @package App\Filters\Organization
 */
class HunterEnrollToOrganizationFilter extends BaseFilter
{
    /**
     * @var string|null
     */
    public ?string $key = null;

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            'key' => [
                'nullable',
                'string',
                'min:2',
            ],
        ];
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function getEloquentBuilder(Builder $builder): Builder
    {
        $self = $this;
        if ($this->key) {
            $builder->where(static function ($query) use ($self) {
                $query->where('first_name', 'like', "%{$self->key}%")
                    ->orWhere('last_name', 'like', "%{$self->key}%")
                    ->orWhere('phone', 'like', "%{$self->key}%")
                    ->orWhere('email', 'like', "%{$self->key}%")
                    ->orWhere('cnp', 'like', "%{$self->key}%");
            });
        }
        return $builder;
    }

    /**
     * @return array
     */
    public function getErrorMessage(): array
    {
        return [
            'key.string'                => trans('organization/validation.field.invalid'),
            'key.min'                   => trans('organization/validation.field.min'),
        ];
    }
}
