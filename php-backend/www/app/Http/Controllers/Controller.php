<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    /**
     * @param int $status
     * @param $response
     * @param array|null $headers
     * @return Response
     */
    protected function handleResponse(int $status, $response, array $headers = null): Response
    {
        $handleResponse = new Response($response, $status);
        if ($headers === null) {
            return $handleResponse->header('Content-Type', 'application/json');
        }
        if (count($headers)) {
            foreach ($headers as $header) {
                $key = array_key_first($header);
                $handleResponse->header($key, $header[$key]);
            }
        }
        return $handleResponse;
    }
}
