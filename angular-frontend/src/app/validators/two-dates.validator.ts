import { ValidatorFn, FormGroup } from '@angular/forms'
import { formatDate } from '@angular/common'

export interface ICompareDates {
  dateStart?: string,
  dateEnd: string,
  comparator: string,
  compareTime: boolean
}

export function datesComparator(dateStart: string, dateEnd: string, comparator: string, compareTime: boolean = false): ValidatorFn {
  return (formGroup: FormGroup): { [key: string]: boolean } | null => {
    const startDate = formGroup.get(dateStart).value
    const endDate   = formGroup.get(dateEnd).value

    if (startDate && endDate) {
      const format = compareTime ? 'MM/dd/yyyy HH:mm:ss' : 'MM/dd/yyyy'
      const d1    = new Date(formatDate(startDate, format, 'en'))
      const d2    = new Date(formatDate(endDate, format, 'en'))

      switch (true) {
        case comparator === '<':
          if (d1 < d2) {
            return {invalidDateInterval: true}
          }
          break
        case comparator === '<=':
          if (d1 <= d2) {
            return {invalidDateInterval: true}
          }
          break
        case comparator === '>':
          if (d1 > d2) {
            return {invalidDateInterval: true}
          }
          break
        case comparator === '>=':
          if (d1 >= d2) {
            return {invalidDateInterval: true}
          }
          break
        case comparator === '!==':
          if (d1 !== d2) {
            return {invalidDateInterval: true}
          }
          break
        case comparator === '==':
        case comparator === '===':
          if (d1 === d2) {
            return {invalidDateInterval: true}
          }
          break
      }
    }
    return null
  }
}

export function multiDatesComparator(groupOfDates: ICompareDates[]): ValidatorFn {
  return (formGroup: FormGroup): { [key: string]: boolean } | null => {
    const validations = {}

    for (let k = 0; k < groupOfDates.length; k++) {
      const group       = groupOfDates[k]
      const invalidAttr = `invalidDateInterval_${k}`
      const startDate   = group.dateStart ? formGroup.get(group.dateStart).value : (new Date()).toString()
      const endDate     = formGroup.get(group.dateEnd).value

      if (startDate && endDate) {
        const format  = group.compareTime ? 'MM/dd/yyyy HH:mm:ss' : 'MM/dd/yyyy'
        const d1      = new Date(formatDate(startDate, format, 'en'))
        const d2      = new Date(formatDate(endDate, format, 'en'))

        switch (true) {
          case group.comparator === '<':
            if (d1 < d2) {
              validations[invalidAttr] = true
            }
            break
          case group.comparator === '<=':
            if (d1 <= d2) {
              validations[invalidAttr] = true
            }
            break
          case group.comparator === '>':
            if (d1 > d2) {
              validations[invalidAttr] = true
            }
            break
          case group.comparator === '>=':
            if (d1 >= d2) {
              validations[invalidAttr] = true
            }
            break
          case group.comparator === '!==':
            if (d1 !== d2) {
              validations[invalidAttr] = true
            }
            break
          case group.comparator === '==':
          case group.comparator === '===':
            if (d1 === d2) {
              validations[invalidAttr] = true
            }
            break
        }
      }
    }
    return JSON.stringify(validations) !== JSON.stringify({}) ? validations : null
  }
}
