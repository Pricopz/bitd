'use strict';

import { Request, Response, NextFunction } from "express";

export const contentTypeHandler = (request: Request, response: Response, next: NextFunction) => {
    response.setHeader('Content-Type', 'application/json');
    next();
};
