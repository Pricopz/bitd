import { Component, OnInit } from '@angular/core'
import { BasicAuthService } from '../../../../../services/basic-auth.service'

@Component({
  selector: 'cui-topbar-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss'],
})
export class TopbarUserMenuComponent implements OnInit {
  public name: string
  public email: string
  public isAuth: boolean = false

  constructor(
    public authService: BasicAuthService,
  ) {}

  public async logout() {
    await this.authService.logout()
  }

  ngOnInit(): void {
    const user = this.authService.getUser()
    this.name = user ? user.name : ''
    this.email = user ? user.email : ''
    this.isAuth = !!user
  }
}
