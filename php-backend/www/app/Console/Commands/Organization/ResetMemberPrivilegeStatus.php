<?php

namespace App\Console\Commands\Organization;

use App\Enum\MemberStatusEnum;
use App\Exceptions\EntitySavingException;
use App\Models\Organization\Member;
use App\Models\Organization\MemberPrivilegeTracking;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class ResetMemberPrivilegeStatus
 * @package App\Console\Commands\Organization
 */
class ResetMemberPrivilegeStatus extends Command
{
    /**
     * @var string
     */
    protected $signature = 'member:reset-privilege-status';

    /**
     * @var string \
     */
    protected $description = 'Insert at the begin of each season members privileges';

    /**
     * @return array
     */
    private function getMembers(): array
    {
        return Member::query()
            # ->where('status', '=', MemberStatusEnum::ACTIVE)
            ->get()
            ->all();
    }

    public function handle(): void
    {
        $year = Carbon::now()->year;
        foreach ($this->getMembers() as $member) {
            /**
             * @var Member $member
             */
            try {
                $privilegeTracking = MemberPrivilegeTracking::query()
                    ->where('member_id', '=', $member->id)
                    ->where(DB::raw('YEAR(start_season_date)'), '=', $year)
                    ->get()
                    ->first();

                if (!$privilegeTracking) {
                    DB::beginTransaction();
                    try {
                        $privilegeTracking = new MemberPrivilegeTracking([
                            'member_id'         => $member->id,
                            'organization_id'   => $member->organization_id,
                            'is_student'        => $member->next_year_is_student ?: 0,
                            'is_retired'        => $member->next_year_is_retired ?: 0,
                            'is_with_disability'=> $member->next_year_is_with_disability ?: 0,
                            'is_privileged'     => $member->organization_managing_role !== null,
                            'is_employee'       => $member->organization_employee_id !== null,
                            'start_season_date' => "{$year}-01-01",
                            'end_season_date'   => "{$year}-12-31",
                        ]);
                        if (!$privilegeTracking->save()) {
                            throw new EntitySavingException('Could not update tracking.');
                        }
                        $member->is_student                     = $member->next_year_is_student ?: 0;
                        $member->next_year_is_student           = null;
                        $member->is_retired                     = $member->next_year_is_retired ?: 0;
                        $member->next_year_is_retired           = null;
                        $member->is_with_disability             = $member->next_year_is_with_disability ?: 0;
                        $member->next_year_is_with_disability   = null;
                        if (!$member->update(['is_student', 'is_retired', 'is_with_disability', 'next_year_is_student', 'next_year_is_retired', 'next_year_is_with_disability'])) {
                            throw new EntitySavingException('Could not reset member.');
                        }

                        DB::commit();
                        echo 'Updated Member with ID ' . $member->id . PHP_EOL;
                    } catch (\Exception $e) {
                        Log::error($e->getMessage());
                        echo 'Error: ' . $e->getMessage() . PHP_EOL;
                        DB::rollBack();
                    }
                }
            } catch (\Exception $e) {
                echo 'Error: ' . $e->getMessage() . PHP_EOL;
            }
        }
    }
}
