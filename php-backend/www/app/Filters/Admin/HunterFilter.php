<?php

namespace App\Filters\Admin;

use App\Filters\BaseFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Rule;

/**
 * Class HunterFilter
 * @package App\Filters\Admin
 */
class HunterFilter extends BaseFilter
{
    /**
     * @var string
     */
    // public string $key = '';
    /**
     * @var boolean
     */
    public ?bool $not_enrolled = false;
    /**
     * @var ?int[]
     */
    public ?array $organization_ids = null;

    /**
     * @param $builder
     * @return mixed
     */
    public function getEloquentBuilder(Builder $builder): Builder
    {
        // if ($this->key) {
        //     $self = $this;
        //     $builder->where(static function ($query) use ($self) {
        //         $query->where('cnp', 'like', "%{$self->key}%")
        //               ->orWhere('first_name', 'like', "%{$self->key}%")
        //               ->orWhere('last_name', 'like', "%{$self->key}%");
        //     });
        // }

        $self = $this;
        if ($this->not_enrolled) {
            $builder->whereNotIn('id', static function ($query) use ($self) {
                $query->select('hunter_id')
                      ->from('hunters_members');
            });
        } elseif (isset($this->organization_ids) && (count($this->organization_ids) > 0)) {
            $builder->join('hunters_members', function ($join) {
                    $join->on('hunters.id', '=', 'hunters_members.hunter_id')
                         ->whereIn('hunters_members.organization_id', $this->organization_ids);
            })->groupBy('hunters.public_id');
        } else {
            $builder->leftJoin('hunters_members', function ($join) {
                $join->on('hunters.id', '=', 'hunters_members.hunter_id');
            })->groupBy('hunters.public_id');
        }

        return $builder;
    }

    public function apply(Builder $builder)
    {
        $builder = $this->getEloquentBuilder($builder);
        return $builder;
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            // 'key' => [
            //     'string',
            //     'min:2',
            // ],
            'not_enrolled' => [
                'boolean'
            ],
            'organization_ids' => [
                'array'
            ],
            'organization_ids.*' => [
                'integer',
                'exists:organizations,id'
            ]
        ];
    }

    public function getErrorMessage() : array
    {
        return [
            // 'key.string'                 => trans('admin/validation.field.string'),
            // 'key.min'                    => trans('admin/validation.field.min'),
            'not_enrolled.boolean'          => trans('admin/validation.field.boolean'),
            'organization_ids.*.integer'    => trans('admin/validation.field.integer'),
            'organization_ids.*.exists'     => trans('admin/validation.field.exists'),
        ];
    }
}
