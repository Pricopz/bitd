<?php

namespace App\Filters\Organization;

use App\Enum\MemberOnlineStatusEnum;
use App\Filters\BaseFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Rule;

/**
 * Class MemberOnlineRequestsFilter
 * @package App\Filters\Organization
 */
class MemberOnlineRequestsFilter extends BaseFilter
{
    /**
     * @var string|null
     */
    public ?string $status = null;
    /**
     * @var string|null
     */
    public ?string $key = null;

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function getEloquentBuilder(Builder $builder): Builder
    {
        if ($this->status) {
            $builder->where("status", '=', $this->status);
        }
        $self = $this;
        if ($this->key) {
            $builder->where(static function ($query) use ($self) {
                $query->where('first_name', 'like', "%{$self->key}%")
                    ->orWhere("last_name", 'like', "%{$self->key}%")
                    ->orWhere("email", 'like', "%{$self->key}%")
                    ->orWhere("cnp", 'like', "%{$self->key}%");
            });
        }
        return $builder;
    }

    /**
     * @return array[]
     */
    public function getRules(): array
    {
        return [
            'status' => [
                'nullable',
                Rule::in(MemberOnlineStatusEnum::asList()),
            ],
            'key' => [
                'nullable',
                'string',
                'min:2'
            ],
        ];
    }

    public function getErrorMessage(): array
    {
        return [
            'status.in'     => trans('organization/validation.field.invalid'),
            'key.min'       => trans('organization/validation.field.min'),
        ];
    }
}
