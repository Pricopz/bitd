<?php

namespace App\Filters\Admin;

use App\Filters\BaseFilter;
use App\Models\Hunter\Hunter;
use App\Models\Organization\Member;
use App\Models\Organization\Organization;
use App\Rules\Admin\CheckOrganization;
use Illuminate\Database\Eloquent\Builder as EBuilder;

/**
 * Class HunterAccountFilter
 * @package App\Filters\Admin
 */
class HunterAccountFilter extends BaseFilter
{
    /**
     * @var string|null
     */
    public ?string $key = null;
    /**
     * @var string|null
     */
    public ?string $organization_id = null;

    /**
     * @param EBuilder $builder
     * @return EBuilder
     */
    public function getEloquentBuilder(EBuilder $builder): EBuilder
    {
        if ($this->key) {
            $self = $this;
            $t0 = Hunter::getTableName();
            $builder->where(static function ($query) use ($self, $t0) {
                $query->where("{$t0}.email", 'like', "%{$self->key}%")
                    ->orWhere("{$t0}.phone", 'like', "%{$self->key}%")
                    ->orWhere("{$t0}.first_name", 'like', "%{$self->key}%")
                    ->orWhere("{$t0}.last_name", 'like', "%{$self->key}%");
            });
        }
        if ($this->organization_id) {
            $organization = Organization::query()->where('public_id', '=', $this->organization_id)->first();
            if ($organization) {
                $t0 = Hunter::getTableName();
                $t1 = Member::getTableName();
                $builder->join($t1, "{$t0}.id", '=', "{$t1}.hunter_id");
                $builder->where("{$t1}.organization_id", '=', $organization->id);
            }
        }
        return $builder;
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            'key' => [
                'nullable',
                'string',
                'min:2',
            ],
            'organization_id' => [
                'nullable',
                new CheckOrganization()
            ],
        ];
    }

    /**
     * @return array
     */
    public function getErrorMessage() : array
    {
        return [
            'key.string'                        => trans('admin/validation.field.string'),
            'key.min'                           => trans('admin/validation.field.min'),
        ];
    }
}
