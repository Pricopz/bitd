<?php
$baseDictionary = require dirname(__DIR__) . '/validation.php';

return array_merge(
    $baseDictionary,
    [
        'excel.required'                            => 'Fisierul este obligatoriu.',
        'excel.mimes'                               => 'Formatul fisierului este invalid.',
        'excel.file'                                => 'Datele trimise nu reprezinta un fisier.',
    ],
);
