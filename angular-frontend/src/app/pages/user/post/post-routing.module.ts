import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { InitComponent } from './init/init.component'
import { PostDetailsComponent } from './post-details/post-details.component'
import { CreatePostComponent } from "./create-post/create-post.component";

const routes: Routes = [
  {
    path: '',
    component: InitComponent,
    data: { title: 'Posts' },
  },
  {
    path: 'create',
    component: CreatePostComponent,
    data: { title: 'Create Post' },
  },
  {
    path: ':id/details',
    component: PostDetailsComponent,
    data: { title: 'Post Details' },
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostRoutingModule {}
