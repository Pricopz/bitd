<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AccountFunctionalitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('account_functionalities')->insert([
            ['title' => 'organization.enroll', 'label' => 'Enroll Organization'],
            ['title' => 'organization.update', 'label' => 'Update Organization'],
            ['title' => 'organization.view', 'label' => 'View Organization'],
            ['title' => 'member.import', 'label' => 'Import Members'],
            ['title' => 'member.view', 'label' => 'View Member'],
            ['title' => 'member.create', 'label' => 'Create Member'],
            ['title' => 'member.update', 'label' => 'Update Member'],
            ['title' => 'member.activate', 'label' => 'Activate Member'],
            ['title' => 'member.inactivate', 'label' => 'Inactivate Member'],
            ['title' => 'member.request.approve', 'label' => 'Approve Member Request'],
            ['title' => 'member.request.reject', 'label' => 'Reject Member Request'],
            ['title' => 'member.request.view', 'label' => 'View Member Request'],
            ['title' => 'taxes.create', 'label' => 'Create Tax'],
            ['title' => 'taxes.view', 'label' => 'View Tax'],
            ['title' => 'taxes.update', 'label' => 'Update Tax'],
            ['title' => 'taxes.activate', 'label' => 'Activate Tax'],
            ['title' => 'taxes.inactivate', 'label' => 'Inactivate Tax'],
            ['title' => 'dues.create', 'label' => 'Create Due'],
            ['title' => 'dues.view', 'label' => 'View Due'],
            ['title' => 'dues.update', 'label' => 'Update Due'],
            ['title' => 'dues.activate', 'label' => 'Activate Due'],
            ['title' => 'dues.inactivate', 'label' => 'Inactivate Due'],
            ['title' => 'dues_extra.create', 'label' => 'Create Extra Due'],
            ['title' => 'dues_extra.view', 'label' => 'View Extra Due'],
            ['title' => 'dues_extra.update', 'label' => 'Update Extra Due'],
            ['title' => 'dues_extra.activate', 'label' => 'Activate Extra Due'],
            ['title' => 'dues_extra.inactivate', 'label' => 'Inactivate Extra Due'],
            ['title' => 'series_invoices.view', 'label' => 'View Invoices Series'],
            ['title' => 'employee.create', 'label' => 'Create Employee'],
            ['title' => 'employee.view', 'label' => 'View Employee'],
            ['title' => 'employee.update', 'label' => 'Update Employee'],
            ['title' => 'employee.delete', 'label' => 'Delete Employee'],
            ['title' => 'fee.create', 'label' => 'Create Fee'],
            ['title' => 'fee.view', 'label' => 'View Fee'],
            ['title' => 'fee.update', 'label' => 'Update Fee'],
            ['title' => 'hunting_group.create', 'label' => 'Create Hunting Group'],
            ['title' => 'hunting_group.view', 'label' => 'View Hunting Group'],
            ['title' => 'hunting_group.update', 'label' => 'Update Hunting Group'],
            ['title' => 'hunting_group.delete', 'label' => 'Delete Hunting Group'],
            ['title' => 'hunting_group.add_member', 'label' => 'Hunting Group Add Member'],
            ['title' => 'hunting_group.delete_member', 'label' => 'Delete Member From Hunting Group'],
            ['title' => 'hunting_subgroup.create', 'label' => 'Create Hunting Subgroup'],
            ['title' => 'hunting_subgroup.view', 'label' => 'View Hunting Subgroup'],
            ['title' => 'hunting_subgroup.delete', 'label' => 'Delete Hunting Subgroup'],
            ['title' => 'hunting_subgroup.update', 'label' => 'Update Hunting Subgroup'],
            ['title' => 'hunting_subgroup.add_member', 'label' => 'Hunting Subgroup Add Member'],
            ['title' => 'hunting_subgroup.delete_member', 'label' => 'Delete Member From Hunting Subgroup'],
            ['title' => 'cash_register.view', 'label' => 'View Cash Register'],
            ['title' => 'cash_register.create', 'label' => 'Create Cash Register'],
            ['title' => 'cash_register.update', 'label' => 'Update Cash Register'],
            ['title' => 'cash_register.status', 'label' => 'Change Cash Register Status'],
            ['title' => 'hunting_background.view', 'label' => 'View Hunting Background'],
            ['title' => 'cash_desk.provision.create', 'label' => 'Create Payment Provision'], # Caserie -> Dispozitii de plata
            ['title' => 'cash_desk.payroll.create', 'label' => 'Create Payroll Sheet'], # Caserie -> Creare Foi de varsamant
            ['title' => 'cash_desk.receipt.create', 'label' => 'Create Receipt Sheet'], # Caserie -> Creare Chitanta
            ['title' => 'cash_desk.invoice.create', 'label' => 'Create Invoice'], # Caserie -> Creare Factura
            ['title' => 'cash_desk.proof_of_payment.create', 'label' => 'Create Proof of payment'], # Caserie -> Creare Dovezi de plata
            ['title' => 'cash_desk.cash_register.view', 'label' => 'View Cash Register'], # Caserie -> Vizualizare Registre casa
            ['title' => 'cash_desk.cash_register.create_payment', 'label' => 'Create Payment On Cash Register'], # Caserie -> Incasare in Registre casa
            ['title' => 'cash_desk.collect.create', 'label' => 'Cash Collect'], # Caserie -> incasare
            ['title' => 'cash_desk.spillage.create', 'label' => 'Create Sheet of spillage'], # Caserie -> Foaie de varsamant
            ['title' => 'cash_desk.receipt.create', 'label' => 'Create Receipt'], # Caserie -> Foaie de varsamant
            ['title' => 'cash_desk.receipt.view', 'label' => 'View Receipt'], # Caserie -> Foaie de varsamant
        ]);
    }
}
