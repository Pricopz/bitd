import { Injectable } from '@angular/core'
import { HttpErrorResponse } from '@angular/common/http'
import { NzNotificationService } from 'ng-zorro-antd'

@Injectable({
  providedIn: 'root',
})
export class HttpResponseHandlerService {
  constructor(private notification: NzNotificationService) {}

  public notifySuccess(title: string, content: string): void {
    this.notification.success(title, content)
  }

  public notifyInfo(title: string, content: string): void {
    this.notification.info(title, content)
  }

  public notifyError(error: HttpErrorResponse): void {
    const statusCode = error.status
    let message = 'Server error occurred.'
    let title = 'Error'
    if (statusCode === 0) {
      message = 'Could not connect to the server.'
    } else {
      if (error.error && error.error.error) {
        message = error.error.error
      } else if (error.error.errors) {
        const _obj = error.error.errors
        message = _obj[Object.keys(_obj)[0]]
      } else if (error.message) {
        message = error.message
      }
      title = `Error ${statusCode}!`
    }
    this.notification.error(title, message)
  }

  public getNotificationInstance(): NzNotificationService {
    return this.notification
  }
}
