'use strict';

import { Sequelize } from "sequelize";
import { ModelCtor } from "sequelize/types/lib/model";

export default abstract class AbstractModelCreator
{
    protected modelCtor: ModelCtor<any> = undefined;

    public getModelCto = async sequelize => {
        if (this.modelCtor === undefined) {
            this.initModel(sequelize);
        }
        return this.modelCtor;
    };

    public abstract initModel(sequelize: Sequelize): void;
}
