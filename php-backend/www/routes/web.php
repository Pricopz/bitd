<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


# API V1
$router->group(['prefix' => 'api/v1'], static function () use ($router) {
    # POSTS
    $router->group(['prefix' => 'posts'], static function () use ($router) {
        $router->get('/', 'Post\\PostController@getPosts');
        $router->get('/{id}', 'Post\\PostController@getPost');

        $router->group(['middleware' => [\App\Http\Middleware\User\JWTAuthMiddleware::class]], static function () use ($router) {
            $router->post('/', 'Post\\PostController@createPost');
            $router->put('/{id}', 'Post\\PostController@updatePost');
            $router->delete('/{id}', 'Post\\PostController@deletePost');
            $router->get('/by/author', 'Post\\PostController@getPostsByAuthor');
        });
    });
});


