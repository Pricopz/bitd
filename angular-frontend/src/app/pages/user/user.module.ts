import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { SharedModule } from 'src/app/shared.module'
import { UserRoutingModule } from './user-routing.module'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { DataCardModule } from '../../components/data-card/data-card.module'
import { PostModule } from './post/post.module';
import { LogoutComponent } from './logout/logout.component'

const COMPONENTS = []

@NgModule({
  declarations: [...COMPONENTS, LogoutComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    UserRoutingModule,
    PostModule,
    NgbModule,
    DataCardModule,
  ],
  exports: [
  ],
})
export class UserModule {}
