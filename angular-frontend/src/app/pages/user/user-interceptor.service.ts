import { Injectable, Injector } from '@angular/core'
import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse,
  HttpEvent,
} from '@angular/common/http'
import { Router } from '@angular/router'
import { Observable, throwError } from 'rxjs'
import { switchMap, take, catchError } from 'rxjs/operators'
import { BasicAuthService } from '../../services/basic-auth.service'

@Injectable()
export class UserInterceptor implements HttpInterceptor {
  public constructor(private injector: Injector, private router: Router) {}

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authService = this.injector.get(BasicAuthService)

    return authService.isAuthenticated().pipe(
      take(1),
      switchMap((data: boolean) => {
        let req = null
        if (data) {
          req = request.clone({
            headers: request.headers.set('Authorization', 'Bearer ' + authService.getToken()),
          })
        } else {
          req = request.clone()
        }

        return next.handle(req).pipe(
          catchError((error: HttpErrorResponse) => {
            const status = error.status
            switch (status) {
              case 401:
                authService.logout()
                this.router.navigate(['auth/login'])
                break
            }
            return throwError(error)
          }),
        )
      }),
    )
  }
}
