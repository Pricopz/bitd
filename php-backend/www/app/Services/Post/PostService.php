<?php

namespace App\Services\Post;

use App\Exceptions\EntityNotFoundException;
use App\Exceptions\EntitySavingException;
use App\Forms\Post\CreatePostForm;
use App\Models\Post\Post;
use App\Models\Post\User;

/**
 * Class StaticPageService
 * @package App\Services\General
 */
class PostService
{
    /**
     * @return array
     */
    public function getPosts(): array
    {
        return Post::query()
            ->with(['author' => static function ($query) {
                $query->select('id', 'name');
            }])
            ->orderBy('created_at', 'DESC')
            ->get()
            ->all();
    }

    /**
     * @param int $id
     * @return Post
     * @throws EntityNotFoundException
     */
    public function getPost(int $id): Post
    {
        $post = Post::query()
            ->with(['author' => static function ($query) {
                $query->select('id', 'name');
            }])
            ->where('id', '=', $id)
            ->first();

        if (!$post) {
            throw new EntityNotFoundException('Post not found.');
        }
        return $post;
    }

    public function getPostsByAuthorId(int $authorId): array
    {
        return Post::query()
            ->with(['author' => static function ($query) {
                $query->select('id', 'name');
            }])
            ->where('author_id', '=', $authorId)
            ->orderBy('created_at', 'DESC')
            ->get()
            ->all();
    }

    /**
     * @param User $user
     * @param CreatePostForm $form
     * @return Post
     * @throws EntitySavingException
     */
    public function createPost(User $user, CreatePostForm $form): Post
    {
        $post = new Post([
            'title' => $form->title,
            'description' => $form->description,
            'author_id' => $user->id,
        ]);
        if (!$post->save()) {
            throw new EntitySavingException('Post was not saved.');
        }
        return $post;
    }

    /**
     * @param Post $post
     * @param CreatePostForm $form
     * @return Post
     * @throws EntitySavingException
     */
    public function updatePost(Post $post, CreatePostForm $form): Post
    {
        if (!$post->update($form->getValues())) {
            throw new EntitySavingException('Post was not updated.');
        }
        return $post;
    }

    /**
     * @param Post $post
     * @return bool
     * @throws \Exception
     */
    public function deletePost(Post $post): bool
    {
        return $post->delete();
    }
}
