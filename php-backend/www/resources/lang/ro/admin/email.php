<?php

return [
    'subject.account.activation'                => 'Activare cont',
    'subject.account.reset.password'            => 'Resetare parolă',
    'subject.approved.enrollment.request'       => 'Cerere dvs. de înrolare a fost aprobată',
    'subject.declined.enrollment.request'       => 'Cerere dvs. de înrolare a fost respinsă',
    'subject.marketing.contact.message'         => 'Mesaj nou platformă',
];
