<?php

namespace App\Filters\Organization;

use App\Enum\OrganizationStatusEnum;
use App\Filters\BaseFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Rule;

/**
 * Class OrganizationFilter
 * @package App\Filters\Organization
 */
class OrganizationFilter extends BaseFilter
{
    /**
     * @var string|null
     */
    public ?string $status = 'not_enrolled';


    public function getRules(): array
    {
        return [
            'status' => [
                'nullable',
                Rule::in(['enrolled', 'not_enrolled', 'all'])
            ],
        ];
    }


    /**
     * @param Builder $builder
     * @return Builder
     */
    public function getEloquentBuilder(Builder $builder): Builder
    {
        if ($this->status) {
            if ($this->status === 'not_enrolled') {
                $builder->where('status', '!=', OrganizationStatusEnum::ENROLLED);
            } else if ($this->status === 'enrolled') {
                $builder->where('status', '=', OrganizationStatusEnum::ENROLLED);
            }
        }
        return $builder;
    }

    /**
     * @return array
     */
    public function getErrorMessage(): array
    {
        return [
            'status.in'  => trans('organization/validation.field.invalid'),
        ];
    }
}
