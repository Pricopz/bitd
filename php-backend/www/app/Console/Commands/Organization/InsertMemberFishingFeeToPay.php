<?php

namespace App\Console\Commands\Organization;

use App\Enum\FishingPermitsStatusEnum;
use App\Enum\MemberStatusEnum;
use App\Exceptions\EntitySavingException;
use App\Models\FishingPermit;
use App\Models\Organization\Fee;
use App\Models\Organization\Member;
use App\Models\Organization\MemberFeePaymentTracking;
use App\Models\Organization\MemberFishingPermitsStatusTracking;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

/**
 * Class InsertMemberFishingFeeToPay
 * @package App\Console\Commands\Organization
 */
class InsertMemberFishingFeeToPay extends Command
{
    /**
     * @var string
     */
    protected $signature = 'member:insert-member-fishing-fee';

    /**
     * @var string \
     */
    protected $description = 'Insert at the begin of each season fishing fee that each member has to pay';


    public function handle(): void
    {
        $currentYear = date('Y');
        try {
            # Get already inserted Fisherman Members
            $existingIds = MemberFeePaymentTracking::query()
                ->where('is_fisherman', '=', 1)
                ->where('year', '=', $currentYear)
                ->pluck('member_id')
                ->toArray();
            $query = Member::query()
                ->where('is_fisherman', '=', 1)
                ->where('status', '=', MemberStatusEnum::ACTIVE);
            # Exclude already inserted Fisherman Members
            if (count($existingIds) > 0) {
                $query->whereNotIn('id', $existingIds);
            }
            # Iterate through each member and insert fishing fee
            foreach ($query->get()->all() as $member) {
                try {
                    /**
                     * @var Member $member
                     */
                    $fee = $member->getFishingFee(new Fee());
                    if ($fee) {
                        $feePaymentTracking = new MemberFeePaymentTracking([
                            'member_id' => $member->id,
                            'organization_id' => $member->organization_id,
                            'fee_id' => $fee->id,
                            'is_hunter' => 0,
                            'is_fisherman' => 1,
                            'fee_amount' => $fee->getValue(),
                            'paid_amount' => 0,
                            'pay_date' => Carbon::now(),
                            'is_payment' => 0,
                            'year' => $currentYear
                        ]);
                        if (!$feePaymentTracking->save()) {
                            throw new EntitySavingException('Could not save fishing fee for member with ID ' . $member->id);
                        }
                        // Insert with status UNREQUESTED the fishing permit
                        $this->insertFishingPermit($member, $currentYear);
                        $this->info('Inserted fishing fee for member with ID ' . $member->id);
                    } else {
                        throw new EntitySavingException('Could not detect fishing fee for member with ID ' . $member->id);
                    }
                } catch (\Exception $e) {
                    Log::error($e->getMessage());
                    $this->error('Error: ' . $e->getMessage());
                }
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            $this->error('Error: ' . $e->getMessage());
        }
    }

    /**
     * @param Member $member
     * @param int $year
     */
    private function insertFishingPermit(Member $member, int $year): void
    {
        $fishingPermit = FishingPermit::query()
            ->where('member_id', '=', $member->id)
            ->where('year', '=', $year)
            ->first();
        if ($fishingPermit) {
            return;
        }
        # Save Permit
        $fishingPermit                                  = new FishingPermit();
        $fishingPermit->member_id                       = $member->id;
        $fishingPermit->organization_id                 = $member->organization_id;
        $fishingPermit->status                          = FishingPermitsStatusEnum::UNREQUESTED;
        $fishingPermit->year                            = $year;
        if (!$fishingPermit->save()) {
            return;
        }
        # Save Permit Status Tracking
        $fishingPermitTracking                              = new MemberFishingPermitsStatusTracking();
        $fishingPermitTracking->permit_id                   = $fishingPermit->id;
        $fishingPermitTracking->member_id                   = $member->id;
        $fishingPermitTracking->organization_id             = $member->organization_id;
        $fishingPermitTracking->prev_status                 = null;
        $fishingPermitTracking->current_status              = FishingPermitsStatusEnum::UNREQUESTED;
        $fishingPermitTracking->prev_status_changed_date    = null;
        $fishingPermitTracking->current_status_changed_date = Carbon::now();
        $fishingPermitTracking->is_single_row               = 1;
        $fishingPermitTracking->is_last_row                 = 1;
        $fishingPermitTracking->save();
    }
}
