export interface ISortingModel {
  column?: string
  direction?: string
}

export class SortingModel implements ISortingModel {
  public column: string
  public direction: string

  constructor(model?: ISortingModel) {
    if (model) {
      Object.assign(this, model)
    }
  }

  static fromJson(object: ISortingModel): SortingModel {
    const obj     = new SortingModel()
    obj.column    = object.column ? object.column : null
    obj.direction = object.direction ? object.direction : null
    return obj
  }
}
