import { NgModule } from '@angular/core'
import { SharedModule } from 'src/app/shared.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { PostsRoutingModule } from "./posts-routing.module";
import { InitComponent } from './init/init.component';


const COMPONENTS = []

@NgModule({
  imports: [SharedModule, FormsModule, ReactiveFormsModule, PostsRoutingModule],
  declarations: [...COMPONENTS, InitComponent],
})
export class PostsModule {}
