'use strict';

import * as express from 'express';
import * as jwt from 'jsonwebtoken';
import {IUserRegistrationForm, UserRegistrationForm} from "../forms/user-registration.form";
import {IUserLoginForm, UserLoginForm} from "../forms/user-login.form";
import UserService from "../services/user.service";
import MySqlService from "../services/mysql.service";
import {UserDto} from "../dtos/user.dto";

export default class UserController
{
    private service: UserService;

    public constructor()
    {
        this.service = new UserService(MySqlService.getInstance());
    }

    public async hello(req: express.Request, res: express.Response): Promise<any>
    {
        return res.status(201).end(JSON.stringify({message: "Hello"}));
    }

    public async create(req: express.Request, res: express.Response): Promise<any>
    {
        try {
            let form: UserRegistrationForm = UserRegistrationForm.fromJson(req.body as IUserRegistrationForm);
            const errors = UserRegistrationForm.getErrors(form);
            if (errors !== null) {
                return res.status(400).end(JSON.stringify(errors));
            }

            let user = await this.service.create(form);
            return res.status(201).end(JSON.stringify(user));
        } catch (e) {
            console.log(e);
            return res.status(500).end(JSON.stringify({error: 'Server error.'}));
        }
    }

    public async login(req: express.Request, res: express.Response): Promise<any>
    {
        try {
            let form: UserLoginForm = UserLoginForm.fromJson(req.body as IUserLoginForm);
            const errors = UserLoginForm.getErrors(form);
            if (errors !== null) {
                return res.status(400).end(JSON.stringify(errors));
            }

            let user = await this.service.login(form);
            if (!user) {
                return res.status(401).end(JSON.stringify({error: 'Unauthorized user.'}));
            }

            const expiresIn = (form.remember ? 30 : 1) * 86400;
            const jwt_token = jwt.sign({uuid: user.id}, process.env.JWT_SECRET, {expiresIn: expiresIn});
            return res.status(200).end(JSON.stringify({user: UserDto.fromJson(user), token: jwt_token, expires_in: expiresIn}));
        } catch (e) {
            console.log(e);
            return res.status(500).end(JSON.stringify({error: 'Server error.'}));
        }
    }
}
