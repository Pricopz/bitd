<?php

namespace App\Filters\Organization;

use App\Filters\BaseFilter;
use App\Models\Organization\OrganizationEmployee;
use App\Models\Organization\OrganizationToEmployee;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class OrganizationToEmployeeFilter
 * @package App\Filters\Organization
 */
class OrganizationToEmployeeFilter extends BaseFilter
{
    /**
     * @var string|null
     */
    public ?string $key = null;

    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'key' => [
                'nullable',
                'string',
                'min:2',
            ],
        ];
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function getEloquentBuilder(Builder $builder): Builder
    {
        if ($this->key) {
            $t0     = OrganizationToEmployee::getTableName();
            $t1     = OrganizationEmployee::getTableName();
            $self   = $this;
            $builder->where(static function ($query) use ($self, $t0, $t1) {
                $query->where("{$t1}.first_name", 'like', "%{$self->key}%")
                    ->orWhere("{$t1}.last_name", 'like', "%{$self->key}%")
                    ->orWhere("{$t1}.phone", 'like', "%{$self->key}%")
                    ->orWhere("{$t1}.email", 'like', "%{$self->key}%")
                    ->orWhere("{$t0}.contact_email", 'like', "%{$self->key}%")
                    ->orWhere("{$t0}.contact_phone", 'like', "%{$self->key}%");
            });
        }
        return $builder;
    }

    /**
     * @return array
     */
    public function getErrorMessage(): array
    {
        return [
            'key.string'    => trans('organization/validation.field.string'),
            'key.min'       => trans('organization/validation.field.min'),
        ];
    }
}
