<?php

namespace App\Filters\Organization;

use App\Enum\TaxActivityTypeEnum;
use App\Enum\TaxMemberTypeEnum;
use App\Filters\BaseFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Rule;

/**
 * Class AdditionalFeeFilter
 * @package App\Filters\Organization
 */
class AdditionalFeeFilter extends BaseFilter
{
    /**
     * @var string|null
     */
    public ?string $key = null;
    /**
     * @var int|null
     */
    public ?int $is_active          = null;
    /**
     * @var int|null
     */
    public ?int $available_online   = null;
    /**
     * @var string|null
     */
    public ?string $member_type     = null;
    /**
     * @var string|null
     */
    public ?string $activity_type   = null;


    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            'key' => [
                'nullable',
                'string',
                'min:2',
            ],
            'is_active' => [
                'nullable',
                Rule::in([0, 1]),
            ],
            'available_online' => [
                'nullable',
                Rule::in([0, 1]),
            ],
            'member_type' => [
                'nullable',
                Rule::in(TaxMemberTypeEnum::asList()),
            ],
            'activity_type' => [
                'nullable',
                Rule::in(TaxActivityTypeEnum::asList()),
            ],
        ];
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function getEloquentBuilder(Builder $builder): Builder
    {
        if ($this->key) {
            $builder->where('fee_title', 'like', "%{$this->key}%");
        }
        if ($this->is_active !== null) {
            $builder->where('is_active', '=', $this->is_active);
        }
        if ($this->available_online !== null) {
            $builder->where('available_online', '=', $this->available_online);
        }
        if ($this->member_type) {
            $builder->where('member_type', '=', $this->member_type);
        }
        if ($this->activity_type) {
            $builder->where('activity_type', '=', $this->activity_type);
        }
        return $builder;
    }

    /**
     * @return array
     */
    public function getErrorMessage(): array
    {
        return [
            'key.string'                        => trans('organization/validation.field.string'),
            'key.min'                           => trans('organization/validation.field.min'),
            'is_active.in'                      => trans('organization/validation.field.invalid'),
            'available_online.in'               => trans('organization/validation.field.invalid'),
            'member_type.in'                    => trans('organization/validation.field.invalid'),
            'activity_type.in'                  => trans('organization/validation.field.invalid'),
        ];
    }
}
