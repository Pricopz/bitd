<?php

namespace App\Filters\Organization;

use App\Filters\BaseFilter;
use App\Models\Organization\FishingBackground;
use App\Rules\Admin\CheckCountyId;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class AllowMembersOnlineOrganizationFilter
 * @package App\Filters\Organization
 */
class AllowMembersOnlineOrganizationFilter extends BaseFilter
{
    /**
     * @var int|null
     */
    public ?int $county_id = null;
    /**
     * @var string|null
     */
    public ?string $key = null;

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            'county_id' => [
                'nullable',
                new CheckCountyId(),
            ],
            'key' => [
                'nullable',
                'string',
                'min:3',
            ],
        ];
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function getEloquentBuilder(Builder $builder): Builder
    {
        $t1 = FishingBackground::getTableName();
        if ($this->key) {
            $builder->where("{$t1}.title", 'like', "%{$this->key}%");
        }
        if ($this->county_id) {
            $builder->where("{$t1}.county_id", '=', "{$this->county_id}");
        }
        return $builder;
    }

    /**
     * @return array
     */
    public function getErrorMessage(): array
    {
        return [
            'key.string'    => trans('organization/validation.field.string'),
            'key.min'       => trans('organization/validation.field.min'),
        ];
    }
}
