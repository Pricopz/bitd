import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { LayoutsModule } from 'src/app/layouts/layouts.module'

import { LoginComponent } from 'src/app/pages/auth/login/login.component'
import { Error500Component } from 'src/app/pages/auth/500/500.component'
import { Error404Component } from 'src/app/pages/auth/404/404.component'
import { AntiAuthGuard } from '../../components/cleanui/layout/Guard/anti-auth.guard'
import { RegisterComponent } from "./register/register.component";

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    data: { title: 'Login' },
    resolve: [AntiAuthGuard],
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: { title: 'Register' },
    resolve: [AntiAuthGuard]
  },
  {
    path: '404',
    component: Error404Component,
    data: { title: 'Error 404' },
  },
  {
    path: '500',
    component: Error500Component,
    data: { title: 'Error 500' },
  },
]

@NgModule({
  imports: [LayoutsModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRouterModule {}
