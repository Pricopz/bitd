import { ValidatorFn, FormControl } from '@angular/forms'

export function conditional(predicate, validator): ValidatorFn {
  return ((formControl: FormControl) => {
    if (!formControl.parent) {
      return null
    }
    if (predicate()) {
      return validator(formControl)
    }
    return null
  })
}
