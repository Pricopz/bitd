import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'

export interface IPSize {
  label: string
  size: number
}

@Component({
  selector: 'app-pagination-size',
  templateUrl: './pagination-size.component.html',
  styleUrls: ['./pagination-size.component.scss'],
})
export class PaginationSizeComponent implements OnInit {
  @Input() label: string
  @Input() currentSize: number
  @Input() sizes: IPSize[]
  @Input() total: number
  @Input() currentPage: number

  @Output() sizeChangeEvent: EventEmitter<number> = new EventEmitter<number>()

  private _defaultSizes: IPSize[] = [
    { size: 25, label: '25' },
    { size: 50, label: '50' },
    { size: 75, label: '75' },
    { size: 500, label: '500' },
  ]

  constructor() {}

  public onChangeValue(): void {
    this.sizeChangeEvent.emit(this.currentSize)
  }

  public getI1(): number {
    if (this.currentPage && this.currentSize) {
      return (this.currentPage - 1) * this.currentSize + 1
    }
    return 0
  }

  public getI2(): number {
    if (this.currentSize && this.currentPage && this.total) {
      if (this.currentSize > this.total) {
        return this.total
      } else {
        const s = this.currentPage * this.currentSize
        return s > this.total ? this.total : s
      }
    }
    return 0
  }

  ngOnInit(): void {
    if (!this.sizes) {
      this.sizes = this._defaultSizes
    }
    if (!this.label) {
      this.label = 'Items per page'
    }
  }
}
