export interface IPaginationModel {
  size?: number
  no?: number
}

export class PaginationModel implements IPaginationModel {
  public size: number = 25
  public no: number   = 1

  constructor(model?: IPaginationModel) {
    if (model) {
      Object.assign(this, model)
    }
  }

  static fromJson(object: IPaginationModel): PaginationModel {
    const obj = new PaginationModel()
    obj.size  = object.size ? object.size : null
    obj.no    = object.no ? object.no : null
    return obj
  }
}
