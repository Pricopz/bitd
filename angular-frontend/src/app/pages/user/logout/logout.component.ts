import { Component, OnInit } from '@angular/core'
import { Router } from "@angular/router"
import { BasicAuthService } from "../../../services/basic-auth.service"

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(private service: BasicAuthService, private router: Router) { }

  async ngOnInit(): Promise<void> {
    await this.service.logout()
    await this.router.navigate(['/posts'])
  }
}
