<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CountyCitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (($handle = fopen(dirname(__DIR__) . '/../cities.csv', 'rb')) !== FALSE) {
            $k = 0;
            while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
                if ($k > 0) {
                    \DB::table('county_city')->insert([
                        ['id' => $data[0], 'city' => $data[1], 'county_id' => $data[2]]
                    ]);
                }
                $k++;
            }
            fclose($handle);
        }
    }

    /*
    179739,Malu,5629
    179748,Cosoba,5629
    179757,Herăşti,5629
    179766,Isvoarele,5629
    179775,Săbăreni,5629
    179962,Gălbinaşi,5610
    180055,Crivăţ,5610
    */
}
