<?php

namespace App\Console\Commands\Organization;

use App\Enum\MemberStatusEnum;
use App\Exceptions\EntitySavingException;
use App\Models\Organization\Fee;
use App\Models\Organization\Member;
use App\Models\Organization\MemberFeePaymentTracking;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

/**
 * Class YearlyInsertMemberFeeToPay
 * @package App\Console\Commands
 */
class InsertMemberHuntingFeeToPay extends Command
{
    /**
     * @var string
     */
    protected $signature = 'member:insert-member-hunting-fee';

    /**
     * @var string \
     */
    protected $description = 'Insert at the begin of each season hunting fee that each member has to pay';


//            $t1 = Member::getTableName();
//            $t2 = MemberFeePaymentTracking::getTableName();
//            $members = Member::query()
//                ->leftJoin($t2, static function ($join) use ($t1, $t2) {
//                    $join->on($t1 . '.id', '=', $t2 . '.member_id')->where($t2 . '.is_hunter', '=', 1);
//                })
//                ->where($t1 . '.is_hunter', '=', 1)
//                ->where('status', '=', MemberStatusEnum::ACTIVE)
//                ->where('year', '!=', $currentYear)
//                ->havingRaw("(select count(member_id) from {$t2} where year = {$currentYear}) = 0")
//                ->get($t1 . '.*')
//                ->all();

    /**
     * Handle
     */
    public function handle(): void
    {
        $currentYear = date('Y');
        try {
            # Get already inserted Hunting Members
            $existingIds = MemberFeePaymentTracking::query()
                ->where('is_hunter', '=', 1)
                ->where('year', '=', $currentYear)
                ->pluck('member_id')
                ->toArray();
            $query = Member::query()
                ->where('is_hunter', '=', 1)
                ->where('status', '=', MemberStatusEnum::ACTIVE);
            # Exclude already inserted Hunting Members
            if (count($existingIds) > 0) {
                $query->whereNotIn('id', $existingIds);
            }
            # Iterate through each member and insert hunting fee
            foreach ($query->get()->all() as $member) {
                try {
                    $fee = $member->getHuntingFee(new Fee());
                    if ($fee) {
                        $feePaymentTracking = new MemberFeePaymentTracking([
                            'member_id' => $member->id,
                            'organization_id' => $member->organization_id,
                            'fee_id' => $fee->id,
                            'is_hunter' => 1,
                            'is_fisherman' => 0,
                            'fee_amount' => $fee->getValue(),
                            'paid_amount' => 0,
                            'pay_date' => Carbon::now(),
                            'is_payment' => 0,
                            'year' => $currentYear
                        ]);
                        if (!$feePaymentTracking->save()) {
                            throw new EntitySavingException('Could not save hunting fee for member with ID ' . $member->id);
                        }
                        echo 'Inserted hunting fee for member with ID ' . $member->id . PHP_EOL;
                    } else {
                        throw new EntitySavingException('Could not detect hunting fee for member with ID ' . $member->id);
                    }
                } catch (\Exception $e) {
                    // TODO ~ Maybe insert into DB
                    Log::error($e->getMessage());
                    echo 'Error: ' . $e->getMessage() . PHP_EOL;
                }
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            echo 'Error: ' . $e->getMessage();
        }
    }
}
