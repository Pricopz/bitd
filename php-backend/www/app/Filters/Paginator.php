<?php

namespace App\Filters;

/**
 * Class Paginator
 * @package App\Filters
 */
class Paginator
{
    /**
     * @var int
     */
    public int $page        = 1;
    /**
     * @var int
     */
    public int $perPage     = 25;
    /**
     * @var array
     */
    protected array $columns   = ['*'];
    /**
     * @var string
     */
    protected string $pageName = 'page';

    /**
     * @var bool
     */
    protected bool $usePaginator = false;

    /**
     * @return bool
     */
    public function isUsePaginator(): bool
    {
        return $this->usePaginator;
    }

    /**
     * @param bool $usePaginator
     */
    public function setUsePaginator(bool $usePaginator): void
    {
        $this->usePaginator = $usePaginator;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

    /**
     * @return string
     */
    public function getPageName(): string
    {
        return $this->pageName;
    }

    /**
     * @param string $pageName
     */
    public function setPageName(string $pageName): void
    {
        $this->pageName = $pageName;
    }

    /**
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data = null): self
    {
        $self = new static();
        if ($data) {
            $self->setUsePaginator(isset($data['page']) ? true : false);
            foreach ($data as $property => $value) {
                if (isset($data[$property])) {
                    $self->{$property} = $value;
                }
            }
        }
        return $self;
    }
}
