import { NgModule } from '@angular/core'
import { SharedModule } from 'src/app/shared.module'
import { AuthRouterModule } from './auth-routing.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

// system pages
import { LoginComponent } from './login/login.component'
import { Error500Component } from './500/500.component';
import { RegisterComponent } from './register/register.component'

const COMPONENTS = [
  LoginComponent,
  Error500Component,
]

@NgModule({
  imports: [SharedModule, AuthRouterModule, FormsModule, ReactiveFormsModule],
  declarations: [...COMPONENTS, RegisterComponent],
})
export class AuthModule {}
