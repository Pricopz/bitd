import { Component, OnInit } from '@angular/core'
import { Router, NavigationStart, NavigationEnd } from '@angular/router'
import { filter } from 'rxjs/operators'
import * as _ from 'lodash'
import { MenuService } from 'src/app/services/menu.service'
import { BasicAuthService } from '../../../../../services/basic-auth.service'

@Component({
  selector: 'cui-menu-top',
  templateUrl: './menu-top.component.html',
  styleUrls: ['./menu-top.component.scss'],
})
export class MenuTopComponent implements OnInit {
  private _menuData: any[]
  private _menuDataActivated: any[]
  private _menuDataChildrenActivated: any[]
  private readonly role: string
  private readonly modules: string[] = []

  public childrenOfChildren: any[] = []

  public get menuDataActivated(): any[] {
    return this._menuDataActivated
  }

  public get menuDataChildrenActivated(): any[] {
    return this._menuDataChildrenActivated
  }

  constructor(
    private menuService: MenuService,
    private basicAuthService: BasicAuthService,
    private router: Router,
  ) {
    this.role = this.basicAuthService.getUserRole()
  }

  public hasAccess(item: any): boolean {
    let roles: string[] = []
    let whenModuleOnly: string = null
    if (item.roles) {
      roles = item.roles
    }
    if (item.whenModuleOnly) {
      whenModuleOnly = item.whenModuleOnly
    }
    if (whenModuleOnly && this.modules.indexOf(whenModuleOnly) === -1) {
      return false
    }
    return roles.indexOf(this.role) !== -1
  }

  ngOnInit() {
    this.menuService.getMenuData().subscribe(menuData => (this._menuData = menuData))
    this.activateMenu(this.router.url)
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        this.activateMenu(event.url ? event.url : null)
      })
  }

  private getPathFromURL(url: any) {
    // Query params
    if (url.indexOf('?') !== -1) {
      return url.split('?')[0]
    }
    // Params
    if (url.indexOf(';') !== -1) {
      return url.split(';')[0]
    }
    return url
  }

  private activateMenu(url: any, menuData = this._menuData) {
    this.childrenOfChildren = []
    url = this.getPathFromURL(url)
    menuData = JSON.parse(JSON.stringify(menuData))
    const pathWithSelection = this.getPath({ url: url }, menuData, (entry: any) => entry, 'url')
    if (pathWithSelection) {
      this._menuDataChildrenActivated = pathWithSelection[0].children
    }
  }

  private getPath(
    element: any,
    source: any,
    property: any,
    keyProperty = 'key',
    childrenProperty = 'children',
    path = [],
  ) {
    let found = false
    const getElementChildren = (value: any) => _.get(value, childrenProperty)
    const getElementKey = (value: any) => _.get(value, keyProperty)
    const key = getElementKey(element)
    return (
      _.some(source, (e: any) => {
        if (getElementKey(e) === key || key.indexOf(getElementKey(e)) !== -1) {
          path.push(e)
          return true
        } else {
          return (found = this.getPath(
            element,
            getElementChildren(e),
            property,
            keyProperty,
            childrenProperty,
            path.concat(e),
          ))
        }
      }) &&
      (found || _.map(path, property))
    )
  }
}
