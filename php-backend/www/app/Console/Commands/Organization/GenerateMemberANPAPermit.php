<?php

namespace App\Console\Commands\Organization;

use App\Enum\FishingPermitsStatusEnum;
use App\Models\FishingPermit;
use App\Models\Organization\Member;
use App\Models\Organization\MemberFishingPermitsStatusTracking;
use App\Models\Organization\MemberGenerateAnpaPermitRequest;
use App\Models\Organization\OrganizationAnpaAuthToken;
use App\Models\Organization\OrganizationSettings;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;

/**
 * Class GenerateMemberANPAPermit
 * @package App\Console\Commands\Organization
 *
 * Run at each 1 minute
 */
class GenerateMemberANPAPermit extends Command
{
    private const MAX_ALLOWED_TRIES = 5;
    /**
     * @var string
     */
    protected $signature = 'member:generate-member-anpa-permit {--verifySSL=}';
    /**
     * @var string
     */
    protected $description = 'Run at each 1 minute to generate ANPA permits for all the qualified members';
    /**
     * @var Client
     */
    private Client $httpClient;
    /**
     * @var bool
     */
    private bool $verifySSL = true;
    /**
     * @var array
     */
    private array $organizations = [];

    /**
     * Handle request
     */
    public function handle(): void
    {
        try {
            $this->initArguments();
            $this->initHttpClient();
            /** @var MemberGenerateAnpaPermitRequest $request */
            foreach ($this->getRequests() as $request) {
                // Configure Organization IF is not configured
                $organizationId = $request->organization_id;
                if (!isset($this->organizations[$organizationId])) {
                    $this->initOrganization($organizationId, $request);
                }
                // Generate Permits for each member
                if (isset($this->organizations[$organizationId])) {
                    $this->generateMemberPermit($organizationId, $request);
                }
            }
            $this->info('Job done.');
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            $this->error($e->getMessage());
        }
    }

    /**
     * @param int $organizationId
     * @param MemberGenerateAnpaPermitRequest $request
     */
    private function generateMemberPermit(int $organizationId, MemberGenerateAnpaPermitRequest $request): void
    {
        /** @var Member $member */
        $member = Member::query()->where('id', '=', $request->member_id)->first();
        if ($member) {
            if ($this->organizations[$organizationId]['quota']) {
                $this->saveMemberANPAPermit($request, $member, $organizationId);
            } else {
                $request->tries_no = 5;
                $request->api_response = 'Organization quota exceeded.';
                $request->update(['tries_no', 'api_response']);
            }
        } else {
            $request->tries_no = 5;
            $request->api_response = 'Member not found.';
            $request->update(['tries_no', 'api_response']);
        }
    }

    /**
     * @param int $organizationId
     * @param MemberGenerateAnpaPermitRequest $request
     */
    private function initOrganization(int $organizationId, MemberGenerateAnpaPermitRequest $request): void
    {
        try {
            if ($token = $this->getOrganizationToken($organizationId, $request['anpa_username'], $request['anpa_password'])) {
                $quota  = $request['anpa_quota'];
                $name   = $request['anpa_organization_name'];
                // GET Profile IF we need it
                if (!$quota || !$name) {
                    $profile = $this->getOrganizationANPAProfile($token, $request['anpa_username']);
                    if ($profile) {
                        $fields = [];
                        if (!$quota) {
                            $quota = $profile['limitvalue'] ?? 0;
                            $fields['anpa_quota'] = $quota;
                        }
                        if (!$name) {
                            $name = $profile['name'] ?? null;
                            $fields['anpa_organization_name'] = $name;
                        }
                        OrganizationSettings::query()
                            ->where('organization_id', '=', $organizationId)
                            ->update($fields);
                    }
                }
                // Set Organization Info
                $this->organizations[$organizationId] = [
                    'username'  => $request['anpa_username'],
                    'token'     => $token,
                    'quota'     => $this->getOrganizationQuota($organizationId, $quota),
                    'name'      => $name
                ];
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            $this->error($e->getMessage());
        }
    }

    /**
     * Init Arguments
     */
    private function initArguments(): void
    {
        $verifySSL = $this->option('verifySSL');
        if ($verifySSL !== null) {
            $this->verifySSL = (bool)$verifySSL;
        }
    }

    /**
     * Init Http Client
     */
    private function initHttpClient(): void
    {
        $this->httpClient = new Client(['base_uri' => env('ANPA_PERMIT_DOMAIN', 'https://permisepescuit.anpa.ro')]);
    }

    /**
     * @return array
     */
    private function getRequests(): array
    {
        $t0 = MemberGenerateAnpaPermitRequest::getTableName();
        $t1 = OrganizationSettings::getTableName();
        return MemberGenerateAnpaPermitRequest::query()
            ->join($t1, "{$t0}.organization_id", '=', "{$t1}.organization_id")
            ->where('permit_generated', '!=', 1)
            ->where('tries_no', '<=', self::MAX_ALLOWED_TRIES)
            ->where('anpa_username', '!=', null)
            ->where('anpa_password', '!=', null)
            ->whereYear("{$t0}.created_at", '=', Carbon::now()->year)
            ->get(["{$t0}.id", "{$t0}.organization_id", 'member_id', 'tries_no', 'anpa_username', 'anpa_password', 'anpa_quota', 'anpa_organization_name'])
            ->all();
    }

    /**
     * @param int $organizationId
     * @param string $username
     * @param string $password
     * @return string|null
     */
    private function getOrganizationToken(int $organizationId, string $username, string $password): ?string
    {
        /** @var OrganizationAnpaAuthToken $token */
        $token = OrganizationAnpaAuthToken::query()->where('organization_id', '=', $organizationId)->first();
        if (!$token || Carbon::now()->gt($token->expire_at)) {
            if ($newToken = $this->getOrganizationANPAToken($username, $password)) {
                $expiresIn      = $newToken['expires_in'] - 300; // Remove 5 minutes for safety...
                $newExpireAt    = Carbon::now()->addSeconds($expiresIn > 0 ? $expiresIn : $newToken['expires_in']);
                if (!$token) {
                    $token = new OrganizationAnpaAuthToken([
                        'organization_id'   => $organizationId,
                        'token'             => $newToken['access_token'],
                        'expire_at'         => $newExpireAt,
                    ]);
                    $token->save();
                } else {
                    $token->token       = $newToken['access_token'];
                    $token->expire_at   = $newExpireAt;
                    $token->update(['token', 'expire_at']);
                }
                return $token->token;
            }
            return null;
        }
        return $token->token;
    }

    /**
     * @param string $username
     * @param string $password
     * @return array|null
     */
    private function getOrganizationANPAToken(string $username, string $password): ?array
    {
        $usernameANPA   = env('ANPA_GENERATE_TOKEN_USERNAME');
        $passwordANPA   = env('ANPA_GENERATE_TOKEN_PASSWORD');
        $credentials    = base64_encode("{$usernameANPA}:{$passwordANPA}");
        $options        = [
            'headers' => [
                'Authorization' => "Basic {$credentials}",
            ],
            RequestOptions::FORM_PARAMS => [
                'grant_type'=> 'password',
                'username'  => $username,
                'password'  => Crypt::decrypt($password),
                'scope'     => 'operator2',
            ],
            'timeout' => 5,
            'connect_timeout' => true,
            'verify' => $this->verifySSL,
        ];
        try {
            $response   = $this->httpClient->post('token', $options);
            $rawContent = $response->getBody()->getContents();
            return \json_decode($rawContent, true);
        } catch (GuzzleException $e) {
            Log::error($e->getMessage());
            $this->error($e->getMessage());
        }
        return null;
    }

    /**
     * @param MemberGenerateAnpaPermitRequest $request
     * @param Member $member
     * @param int $organizationId
     */
    private function saveMemberANPAPermit(MemberGenerateAnpaPermitRequest $request, Member $member, int $organizationId): void
    {
        try {
            $permit = $this->getMemberANPAPermit($member, $organizationId);
            $this->organizations[$organizationId]['quota'] -=1;

            $request->tries_no = $request->tries_no + 1;
            $request->api_response = json_encode($permit);
            $request->permit_generated = 1;
            $request->update(['permit_generated', 'tries_no', 'api_response']);

            // Fishing Permits
            $currentYear    = Carbon::now()->year;
            /** @var FishingPermit $fishingPermit */
            $fishingPermit  = FishingPermit::query()
                ->where('member_id', '=', $member->id)
                ->where('year', '=', $currentYear)
                ->first();
            if ($fishingPermit) {
                $fishingPermit->update([
                    'number'            => $permit['Entries']['Entry']['returnednumber'] ?? null,
                    'returned_id'       => $permit['Entries']['Entry']['returnedid'] ?? null,
                    'date_issue'        => Carbon::now(),
                    'date_expire'       => $currentYear . '-12-31',
                    'status'            => FishingPermitsStatusEnum::ACTIVE
                ]);
                /** @var MemberFishingPermitsStatusTracking $oldFishingPermitTracking */
                $oldFishingPermitTracking  = MemberFishingPermitsStatusTracking::query()
                    ->where('permit_id', '=', $fishingPermit->id)
                    ->where('member_id', '=', $member->id)
                    ->orderBy('id', 'DESC')
                    ->first();
                if ($oldFishingPermitTracking) {
                    # Save Permit Status Tracking
                    $fishingPermitTracking                              = new MemberFishingPermitsStatusTracking();
                    $fishingPermitTracking->permit_id                   = $fishingPermit->id;
                    $fishingPermitTracking->member_id                   = $member->id;
                    $fishingPermitTracking->organization_id             = $member->organization_id;
                    $fishingPermitTracking->prev_status                 = $oldFishingPermitTracking->current_status;
                    $fishingPermitTracking->current_status              = FishingPermitsStatusEnum::ACTIVE;
                    $fishingPermitTracking->prev_status_changed_date    = $oldFishingPermitTracking->current_status_changed_date;
                    $fishingPermitTracking->current_status_changed_date = Carbon::now();
                    $fishingPermitTracking->is_single_row               = 0;
                    $fishingPermitTracking->is_last_row                 = 1;
                    $fishingPermitTracking->save();
                    // Update Old Status Tracking
                    $oldFishingPermitTracking->update(['is_single_row' => 0, 'is_last_row' => 0]);
                } else {
                    # Save Permit Status Tracking
                    $fishingPermitTracking                              = new MemberFishingPermitsStatusTracking();
                    $fishingPermitTracking->permit_id                   = $fishingPermit->id;
                    $fishingPermitTracking->member_id                   = $member->id;
                    $fishingPermitTracking->organization_id             = $member->organization_id;
                    $fishingPermitTracking->prev_status                 = null;
                    $fishingPermitTracking->current_status              = FishingPermitsStatusEnum::ACTIVE;
                    $fishingPermitTracking->prev_status_changed_date    = null;
                    $fishingPermitTracking->current_status_changed_date = Carbon::now();
                    $fishingPermitTracking->is_single_row               = 1;
                    $fishingPermitTracking->is_last_row                 = 1;
                    $fishingPermitTracking->save();
                }
            } else {
                $fishingPermit = new FishingPermit([
                    'member_id'         => $member->id,
                    'organization_id'   => $member->organization_id,
                    'number'            => $permit['Entries']['Entry']['returnednumber'] ?? null,
                    'returned_id'       => $permit['Entries']['Entry']['returnedid'] ?? null,
                    'date_issue'        => Carbon::now(),
                    'date_expire'       => $currentYear . '-12-31',
                    'status'            => FishingPermitsStatusEnum::ACTIVE
                ]);
                $fishingPermit->save();
                // Tracking Status
                $fishingPermitTracking                              = new MemberFishingPermitsStatusTracking();
                $fishingPermitTracking->permit_id                   = $fishingPermit->id;
                $fishingPermitTracking->member_id                   = $member->id;
                $fishingPermitTracking->organization_id             = $member->organization_id;
                $fishingPermitTracking->prev_status                 = null;
                $fishingPermitTracking->current_status              = FishingPermitsStatusEnum::ACTIVE;
                $fishingPermitTracking->prev_status_changed_date    = null;
                $fishingPermitTracking->current_status_changed_date = Carbon::now();
                $fishingPermitTracking->is_single_row               = 1;
                $fishingPermitTracking->is_last_row                 = 1;
                $fishingPermitTracking->save();
            }
            $this->info("ANPA permit generated for the member ID: #{$member->id}.");
        } catch (GuzzleException $e) {
            $request->tries_no = $request->tries_no + 1;
            $request->api_response = $e->getMessage();
            $request->update(['tries_no', 'api_response']);

            Log::error($e->getMessage());
            $this->error($e->getMessage());
        } catch (\Exception $e) {
            $request->tries_no = $request->tries_no + 1;
            $request->api_response = $e->getMessage();
            $request->update(['tries_no', 'api_response']);

            Log::error($e->getMessage());
            $this->error($e->getMessage());
        }
    }

    /**
     * @param Member $member
     * @param int $organizationId
     * @return array|null
     * @throws GuzzleException
     */
    private function getMemberANPAPermit(Member $member, int $organizationId): ?array
    {
        $token = $this->organizations[$organizationId]['token'];
        $options = [
            'headers' => [
                'Authorization' => "Bearer {$token}",
            ],
            RequestOptions::JSON => [
                '_postinsertpermis' => $this->getFormattedMemberData($organizationId, $member),
            ],
            'timeout' => 5,
            'connect_timeout' => true,
            'verify' => $this->verifySSL,
        ];
        $response   = $this->httpClient->post('anpa2/insertpermis', $options);
        $rawContent = $response->getBody()->getContents();
        return \json_decode($rawContent, true);
    }

    /**
     * @param int $organizationId
     * @param Member $member
     * @return array
     */
    private function getFormattedMemberData(int $organizationId, Member $member): array
    {
        $anpaUsername = strtoupper($this->organizations[$organizationId]['username']);
        return [
            "pfirstname"    => $member->first_name,
            "plastname"     => $member->last_name,
            "pcnp"          => $member->cnp,
            "pregion"       => $member->getAnpaRegion(),
            "paddress"      => $member->getAnpaAddress(),
            "pregalldate"   => $this->getFormattedDateTime(),
            "plimittype"    => $anpaUsername,
            "pseriestype"   => $anpaUsername,
            "pstatustype"   => $anpaUsername
        ];
    }

    /**
     * @return string
     */
    private function getFormattedDateTime(): string
    {
        $now = microtime(true);
        return Carbon::now()->format('Y-m-d\TH:i:s').sprintf('.%03dZ', round(($now - floor($now)) * 1000));
    }

    /**
     * @param string $token
     * @param string $username
     * @return array|null
     */
    private function getOrganizationANPAProfile(string $token, string $username): ?array
    {
        try {
            $options = [
                'headers' => [
                    'Authorization' => "Bearer {$token}",
                    'Accept' => 'application/xml'
                ],
                'timeout' => 5,
                'connect_timeout' => true,
                'verify' => $this->verifySSL,
            ];
            $username       = urlencode($username);
            $response       = $this->httpClient->get("anpa2/getlimitbystatus?plicence={$username}", $options);
            $rawContent     = $response->getBody()->getContents();
            $responseXml    = new \SimpleXMLElement($rawContent, LIBXML_NOWARNING);
            return (array)$responseXml->Entry;
        } catch (GuzzleException $e) {
            Log::error($e->getMessage());
            $this->error($e->getMessage());
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            $this->error($e->getMessage());
        }
        return null;
    }

    /**
     * @param int $organizationId
     * @param int $quota
     * @return int
     */
    private function getOrganizationQuota(int $organizationId, int $quota): int
    {
        // GET Remaining Quota
        $currentYearPermitsNo = FishingPermit::query()
            ->where('organization_id', '=', $organizationId)
            ->whereYear('date_expire', '=', Carbon::now()->year)
            ->where('status', '=', FishingPermitsStatusEnum::ACTIVE)
            ->count(['id']);
        return $quota - $currentYearPermitsNo;
    }
}
