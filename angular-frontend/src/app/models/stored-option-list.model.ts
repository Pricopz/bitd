export interface IStoredOptionListModel {
  key?: string
  options?: string[]
}

export class StoredOptionListModel implements IStoredOptionListModel {
  public key: string        = ''
  public options: string[]  = []

  constructor(model?: IStoredOptionListModel) {
    if (model) {
      Object.assign(this, model)
    }
  }
}
