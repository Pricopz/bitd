import { Component, OnInit } from '@angular/core'
import { PaginationModel } from '../../../../models/pagination.model'
import { HttpErrorResponse } from '@angular/common/http'
import { HttpResponseHandlerService } from '../../../../services/http-response-handler.service'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { EditPostModalComponent } from '../edit-post-modal/edit-post-modal.component'
import { PostsService } from "../../../../services/posts.service";
import { ConfirmationModalComponent } from "../../common/confirmation-modal/confirmation-modal.component";

@Component({
  selector: 'app-init',
  templateUrl: './init.component.html',
  styleUrls: ['./init.component.scss']
})
export class InitComponent implements OnInit {
  private _pagination: PaginationModel = new PaginationModel()
  private _items: any[] = []
  private _collectionSize: number = 0
  private _isLoading: boolean = false

  public get items(): any[] {
    return this._items
  }

  public get page(): number {
    return this._pagination.no
  }

  public get pageSize(): number {
    return this._pagination.size
  }

  public get collectionSize(): number {
    return this._collectionSize
  }

  public get isLoading(): boolean {
    return this._isLoading
  }

  constructor(private service: PostsService,
              private modalService: NgbModal,
              private responseHandlerService: HttpResponseHandlerService) { }

  public onEditPostModalOpen(item: any) {
    const modalRef = this.modalService.open(EditPostModalComponent, { size: 'md' })
    modalRef.componentInstance.item = item
    modalRef.result
      .then((result: {item: any}) => {
        const _i = this._items.filter(i => i.id === item.id)
        if (_i[0]) {
          _i[0].title = result.item.title
        }
      })
      .catch(res => {})
  }

  public onDeletePostModalOpen(item: any) {
    const modalRef = this.modalService.open(ConfirmationModalComponent, { size: 'md' })
    modalRef.componentInstance.item = item
    modalRef.result
      .then((result: any) => {
        this.onDelete(item.id)
      })
      .catch(res => {})
  }

  private onDelete(id: number) {
    this.service.deletePost(id).subscribe(
      (resource: any) => {
        this.getPosts()
        this._isLoading = false
      },
      (error: HttpErrorResponse) => {
        this.responseHandlerService.notifyError(error)
        this._isLoading = true
      },
    )
  }

  public onPageChanged(event) {
    this._pagination.no = event
    this.getPosts()
  }

  private getPosts(): void {
    this._isLoading = true
    this.service.getAuthorsPosts().subscribe(
      (resource: any) => {
        this._items = resource
        this._isLoading = false
      },
      (error: HttpErrorResponse) => {
        this.responseHandlerService.notifyError(error)
        this._isLoading = true
      },
    )
  }

  ngOnInit(): void {
    this.getPosts()
  }
}
