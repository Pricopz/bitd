import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { SharedModule } from '../../../shared.module'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { InitComponent } from './init/init.component'
import { PostRoutingModule } from './post-routing.module'
import { EditPostModalComponent } from './edit-post-modal/edit-post-modal.component';
import { PostDetailsComponent } from './post-details/post-details.component';
import { CreatePostComponent } from './create-post/create-post.component'

const COMPONENTS = [InitComponent, EditPostModalComponent]

@NgModule({
  declarations: [...COMPONENTS, PostDetailsComponent, CreatePostComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    PostRoutingModule,
    NgbModule,
  ],
  exports: [
  ],
  entryComponents: [
    EditPostModalComponent
  ]
})
export class PostModule {}
