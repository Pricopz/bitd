import { Injectable } from '@angular/core'
import {HttpClient, HttpHeaders} from '@angular/common/http'
import {Observable} from 'rxjs'
import {map} from 'rxjs/operators'
import {environment} from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private apiURL: string = environment.apiPostUrl + '/posts'
  constructor(private http: HttpClient) { }

  public getPosts(): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json')

    const url = this.apiURL
    return this.http.get(url, { headers: headers }).pipe(
      map((resource: any[]) => {
        return resource
      }),
    )
  }

  public getPost(id: number): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
    const url = this.apiURL + '/' + encodeURIComponent(id)
    return this.http.get(url, { headers: headers }).pipe(
      map((resource: any[]) => {
        return resource
      }),
    )
  }

  public getAuthorsPosts(): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
    const url = this.apiURL + '/by/author'
    return this.http.get(url, { headers: headers }).pipe(
      map((resource: any[]) => {
        return resource
      }),
    )
  }

  public updatePost(id: number, data: {title: string, description: any}): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
    const url = this.apiURL + '/' + encodeURIComponent(id)
    return this.http.put(url, data, { headers: headers }).pipe(
      map((resource: any[]) => {
        return resource
      }),
    )
  }

  public createPost(data: {title: string, description: any}): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
    const url = this.apiURL
    return this.http.post(url, data, { headers: headers }).pipe(
      map((resource: any[]) => {
        return resource
      }),
    )
  }

  public deletePost(id: number): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
    const url = this.apiURL + '/' + encodeURIComponent(id)
    return this.http.delete(url, { headers: headers }).pipe(
      map((resource: any[]) => {
        return resource
      }),
    )
  }
}
