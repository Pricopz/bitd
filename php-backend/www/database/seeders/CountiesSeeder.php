<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CountiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (($handle = fopen(dirname(__DIR__) . '/../counties.csv', 'rb')) !== FALSE) {
            $k = 0;
            while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
                if ($k > 0) {
                    \DB::table('counties')->insert([
                        ['id' => $data[0], 'name' => $data[1]]
                    ]);
                }
                $k++;
            }
            fclose($handle);
        }
    }
}
