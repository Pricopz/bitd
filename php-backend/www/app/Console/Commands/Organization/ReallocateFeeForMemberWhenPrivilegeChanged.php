<?php

namespace App\Console\Commands\Organization;

use App\Models\Organization\Fee;
use App\Models\Organization\Member;
use App\Models\Organization\MemberFeePaymentTracking;
use App\Models\Organization\MemberPrivilegeChangeDetection;
use App\Models\Organization\Organization;
use Carbon\Carbon;
use Illuminate\Console\Command;

/**
 * Class ReallocateFeeForMemberWhenPrivilegeChanged
 * @package App\Console\Commands\Organization
 */
class ReallocateFeeForMemberWhenPrivilegeChanged extends Command
{
    /**
     * @var string
     */
    protected $signature = 'member:reallocate-fee-on-privilege-changed';

    /**
     * @var string \
     */
    protected $description = 'Reallocate fee when privileges are changed on update';


    public function handle(): void
    {
        $year = Carbon::now()->year;

        foreach ($this->getDetectedChanges($year) as $item) {
            /**
             * @var MemberPrivilegeChangeDetection $item
             */
            try {
                if (($member = $item->member()->first()) && ($organization = $item->organization()->first())) {

                    /**
                     * @var Member $member
                     * @var Organization $organization
                     */
                    // IF Hunter
                    if ($member->is_hunter) {
                        $fee = (new Fee())->getHuntingFeeByOrgMember($organization, $member);
                        if ($fee) {
                            /**
                             * @var MemberFeePaymentTracking $memberFeePaymentTracking
                             */
                            $memberFeePaymentTracking = MemberFeePaymentTracking::query()
                                ->where('member_id', '=', $member->id)
                                ->where('is_hunter', '=', 1)
                                ->where('is_payment', '=', 0)
                                ->where('year', '=', $year)
                                ->first();

                            if ($memberFeePaymentTracking && (int)$memberFeePaymentTracking->fee_id !== (int)$fee->id) {
                                // Update
                                MemberFeePaymentTracking::query()
                                    ->where('member_id', '=', $member->id)
                                    ->where('is_hunter', '=', 1)
                                    ->where('year', '=', $year)
                                    ->update(['fee_id' => $fee->id, 'fee_amount' => $fee->getValue()]);

                                echo 'Reallocate Hunting fee for Member ID ' . $item['member_id'] . PHP_EOL;
                            }
                        }
                    }
                    // IF fisherman
                    if ($member->is_fisherman) {
                        $fee = (new Fee())->getFishingFeeByOrgMember($organization, $member);
                        if ($fee) {
                            /**
                             * @var MemberFeePaymentTracking $memberFeePaymentTracking
                             */
                            $memberFeePaymentTracking = MemberFeePaymentTracking::query()
                                ->where('member_id', '=', $member->id)
                                ->where('is_fisherman', '=', 1)
                                ->where('is_payment', '=', 0)
                                ->where('year', '=', $year)
                                ->first();
                            if ($memberFeePaymentTracking && (int)$memberFeePaymentTracking->fee_id !== (int)$fee->id) {
                                // Update
                                MemberFeePaymentTracking::query()
                                    ->where('member_id', '=', $member->id)
                                    ->where('is_fisherman', '=', 1)
                                    ->where('year', '=', $year)
                                    ->update(['fee_id' => $fee->id, 'fee_amount' => $fee->getValue()]);

                                echo 'Reallocate Fishing fee for Member ID ' . $item['member_id'] . PHP_EOL;
                            }
                        }
                    }
                }
                $item->update(['processed' => 1]);
            } catch (\Exception $e) {
                echo 'Fail reallocate fee for Member ID ' . $item['member_id'] . PHP_EOL;
                echo 'Error: ' . $e->getMessage() . PHP_EOL;
            }
        }
    }

    /**
     * @param int $year
     * @return array
     */
    private function getDetectedChanges(int $year): array
    {
        return MemberPrivilegeChangeDetection::query()
            ->where('processed', '!=', 1)
            ->where('year', '=', $year)
            ->get()
            ->all();
    }
}
