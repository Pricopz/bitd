<?php

namespace App\Filters\Organization;

use App\Filters\BaseFilter;
use App\Models\Organization\OrganizationProvisionDetails;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class OrganizationMoneyTrackFilter
 * @package App\Filters\Organization
 */
class OrganizationMoneyTrackFilter extends BaseFilter
{
    /**
     * @var string|null
     */
    public ?string $details = null;

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            'details' => [
                'required',
                'string',
                'min:3',
            ]
        ];
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function getEloquentBuilder(Builder $builder): Builder
    {
        if ($this->details) {
            $self   = $this;
            $t0     = OrganizationProvisionDetails::getTableName();
            $builder->where(static function ($query) use ($self, $t0) {
                $query->where("{$t0}.first_name", 'like', "%{$self->details}%")
                    ->orWhere("{$t0}.last_name", 'like', "%{$self->details}%")
                    ->orWhere("{$t0}.cnp", 'like', "%{$self->details}%")
                    ->orWhere("{$t0}.email", 'like', "%{$self->details}%")
                    ->orWhere("{$t0}.phone", 'like', "%{$self->details}%")
                    ->orWhere("{$t0}.reason", 'like', "%{$self->details}%")
                    ->orWhere("{$t0}.company", 'like', "%{$self->details}%");
            });
        }
        return $builder;
    }

    /**
     * @return array
     */
    public function getErrorMessage(): array
    {
        return [
            'details.required'  => trans('organization/validation.field.required'),
            'details.string'    => trans('organization/validation.field.string'),
            'details.min'       => trans('organization/validation.field.min'),
        ];
    }
}
