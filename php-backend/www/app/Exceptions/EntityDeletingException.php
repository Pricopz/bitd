<?php

namespace App\Exceptions;

/**
 * Class EntityDeletingException
 * @package App\Exceptions
 */
class EntityDeletingException extends \Exception
{

}
