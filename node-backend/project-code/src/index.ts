'use strict';

import * as express from 'express';
import * as dotenv from "dotenv";
import * as cors from "cors";
import * as bodyParser from 'body-parser';
import MySqlService from "./services/mysql.service";
import {getDBConfig} from "./config/db.config";
import {RouterManager} from "./routes/router-manager";
import {contentTypeHandler} from "./middlewares/contentType.middleware";

dotenv.config();

const app               = express();
const PORT: number      = parseInt(process.env.APP_PORT || '8081' as string, 10);
const HOST: string      = process.env.APP_HOST_LOCAL || 'localhost' as string;
const DB_HOST: string   = process.env.DB_HOST_LOCAL || 'localhost' as string;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

// Set Content-Type: Application/json
app.use(contentTypeHandler);

// Router Manager
const rm: RouterManager = new RouterManager();
rm.setRoutes(app);

MySqlService.getInstance()
    .initDatabase(getDBConfig(DB_HOST))
    .then((response) => {
       console.log(response);
    }, (error) => {
       console.log(error);
    });

app.listen(PORT,() => {
    console.log(`Server is running on: ${HOST}:${PORT}`);
});
// const server = app.listen(8081, function () {
//    // const host = server.address().
//    // const port = server.address().port
//
//    // console.log("Example app listening at http://%s:%s", host, port)
// })
