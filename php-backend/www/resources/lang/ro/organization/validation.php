<?php
$baseDictionary = require dirname(__DIR__) . '/validation.php';

return array_merge(
    $baseDictionary,
    [
        'organisation.unknown'                              => 'Asociatia este invalida.',
        'tc.required'                                       => 'Termeni si conditiile sunt obligatorii.',
        'email.inactive.unique'                             => 'Email-ul este deja utilizat. Daca este contul dvs. si nu l-ati activat retrimite-ti link-ul de activare.',
        'file.statute.mimes'                                => 'Formatul fisierului este invalid. Se accepta doar fisiere in format .pdf, .doc, .docx, .jpg, .jpeg sau .png.',
        'file.documents.mimes'                              => 'Formatul fisierului este invalid. Se accepta doar fisiere in format .pdf, .jpg, .jpeg sau .png.',
    ]
);
