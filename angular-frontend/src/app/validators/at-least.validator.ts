import { ValidatorFn, FormGroup } from '@angular/forms'

export function atLeast(controlsName: string[], no: number): ValidatorFn {
  return (formGroup: FormGroup): {[key: string]: boolean} | null => {
    let count = 0
    for (let i = 0; i < controlsName.length; i++) {
      const controlName   = controlsName[i]
      const controlValue  = formGroup.get(controlName).value
      if (controlValue !== null && controlValue !== '' && !isEmptyCheckBoxList(controlValue)) {
        count++
      } else if (controlValue instanceof Date) {
        count++
      }
      if (count > no) {
        break
      }
    }
    if (count < no) {
      return {invalidCount: true}
    }
    return null
  }
}
function isEmptyCheckBoxList(value: any): boolean {
  if (typeof value === 'object') {
    for (let i = 0; i < value.length; i++) {
      if (value[i] !== false) {
        return false
      }
    }
    return true
  }
  return false
}
