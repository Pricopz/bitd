<?php

namespace App\Http\Controllers\Post;

use App\Exceptions\EntityNotFoundException;
use App\Forms\Post\CreatePostForm;
use App\Http\Controllers\Controller;
use App\Services\Post\PostService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

/**
 * Class StaticPageController
 * @package App\Http\Controllers\General
 */
class PostController extends Controller
{
    /**
     * @var PostService
     */
    private PostService $service;

    /**
     * StaticPageController constructor.
     */
    public function __construct()
    {
        $this->service = new PostService();
    }

    /**
     * @return Response
     */
    public function getPosts(): Response
    {
        try {
            return $this->handleResponse(200, $this->service->getPosts());
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return $this->handleResponse(500, ['error' => 'Server error.']);
        }
    }

    /**
     * @param int $id
     * @return Response
     */
    public function getPost(int $id): Response
    {
        try {
            return $this->handleResponse(200, $this->service->getPost($id));
        } catch (EntityNotFoundException $e) {
            return $this->handleResponse(404, ['error' => 'Post not found.']);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return $this->handleResponse(500, ['error' => 'Server error.']);
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getPostsByAuthor(Request $request): Response
    {
        try {
            $user = $request->get('user.user');
            return $this->handleResponse(200, $this->service->getPostsByAuthorId($user->id));
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return $this->handleResponse(500, ['error' => 'Server error.']);
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function createPost(Request $request): Response
    {
        try {
            $user       = $request->get('user.user');
            /** @var CreatePostForm $form */
            $form       = CreatePostForm::fromArray($request->all());
            $validator  = Validator::make(
                $form->getValues(),
                $form->getRules(),
                $form->getErrorMessage()
            );
            if ($validator->fails()) {
                return $this->handleResponse(400, $validator->errors());
            }
            return $this->handleResponse(201, $this->service->createPost($user, $form));
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return $this->handleResponse(500, ['error' => 'Server error.']);
        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function updatePost(int $id, Request $request): Response
    {
        try {
            $post       = $this->service->getPost($id);
            /** @var CreatePostForm $form */
            $form       = CreatePostForm::fromArray($request->all());
            $validator  = Validator::make(
                $form->getValues(),
                $form->getRules(),
                $form->getErrorMessage()
            );
            if ($validator->fails()) {
                return $this->handleResponse(400, $validator->errors());
            }
            return $this->handleResponse(201, $this->service->updatePost($post, $form));
        } catch (EntityNotFoundException $e) {
            return $this->handleResponse(404, ['error' => 'Post not found.']);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return $this->handleResponse(500, ['error' => 'Server error.']);
        }
    }

    /**
     * @param int $id
     * @return Response
     */
    public function deletePost(int $id): Response
    {
        try {
            $post = $this->service->getPost($id);
            return $this->handleResponse(204, $this->service->deletePost($post));
        } catch (EntityNotFoundException $e) {
            return $this->handleResponse(404, ['error' => 'Post not found.']);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return $this->handleResponse(500, ['error' => 'Server error.']);
        }
    }
}
