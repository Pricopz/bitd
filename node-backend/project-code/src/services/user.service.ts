'use strict';

import * as bcrypt from 'bcryptjs';
import MySqlService from "./mysql.service";
import {TableEnum} from "../enums/table.enum";
import {UserLoginForm} from "../forms/user-login.form";
import {UserRegistrationForm} from "../forms/user-registration.form";

export default class UserService
{
    protected mySqlService: MySqlService;

    public constructor(mySqlService: MySqlService)
    {
        this.mySqlService = mySqlService;
    }

    public async create(form: UserRegistrationForm): Promise<any>
    {
        const clinicFactory = await this.mySqlService.getModelCto(TableEnum.USERS);
        return await clinicFactory.create(this.formToModel(form));
    }

    public async login(form: UserLoginForm)
    {
        try {
            const user = await this.getUserByEmail(form.email);
            if (user && this.isSamePassword(form.password, `${user.password}`.toString())) {
                return user;
            }
        } catch (e) {
            console.log(e);
            throw new Error('User could not login.');
        }
        return null;
    }

    public async getUserByEmail(email: string): Promise<any>
    {
        try {
            const userFactory = await this.mySqlService.getModelCto(TableEnum.USERS);
            return await userFactory.findOne({where: {email: email}});
        } catch (e) {
            console.log(e);
            throw new Error(e.message)
        }
    }

    protected isSamePassword(password: string, hash: string): boolean
    {
        return bcrypt.compareSync(password, hash);
    }

    protected getPasswordAsHash(password: string): string
    {
        const salt = bcrypt.genSaltSync(10);
        return bcrypt.hashSync(password, salt);
    }

    private formToModel(form: UserRegistrationForm): any
    {
        return {
            name: form.name,
            email: form.email,
            password: this.getPasswordAsHash(form.password),
        };
    }
}
