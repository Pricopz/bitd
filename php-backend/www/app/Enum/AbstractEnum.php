<?php

namespace App\Enum;

/**
 * Class AbstractEnum
 * @package App\Enum
 */
abstract class AbstractEnum
{
    /**
     * @return array
     */
    public static function asMap(): array
    {
        try {
            $rc = new \ReflectionClass(static::class);
            return $rc->getConstants();
        } catch (\ReflectionException $e) {
            return [];
        }
    }

    /**
     * @return array
     */
    public static function asList(): array
    {
        return array_keys(static::asMap());
    }
}
