import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'labelMap'
})
export class LabelMapPipe implements PipeTransform {

  transform(label: any, mapping: any): any {
    if (label in mapping) {
      return mapping[label]
    }
    return label
  }

}
