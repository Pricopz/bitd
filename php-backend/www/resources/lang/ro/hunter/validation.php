<?php
$baseDictionary = require dirname(__DIR__) . '/validation.php';

return array_merge(
    $baseDictionary,
    [
        'tc.required'              => 'Termeni si conditiile sunt obligatorii.',
        'email.inactive.unique'    => 'Email-ul este deja utilizat. Daca este contul dvs. si nu l-ati activat retrimite-ti link-ul de activare.',
    ]
);
