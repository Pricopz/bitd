import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {PostsService} from "../../../../services/posts.service";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {HttpResponseHandlerService} from "../../../../services/http-response-handler.service";
import {HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss']
})
export class CreatePostComponent implements OnInit {

  public form: FormGroup
  public title: FormControl
  public description: FormControl

  constructor(private service: PostsService,
              public router: Router,
              private responseHandlerService: HttpResponseHandlerService) { }

  public onSubmitForm() {
    if (this.form.valid) {
      this.service.createPost({title: this.form.get('title').value, description: this.form.get('description').value}).subscribe(
        (resource: any) => {
          this.responseHandlerService.notifySuccess('Success', 'Post saved with success!')
          return this.router.navigate(['/user/posts'])
        }, (error: HttpErrorResponse) => {
          this.responseHandlerService.notifyError(error)
        }
      )
    }
  }

  private initFormControls() {
    this.title = new FormControl(null, [Validators.required])
    this.description = new FormControl(null, [Validators.required])
  }

  private iniFormGroup() {
    this.form = new FormGroup(
      {
        title: this.title,
        description: this.description,
      }
    )
  }

  ngOnInit(): void {
    this.initFormControls()
    this.iniFormGroup()
  }
}
