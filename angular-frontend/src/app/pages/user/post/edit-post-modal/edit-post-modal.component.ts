import { Component, Input, OnInit } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { HttpErrorResponse } from '@angular/common/http'
import { HttpResponseHandlerService } from '../../../../services/http-response-handler.service'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { PostsService } from "../../../../services/posts.service";

@Component({
  selector: 'app-edit-post-modal',
  templateUrl: './edit-post-modal.component.html',
  styleUrls: ['./edit-post-modal.component.scss']
})
export class EditPostModalComponent implements OnInit {
  @Input() item: any
  public form: FormGroup
  public title: FormControl
  public description: FormControl

  constructor(private service: PostsService,
              private activeModal: NgbActiveModal,
              private responseHandlerService: HttpResponseHandlerService) { }

  public onSubmitForm() {
    if (this.form.valid) {
      this.service.updatePost(this.item.id, {title: this.form.get('title').value, description: this.form.get('description').value}).subscribe(
        (resource: any) => {
          this.responseHandlerService.notifySuccess('Success', 'Post changed with success!')
          this.activeModal.close({item: resource})
        }, (error: HttpErrorResponse) => {
          this.responseHandlerService.notifyError(error)
        }
      )
    }
  }

  private initFormControls() {
    this.title = new FormControl(this.item.title, [Validators.required])
    this.description = new FormControl(this.item.description, [Validators.required])
  }

  private iniFormGroup() {
    this.form = new FormGroup(
      {
        title: this.title,
        description: this.description,
      }
    )
  }

  ngOnInit(): void {
    this.initFormControls()
    this.iniFormGroup()
  }
}
