import {
  Component,
  Type,
  OnDestroy,
  AfterViewInit,
  ComponentFactoryResolver,
  OnInit,
  ComponentRef,
  ViewChild,
  ChangeDetectorRef,
  HostListener,
  ElementRef
} from '@angular/core'
import { Subject } from 'rxjs'
import { InsertionDirective } from './insertion.directive'
import { DataCardRef } from './data-card-ref'

@Component({
  selector: 'app-data-card',
  templateUrl: './data-card.component.html',
  styleUrls: ['./data-card.component.scss']
})
export class DataCardComponent implements OnInit, AfterViewInit, OnDestroy {

  private readonly _onClose = new Subject<any>()
  public componentRef: ComponentRef<any>
  public childComponentType: Type<any>
  public onClose = this._onClose.asObservable()

  @ViewChild(InsertionDirective)
  insertionPoint: InsertionDirective
  // Click Outside
  public closeOnOutsideClick: boolean
  public ignoreOutsiderElements: string[]
  public clickOnOpen: boolean = false
  @HostListener('document:click', ['$event'])
  clickedOutside(event) {
    event.stopPropagation()
    if (!this.closeOnOutsideClick) {
      return
    }
    if (!this.clickOnOpen && !this.el.nativeElement.contains(event.target)) {
      if (!this.foundOutsiderElementsInPaths(event.path)) {
        this.dataCardRef.close()
      }
    }
    this.clickOnOpen = false
  }
  // Click Outside
  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private cd: ChangeDetectorRef,
              private dataCardRef: DataCardRef,
              private el: ElementRef) { }

  ngAfterViewInit() {
    this.loadChildComponent(this.childComponentType)
    this.cd.detectChanges()
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    if (this.componentRef) {
      this.componentRef.destroy()
    }
  }

  onOverlayClicked(evt: MouseEvent) {
    this.dataCardRef.close()
  }

  onDataCardClicked(evt: MouseEvent) {
    evt.stopPropagation()
  }

  loadChildComponent(componentType: Type<any>) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(componentType)

    const viewContainerRef = this.insertionPoint.viewContainerRef
    viewContainerRef.clear()

    this.componentRef = viewContainerRef.createComponent(componentFactory)
  }

  public close() {
    this._onClose.next()
  }

  private foundOutsiderElementsInPaths(paths: any[]): boolean {
    if (!this.ignoreOutsiderElements || this.ignoreOutsiderElements.length === 0) {
      return false
    }
    return paths.some((path, index) => {
      for (const i in this.ignoreOutsiderElements) {
        if (this.ignoreOutsiderElements[i] && this.ignoreOutsiderElements[i] === path.className) {
          return true
        }
      }
    })
  }
}
