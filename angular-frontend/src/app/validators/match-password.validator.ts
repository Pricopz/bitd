import { ValidatorFn, FormGroup } from '@angular/forms'

export function matchPassword(passwordKey: string, confirmPasswordKey: string): ValidatorFn {
  return (formGroup: FormGroup): { [key: string]: boolean } | null => {
    const password = formGroup.get(passwordKey).value
    const confirmPassword = formGroup.get(confirmPasswordKey).value

    if (password !== confirmPassword) {
      return {
        mismatchPassword: true,
      }
    }
    return null
  }
}
