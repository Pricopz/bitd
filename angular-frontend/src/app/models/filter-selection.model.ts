import {HelperService} from '../services/helper.service'

export interface IFilterSelection {
  id?: number
  label?: string
  isPublic?: number
  selection?: any
}

export class FilterSelection implements IFilterSelection {
  public id: number
  public label: string
  public isPublic: number
  public selection: any

  constructor(model?: IFilterSelection) {
    if (model) {
      Object.assign(this, model)
    }
  }

  public static fromJson(object: IFilterSelection) {
    const obj     = new FilterSelection()
    obj.id        = object.id ? object.id : null
    obj.label     = object.label ? object.label : null
    obj.isPublic  = !HelperService.isNullOrUndefined(object.isPublic) ? object.isPublic : null
    obj.selection = object.selection ? object.selection : null
    return obj
  }
}
