export const environment = {
  production: true,
  authenticated: false,
  hmr: false,
  apiUrl: 'http://backend.spas.local:8085/api/v1',
  backendUrl: 'http://backend.spas.local:8085', // frontend.user.local | backend.user.local
  dataCardDocUrl: 'https://valid.okea.no/tags/DRA',
  useAD: false, // true | false Use Active Directory to login
  weeksAhead: 6, // number - for N Week Ahead Report
  useApiMock: false,
  userManualUrl: '#',
  acrAppUrl: 'https://acr.zeeksolutions.no',
}
