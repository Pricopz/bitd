import { Pipe, PipeTransform } from '@angular/core'
import { IFilterSelection } from '../models/filter-selection.model'

@Pipe({
  name: 'selectionFilter',
  pure: false
})

export class SelectionFilterPipe implements PipeTransform {
  transform(items: IFilterSelection[], isPublic: 0 | 1): any {
    return items.filter(item => item.isPublic === isPublic)
  }
}
