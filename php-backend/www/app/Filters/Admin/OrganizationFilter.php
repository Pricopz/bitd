<?php

namespace App\Filters\Admin;

use App\Enum\HuntingBackgroundTypeEnum;
use App\Enum\OrganizationStatusEnum;
use App\Filters\BaseFilter;
use App\Models\HuntingBackground;
use App\Models\Organization\Organization;
use Illuminate\Database\Query\Builder;
use Illuminate\Validation\Rule;

/**
 * Class OrganizationFilter
 * @package App\Filters\Admin
 */
class OrganizationFilter extends BaseFilter
{
    /**
     * @var string|null
     */
    public ?string $key = null;
    /**
     * @var array
     */
    public array $status = [];
    /**
     * @var array
     */
    public array $administration = [];
    /**
     * @var int|null
     */
    public ?int $county_hunt = null;
    /**
     * @var int|null
     */
    public ?int $county_fish = null;

    /**
     * @param $builder
     * @return mixed
     */
    public function getQueryBuilder(Builder $builder): Builder
    {
        $t0     = Organization::getTableName();
        $t1     = HuntingBackground::getTableName();
        $self   = $this;
        if ($this->key) {
            $builder->where(static function ($query) use ($self, $t0) {
                $query->where('cif', 'like', "%{$self->key}%")
                    ->orWhere("{$t0}.name", 'like', "%{$self->key}%")
                    ->orWhere("{$t0}.short_name", 'like', "%{$self->key}%");
            });
        }
        if ($this->status) {
            $builder->whereIn("{$t0}.status", $this->status);
        }
        if ($this->administration && $havingRaw = $this->getHavingCondition()) {
            $builder->havingRaw($havingRaw);
        }
        if ($this->county_hunt) {
            $builder->where("{$t1}.county_id", '=', $this->county_hunt);
            $builder->where("{$t1}.type", '=', HuntingBackgroundTypeEnum::HUNTING);
        }
        if ($this->county_fish) {
            $builder->where("{$t1}.county_id", '=', $this->county_fish);
            $builder->where("{$t1}.type", '=', HuntingBackgroundTypeEnum::FISHING);
        }
        return $builder;
    }

    /**
     * @return string
     */
    protected function getHavingCondition(): string
    {
        $map = [
            1 => 'hunting_bg > 0',
            2 => 'fishing_bg > 0',
            3 => '(hunting_bg > 0 and fishing_bg > 0)',
        ];
        $having = [];
        foreach ($this->administration as $item) {
            if (isset($map[$item])) {
                $having[] = $map[$item];
            }
        }
        return implode(' or ', $having);
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            'key' => [
                'nullable',
                'string',
                'min:2',
            ],
            'status' => [
                'array',
                Rule::in(OrganizationStatusEnum::asList())
            ],
            'administration' => [
                'array',
                Rule::in([1, 2, 3])
            ],
            'county_hunt' => [
                'nullable',
                'int'
            ],
            'county_fish' => [
                'nullable',
                'int'
            ],
        ];
    }

    /**
     * @return array
     */
    public function getErrorMessage() : array
    {
        return [
            'key.string'                        => trans('admin/validation.field.string'),
            'key.min'                           => trans('admin/validation.field.min'),
            'status.array'                      => trans('admin/validation.field.array'),
            'status.in'                         => trans('admin/validation.field.in'),
            'administration.array'              => trans('admin/validation.field.array'),
            'administration.in'                 => trans('admin/validation.field.in'),
            'county_hunt.int'                   => trans('admin/validation.field.invalid'),
            'county_fish.int'                   => trans('admin/validation.field.invalid'),
        ];
    }
}
